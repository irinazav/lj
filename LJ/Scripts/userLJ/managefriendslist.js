﻿
        $(document).ready(function () {
           
            $(function () {

                $(".link-delete-friend").on("click",  function (event) {
                    event.preventDefault(); 
                    var deleteLinkObj = $(this);
                    var parent = deleteLinkObj.closest(".row");
                    var imageToDelete = parent.find("img").clone().removeClass("userpic-size-sm").addClass("userpic-size-lg");

                    var userNameToDelete = deleteLinkObj.attr("href").split('=')[1];

                    var temp = $("<div><div class='container'><div class='userpic-modal'></div><div id='divModalDelete'>Do you want to delete <span class='glyphicon glyphicon-user'></span> " +
                        userNameToDelete + " from your friend list?</div></div>");
                    temp.find(".userpic-modal").append(imageToDelete);

                    BootstrapDialog.show({
                        title: 'Confirmation',
                        message: temp.html(),

                        buttons: [{
                            label: 'Yes',
                            action: function (dialogItself) {

                                $.post(deleteLinkObj[0].href, function (data) {
                                    if (data.result == 1) {

                                        location.reload();
                                        dialogItself.close();
                                    }
                                    else {

                                        dialogItself.close();
                                        temp.find("#divModalDelete").html("Not able to delete <span class='glyphicon glyphicon-user'></span> " +
                                            userNameToDelete + " from your friend list.").addClass("text-danger");
                                        var dialogInstance1 = new BootstrapDialog({
                                            title: 'Error',
                                            type: BootstrapDialog.TYPE_DANGER,
                                            message: temp.html()
                                        });
                                        dialogInstance1.open();
                                    }

                                });

                            }

                        }, {
                            label: 'No',
                            action: function (dialogItself) {
                                dialogItself.close();
                            }
                        }]
                    });


                });




                //==============================
                $(function () {
                    $(".link-add-friend").on("click", function (event) {
                        event.preventDefault();
                        var deleteLinkObj = $(this);
                        var parent = deleteLinkObj.closest(".row");
                        var imageToDelete = parent.find("img").clone().removeClass("userpic-size-sm").addClass("userpic-size-lg");

                        var userNameToDelete = deleteLinkObj.attr("href").split('=')[1];

                        var temp = $("<div><div class='container'><div class='userpic-modal'></div><div id='divModalDelete'>Do you want to add <span class='glyphicon glyphicon-user'></span> " +
                            userNameToDelete + " to your friend list?</div></div>");
                        temp.find(".userpic-modal").append(imageToDelete);

                        BootstrapDialog.show({
                            title: 'Confirmation',
                            message: temp.html(),

                            buttons: [{
                                label: 'Yes',
                                action: function (dialogItself) {

                                    $.post(deleteLinkObj[0].href, function (data) {
                                        if (data.result == 1) {

                                            location.reload();
                                            dialogItself.close();
                                        }
                                        else {

                                            dialogItself.close();
                                            temp.find("#divModalDelete").html("Not able to add <span class='glyphicon glyphicon-user'></span> " +
                                                userNameToDelete + " to your friend list.").addClass("text-danger");
                                            var dialogInstance1 = new BootstrapDialog({
                                                title: 'Error',
                                                type: BootstrapDialog.TYPE_DANGER,
                                                message: temp.html()
                                            });
                                            dialogInstance1.open();
                                        }

                                    });

                                }

                            }, {
                                label: 'No',
                                action: function (dialogItself) {
                                    dialogItself.close();
                                }
                            }]
                        });


                    });

                });

            });



        });