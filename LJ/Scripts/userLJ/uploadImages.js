//used plugin from http://odyniec.net/projects/imgareaselect/usage.html
//=====================================================================

$(document).ready(function () {
    
    //value for new cloppedimage 
    var cloppedImageInfo = {    
        scale: 0,
        id : 0,
        newImageId: 0,
        x : 0,
        Y : 0,
        width : 0,
        height: 0,
        src: $("#preview").attr("src"),
        url: "",
        imageUploadTypeId: 0
    };

   

    //setup ddsclick
    $('#ddUserImages').ddslick({
        width: 90,
        onSelected: function (data) {
            $("#DefaultUserImageId").val(data.selectedData.value.toString());
        }
    });
    

    var croppreview = $("#previewCloppedImage");
    $("#inputFile").hide();

    setUpScrollImagesMenu();

    //Get the preview image and set the onload event handler
    var preview = $('#preview').load(function () {
       
        setPreview();
        ias.setOptions({
            aspectRatio: "1:1",//iz
            x1: 0,
            y1: 0,
            x2: $(this).width(),
            y2: $(this).height(),
            show: true
        });
    });

    //Set the 4 coordinates for the cropping
    var setPreview = function (x, y, w, h) {
       
        $('#UserImage_X').val(x || 0);
        $('#UserImage_Y').val(y || 0);
        $('#UserImage_Width').val(w || preview[0].naturalWidth);
        $('#UserImage_Height').val(h || preview[0].naturalHeight);
         
        
        cloppedImageInfo.x = y || 0;
        cloppedImageInfo.y = y || 0;
        cloppedImageInfo.width = w || preview[0].naturalWidth;
        cloppedImageInfo.height = h || preview[0].naturalHeight;
        
        
    };

    //Initialize the image area select plugin
    var ias = preview.imgAreaSelect({
        handles: true,
        instance: true,
        parent: 'body',
        onSelectChange: function (img, selection) {

           
            if (!selection.width || !selection.height)
                return;
           
            cloppedImageInfo.scale = preview[0].naturalWidth / preview.width();//iz
            var scale = cloppedImageInfo.scale;
            var scaleCropPreview = selection.width / croppreview.width();

            $('#previewCloppedImage img').css({
                width: Math.round(preview[0].naturalWidth / (scale * scaleCropPreview)),
                height: Math.round(preview[0].naturalHeight / (scale * scaleCropPreview)),
                marginLeft: -Math.round(selection.x1 / scaleCropPreview),
                marginTop: -Math.round(selection.y1 / scaleCropPreview)               
            });
            
        },
        onSelectEnd: function (s, selection) {
           
            var scale = preview[0].naturalWidth / preview.width();
            setPreview(
                         Math.round(scale * selection.x1),
                         Math.round(scale * selection.y1),
                         Math.round(scale * selection.width),
                         Math.round(scale * selection.height));
        }
    });

 
    //Initial state of X, Y, Width and Height is 0 0 1 1
    setPreview(0, 0, 1, 1);


    //===================================
    $('#UserImage_Url').change(function () {
        cloppedImageInfo.imageUploadTypeId = 1; 
        preview.attr('src', this.value);
       
    });


    //=====================================
    $('#buttonToggle').click(function () {
        $("#divCuttingArea").toggle();
       // var $this = $(this);       
      //  $this.text() == "Close cutting  area" ? $this.text("Open cutting area") : $this.text("Close cutting  area");
      
    });



  // File button ( hidden input file works via "label for" )
    //==============================================
    $('#divInputFiles').on("change","input", function (evt) {
        var f = evt.target.files[0];
        var reader = new FileReader();
        if (!f.type.match('image.*')) {
            BootstrapDialog.alert({
                title: 'Warning',
                message: 'The selected file does not appear to be an image!',
                type: BootstrapDialog.TYPE_WARNING,
            });
            return;
        }
        cloppedImageInfo.imageUploadTypeId = 4; //Is File
        cloppedImageInfo.url = "";
        reader.onload = function (e) {
            preview.attr('src', e.target.result);
            $("#previewCloppedImage img").attr('src', e.target.result);
        };
        reader.readAsDataURL(f);   
    });

    // Url button
    //===================================
    $('#buttonUrl').click(function (e) {
        BootstrapDialog.show({
            message: 'Enter image url: <input  type="text" class="form-control"><br/><span class="text-warning"></span>',
            buttons: [{
                label: 'Upload',
                action: function (dialogRef) {
                    var url = $.trim(dialogRef.getModalBody().find('input').val());
                    //test if image
                    testUrl(url,
                       function (f, result) {
                        if (result == "success") {
                            preview.attr('src', url);
                            $("#previewCloppedImage img").attr('src', url);
                            cloppedImageInfo.imageUploadTypeId = 1;
                            cloppedImageInfo.url = url;
                            ias.update();
                            dialogRef.close();
                        }
                        else {
                            dialogRef.getModalBody().find('span').val("Test result = url is image : " + result);
                            dialogRef.open();
                        }
                    });
                }
            },
            {
                label: 'Close',
                action: function (dialogRef) {
                    dialogRef.close();
                }
            }]
        });
    });





    // Upload 
    //==================================
    $('#buttonUpload').click(function (e) {
       
        e.preventDefault();

        if ($("#preview").attr("src") == cloppedImageInfo.src) {
            BootstrapDialog.alert({
                title: 'Warning',
                message: 'No image selected!',
                type: BootstrapDialog.TYPE_WARNING, 
            });
            return;
        }           
        var newControl1 = $("#tmpUserImages").tmpl({
            newImageId: cloppedImageInfo.newImageId
        });
        
        newControl1.find(":first-child").find(":first-child").append($('#previewCloppedImage img').clone().attr("title", "Image not saved"));
        $("#divRowScroll").find('.no-img-userpic').remove();
       
        $("#divRowScroll").append(newControl1);
        

        var tmpInfo = {
            newImageId: cloppedImageInfo.newImageId,
            url: cloppedImageInfo.url,
            imageUploadTypeId: cloppedImageInfo.imageUploadTypeId,
            x : $('#UserImage_X').val(),
            y : $('#UserImage_Y').val(),
            w: $('#UserImage_Width').val(),
            h: $('#UserImage_Height').val()
        };
     
        var newControl2 = $("#tmpUploadedUserImages").tmpl(tmpInfo);
        var $divHiddenControls = $("#divHiddenControls");
        $divHiddenControls.append(newControl2);
      
        var $oldInputFile = $("#inputFile");
        var newId = "UploadedUserImage" + cloppedImageInfo.newImageId;
        // Url = 1, Flickr = 2, File = 4
        if (cloppedImageInfo.imageUploadTypeId == 4) { //File
            $oldInputFile.attr("id", newId).attr("name", "File");
            var newInputFile = $(" <input id='inputFile' name='inputFile' type='file' />");
            newInputFile.insertAfter($oldInputFile);
            newInputFile.hide();
        }
        else if (cloppedImageInfo.imageUploadTypeId == 1) { //Url
            var fakeInputFile = $(" <input id='UploadedUserImage" + cloppedImageInfo.newImageId + "' name='File' type='hidden' />");           
            fakeInputFile.insertBefore($oldInputFile);           
        }
       
        cloppedImageInfo.newImageId++;
        cloppedImageInfo.imageUploadTypeId = 0;
        $("#preview").attr("src", cloppedImageInfo.src);       
        $("#previewCloppedImage img").attr("src", cloppedImageInfo.src);
        $('#previewCloppedImage img').css({
            width: 50,
            height: 50,
            marginLeft: 0,
            marginTop: 0
        });
    });


    



    // button Save (Rename control before saving for binding at server )
    //===================================
    $('#buttonSaveUserpics').click(function (e) {
        e.preventDefault();
        
        var order = 0;
        
        $('.hidden-defaults').each(function (index, element) {
            if ($(this).val() == '1') { order = index +1; return false; }
        });

       
        if (order == 0) {
            BootstrapDialog.alert({
                title: 'Warning',
                message: 'Default userpic must be selected.',
                type: BootstrapDialog.TYPE_WARNING,
            });
            return;
        }
      
        $("#DefaultIdOrder").val(order);
        //UserImages sorted
        $('.hidden-ids').each(function (index, element) {
            var $this = $(this);
            $this.attr("name", "UserImages[" + index + "]." + $this.attr("name"));
            
        }); 
       
        $('.hidden-new-ids').each(function (index, element) {
            var $this = $(this);
            $this.attr("name", "UserImages[" + index + "]." + $this.attr("name"));           
        });
       
       
       
        // at server UploadedUserImages and UserImages join by newImageId
        $("#divHiddenControls").children().each(function (index, items) {
            
            $(items).children().each(function (i, item) {
                var $item = $(item);
                $item.attr("name", "UploadedUserImages[" + index + "]." + $item.attr("name"));
                
            });
        });
        
       
        $('#divInputFiles').children().not("#inputFile").each(function (index, element) {
            var $this = $(this);
            $this.attr("name", "UploadedUserImages[" + index + "]." + $this.attr("name"));
        });
     
        $(this).closest("form").submit();
    });



    //"is image?" test with callback
    //======================================
    function testUrl(url, callback, timeout) {
        timeout = timeout || 5000;
        var timedOut = false, timer;
        var img = new Image();
        img.onerror = img.onabort = function () {
            if (!timedOut) {
                clearTimeout(timer);
                callback(url, "error");
            }
        };
        img.onload = function () {
            if (!timedOut) {
                clearTimeout(timer);
                callback(url, "success");
            }
        };
        img.src = url;
        timer = setTimeout(function () {
            timedOut = true;
            callback(url, "timeout");
        }, timeout);
    }



    // check ending for img
    //===========================
    function checkUrl(url) {
        return (url.match(/\.(jpeg|jpg|gif|png)$/) != null);
    }
    

    // setUp images menu ,disable first and last
    //=========================================
    function setUpScrollImagesMenu() {
        var $divImages = $(".div-scroll-userpic");
        var last = $divImages.last();
        var first = $divImages.first();
        last.find(".makeLastImage").addClass("stopImageMenu");
        last.find(".moveRight").addClass("stopImageMenu");
        first.find(".makeFirstImage").addClass("stopImageMenu");
        first.find(".moveLeft").addClass("stopImageMenu");
    };
   

    //Menu for image pabel (row-scroll >.divImageMenu > .div-scroll-userpic)
    //=====================================================================
    $("#divRowScroll").on("click", ".makeFirstImage", function (event) {
        event.preventDefault();

        var $this = $(this);
        if ($this.hasClass("stopImageMenu")) return false;  // First

        var parent = $this.closest(".div-scroll-userpic");
       
        var images = $(".div-scroll-userpic");
        var count = images.size(); //4 -one image , 2 FirstImage
        var first = images.first(); // 2- lastImage will be first 


        first.find(".stopImageMenu").removeClass("stopImageMenu");

        if (count > 0) { // LastImage make Fist
            parent.find(".stopImageMenu").removeClass("stopImageMenu");

        }
        parent.find(".makeFirstImage").addClass("stopImageMenu");
        parent.find(".moveLeft").addClass("stopImageMenu");

        parent.insertBefore(first);

        var last = $(".div-scroll-userpic").last(); // 2- lastImage will be first 
        last.find(".makeLastImage").addClass("stopImageMenu");
        last.find(".moveRight").addClass("stopImageMenu");
        return false;

    });



    //==========================================
    $("#divRowScroll").on("click", ".makeLastImage", function (event) {
        event.preventDefault();

        var $this = $(this);
        if ($this.hasClass("stopImageMenu")) return false;  // Last

        var parent = $this.closest(".div-scroll-userpic");
        var images = $(".div-scroll-userpic");
        var count = images.size(); //4 -one image , 2 FirstImage
        var last = images.last(); // 2- lastImage will be first 

        last.find(".stopImageMenu").removeClass("stopImageMenu");

        if (count > 0) { // LastImage make Fist
            parent.find(".stopImageMenu").removeClass("stopImageMenu");
        }
        parent.find(".makeLastImage").addClass("stopImageMenu");
        parent.find(".moveRight").addClass("stopImageMenu");

        parent.insertAfter(last);

        var first = $(".div-scroll-userpic").first(); // 2- lastImage will be first 
        first.find(".makeFirstImage").addClass("stopImageMenu");
        first.find(".moveLeft").addClass("stopImageMenu");
        return false;

    });
    //===================================================

    $("#divRowScroll").on("click", ".moveRight", function (event) {
        event.preventDefault();
        var $this = $(this);
        if ($this.hasClass("stopImageMenu")) return false;  // LastRight

        var parent = $this.closest(".div-scroll-userpic");
        var images = $(".div-scroll-userpic");
        var count = parent.find(".stopImageMenu").size(); //4 -one image , 2 FirstImage

        var parentRightSibling = parent.next();
        var countSibling = parentRightSibling.find(".stopImageMenu").size();
        

        parent.insertAfter(parentRightSibling);

        if (count > 0 && countSibling > 0) {  //only 2 images
            parent.find(".stopImageMenu").removeClass("stopImageMenu");
            parent.find(".moveRight").addClass("stopImageMenu");
            parent.find(".makeLastImage").addClass("stopImageMenu");

            parentRightSibling.find(".stopImageMenu").removeClass("stopImageMenu");
            parentRightSibling.find(".makeFirstImage").addClass("stopImageMenu");
            parentRightSibling.find(".moveLeft").addClass("stopImageMenu");
            return false;
        }

        if (count > 0) {  //FistImage move right, images > 2 
            parent.find(".stopImageMenu").removeClass("stopImageMenu");
            parentRightSibling.find(".makeFirstImage").addClass("stopImageMenu");
            parentRightSibling.find(".moveLeft").addClass("stopImageMenu");
            return false;
        }

        if (countSibling > 0) {  //image will be Last, images > 2 

            parent.find(".makeLastImage").addClass("stopImageMenu");
            parent.find(".moveRight").addClass("stopImageMenu");
            parentRightSibling.find(".stopImageMenu").removeClass("stopImageMenu");
            return false;
        }

        return false;

    });


    //====================================================
    $("#divRowScroll").on("click", ".moveLeft", function (event) {
        event.preventDefault();
        var $this = $(this);
        if ($this.hasClass("stopImageMenu")) return false;  // FirstRight

        var parent = $this.closest(".div-scroll-userpic");
        var images = $(".div-scroll-userpic");
        var count = parent.find(".stopImageMenu").size();//4 -one image , 2 FirstImage
        var parentLeftSibling = parent.prev();
        var countSibling = parentLeftSibling.find(".stopImageMenu").size();

        parent.insertBefore(parentLeftSibling);

        if (count > 0 && countSibling > 0) {  //only 2 images
            parent.find(".stopImageMenu").removeClass("stopImageMenu");
            parent.find(".moveLeft").addClass("stopImageMenu");
            parent.find(".makeFirstImage").addClass("stopImageMenu");

            parentLeftSibling.find(".stopImageMenu").removeClass("stopImageMenu");
            parentLeftSibling.find(".makeLastImage").addClass("stopImageMenu");
            parentLeftSibling.find(".moveRight").addClass("stopImageMenu");
            return false;
        }

        if (count > 0) {  //LastImage move left, images > 2 
            parent.find(".stopImageMenu").removeClass("stopImageMenu");
            parentLeftSibling.find(".makeLastImage").addClass("stopImageMenu");
            parentLeftSibling.find(".moveRight").addClass("stopImageMenu");
            return false;
        }

        if (countSibling > 0) {  //image will be first, images > 2 

            parent.find(".makeFirstImage").addClass("stopImageMenu");
            parent.find(".moveLeft").addClass("stopImageMenu");
            parentLeftSibling.find(".stopImageMenu").removeClass("stopImageMenu");
            return false;
        }

        return false;

    });



    //=======================================
    $("#divRowScroll").on("click", ".makeMainImage", function (event) {
        var $this = $(this);
        event.preventDefault();
        var parent = $(this).closest(".div-scroll-userpic");
        var newDefaultImage = parent.find('img').clone().removeAttr("title").removeAttr("id");
      
        var temp = $("<div><div class='container'><div class='div-userpic-layout'></div><div>Do you want to set this userpick as default?</div></div>");
        temp.find(".div-userpic-layout").append(newDefaultImage);
       
         BootstrapDialog.show({
                title: 'Confirmation',
                message: temp.html(),
               
                buttons: [{
                    label: 'Yes',
                    action: function (dialogItself) {

                        $(".hidden-defaults").val("0");
                        parent.find(".hidden-defaults").val("1");

                        $(".glyphicon-ok").removeClass("glyphicon-ok").addClass("glyphicon-minus");
                        $this.removeClass("glyphicon-minus").addClass("glyphicon-ok");

                        $("#defaultImg1").replaceWith(newDefaultImage.clone().attr("id", "defaultImg1"));
                        $("#defaultImg2").replaceWith(newDefaultImage.clone().attr("id", "defaultImg2"));

                        dialogItself.close();
                    }
                }, {
                    label: 'No',
                    action: function (dialogItself) {
                        dialogItself.close();
                    }
                }]
         });

        return false;
    });



    //=============================================
    $("#divRowScroll").on("click", ".removeImage", function (event) {
        event.preventDefault();
        
        $this = $(this);
        var parent = $this.closest(".div-scroll-userpic");
        var imageToDelete = parent.find("img").clone();
        var grandParent = $("#divRowScroll");
       
            var temp = $("<div><div class='container'><div class='div-userpic-layout'></div><div>Do you want to delete this userpick?</div></div>");
            temp.find(".div-userpic-layout").append(imageToDelete);
       
            BootstrapDialog.show({
                title: 'Confirmation',
                message: temp.html(),
               
                buttons: [{
                    label: 'Yes',
                    action: function (dialogItself) {
                                              
                        // delete  uploaded staff  
                       if (parent.find(".hidden-ids").val() == '0') {
                            var newImageIdForDelete = parent.find(".hidden-new-ids").val();
                            $("#divUploadedImage" + newImageIdForDelete).remove();
                            $("#UploadedUserImage" + newImageIdForDelete).remove();
                       }
                        var isDefaultImage = parent.find('.glyphicon-ok').length > 0;
                        parent.empty().remove();
                        var count = grandParent.children().length;
                       
                        if (count == 0) { // no images
                            var newEmptyImage = $("<div class='div-scroll-userpic no-img-userpic'><img class='imageGalleryItem  userpic-size'  src='/Blog/GetImage/1' alt = 'no-image'/>" +
                                     "<input class='hidden-ids' id ='Id' name='Id' type='hidden' value='1' />" +
                                     "<input name='NewImageId' type='hidden' value='0' class='hidden-new-ids' />" +
                                     "<input class='hidden-defaults' name='IsDefaultId' type='hidden' value='1' /></div>");
                               newEmptyImage.appendTo(grandParent);
                           

                        }
                        
                        var first = grandParent.children().first();                        
                       if ((count == 1) || (count > 0 && isDefaultImage)) { // img in table> 0 && removed image was MainImage
                           
                            // general set up
                            first.find(".makeFirstImage").addClass("stopImageMenu");
                            first.find(".moveLeft").addClass("stopImageMenu");
                            first.find(".makeMainImage").addClass("glyphicon-ok").removeClass("glyphicon-minus");
                            first.find(".hidden-defaults").val("1");  //input for MainImage

                           
                            var last = grandParent.last();
                            last.find(".makeLastImage").addClass("stopImageMenu");
                            last.find(".moveRight").addClass("stopImageMenu");
                        }
                        /*
                       if (count == 0 || isDefaultImage) {
                            $("#defaultImg1").replaceWith(first.clone().removeAttr("title").attr("id", "defaultImg1"));
                            $("#defaultImg2").replaceWith(first.clone().removeAttr("title").attr("id", "defaultImg2"));
                        }*/
                       
                        dialogItself.close();
                    }
                    
                }, {
                    label: 'No',
                    action: function (dialogItself) {
                        dialogItself.close();
                    }
                }]
            });


    });


});