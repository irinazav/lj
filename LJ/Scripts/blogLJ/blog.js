﻿$(document).ready(function () {
   
    // Offset for new comment-reply
    var offset = 15;
    // ddslick - images select  , onSelected update imageid   
    var imageid;
    var deleteLinkObj;


    //responsive img and video
    $("img").addClass("img-responsive");
    $("iframe").addClass("embed-responsive-item").wrap("<div class='embed-responsive embed-responsive-16by9'></div>");
  

     // Modal window
    //https://ricardocovo.com/2010/09/02/asp-mvc-delete-confirmation-with-ajax-jquery-ui-dialog/
    //http://stackoverflow.com/questions/887029/how-to-implement-confirmation-dialog-in-jquery-ui-dialog


    // Hover show  likes list
    //http://html5beta.com/jquery-2/a-little-guide-about-jqueryui-tooltip-ajax-callback/
    //================================
    $(document).tooltip({
        items: 'a.likecount',
        tooltipClass: 'preview-tip',
        position: { my: "left+5 top", at: "right center" },
        content: function (callback) {
            var $this = $(this);
            var numbertoshow = 20;
            var href = $this.prev().attr("href")
                  .replace("UpdateLikes", "GetLikesList") + "&numbertoshow=" + numbertoshow;
           
            $.post(href, function (data) {                
                var userNamesWhoLikes = "<div><span class=\"likeListheader\">" +
                       data.likeordislike + ":</span><br/>" +
                       (data.count ? data.usernames.join(",<br>") : "No names") +
                       (data.count > numbertoshow ? "<br/>and " + (data.count - numbertoshow) + " more ..." : "") +
                       "</div>";
                
                callback(userNamesWhoLikes);
                if ($this.text() != data.count) $this.text(data.count);
            });
        },
    });
   
   

    // Comment, Reply. Post likes dislakes
    //================================
    $(function () {
       
        $("a.linklike").click(function (e) {
            e.preventDefault();

           var $this = $(this);
           $.post($this[0].href, function (data) {
               if (data.count == "-1") alert("You have rated it before");
               else 
                  $this.next().text(data.count);
           });

       });
    });



    //================================
    $(function () {

        $("button.btn-get-postratings").click(function (e) {
            e.preventDefault();
            window.open("/Visitor/GetPostRating", "mywindow", "status=1")
        });

        //================================
        $("button.confirm-delete-post").click(function (e) {

            e.preventDefault();
            var deleteLinkObj = $(this);
            var content = deleteLinkObj.attr("title");
            var form = deleteLinkObj.closest("form");
                        
            var strdialog = "<div>Do you want to delete this post?</div>";

            $(strdialog).dialog({
                draggable: false,
                modal: true,
                resizable: false,
                width: 'auto',
                title: "Delete post",
                buttons: {
                    "Confirm": function () {
                       
                         form.submit();                           
                        $(this).dialog("close");
                    },

                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });

        });
    });


    //confirm delete 
    //==============================
    $(function () {
       
        $("a.confirm-delete").click(function (e) {
           
            e.preventDefault();

            var target = $(this).attr("href");
            var title = $(this).attr("title");
            var content = $(this).attr("alt");
            
            deleteLinkObj = $(this);
            
            var strdialog = "<div id=\"dialog-confirm\" title=\"" + content +
                "\"><p><span class=\"ui-icon ui-icon-alert\" style=\"float:left; margin:12px 12px 20px 0;\"></span>The " +
                content + " will be permanently deleted and cannot be recovered. Are you sure?</p></div>";
         
            $(strdialog).dialog({
                draggable: false,
                modal: true,
                resizable: false,
                width: 'auto',
                title: title,
                buttons: {                   
                    "Confirm": function () {
                        $.post(deleteLinkObj[0].href, function (data) {
                            location.reload();
                            return;

                            //delete replies
                            var ids = data.ReplyIdsForDelete;
                            if (ids.length > 0) {
                               
                                for (var i = 0 ; i < ids.length; i++) {

                                    var parent = $(this).closest(".row");
                                    var body = parent.next();
                                    var bottomMenu = siblingBody.next();

                                    if (ids[i].Deleted) {
                                        parent.remove();
                                        bottomMenu.remove();
                                        body.remove();
                                    }
                                    else {                                                                           
                                       parent.find(".comment-time").html(ids[i].DateTime)
                                       parent.find(".comment-date").html(data.DeletedReplyText)
                                       bottomMenu.remove();
                                       body.html(ids[i].DateTime);
                                    }
                                }
                            };

                            //delete comment
                            ids = data.CommentIdsForDelete;
                            if (ids.length > 0) {

                                for (var i = 0 ; i < ids.length; i++) {
                                    if (ids[i].Deleted) {
                                        $("#gpcmt" + ids[i].Id).remove();
                                    }
                                    else {
                                        $("#cexcmt" + ids[i].Id).children().first().html(data.DeletedCommentText);
                                        $("#datecmt" + ids[i].Id).html(ids[i].DateTime);
                                        
                                        $("#cc2cmt" + ids[i].Id).remove();

                                    }
                                }
                            };                            
                        });

                        $(this).dialog("close");
                    },
                  
                    "Cancel": function () {
                        $(this).dialog("close");
                    }
                }
            });

        });

    });


// btn save comment/reply
//================================   
        $(".container").on("click", ".btn-save-tmp", function (e) {
           
            var form = $(this).closest("form");
            var control = form.find(".text-box")
            var val = parseInt(control.val());
          
            if (val == 0 || val > 1500)
            {
                e.preventDefault();
                var msg = val == 0? "Comment can't be empty" : "Comment HTML size can't be more than 1,500 characters."
                form.find(".comment-message").text(msg);
            }

    });

// Cancel button new form (new commen-reply)
//===========================================
$(function () {

    $(".container").on("click", ".btn-cancel-new-form", function (e) {
        e.preventDefault();
        var parent = $(this).closest(".row");

        parent.prev().find("a.comment-reply").data("clicks", false);
        var textareaid = parent.find("textarea").attr("id");
        
        tinymce.execCommand('mceRemoveEditor', false, textareaid);
        parent.empty().remove();

    });
});

// Cancel button edit form (new commen-reply)
//===========================================
$(function () {

    $(".container").on("click", ".btn-cancel-edit-form", function (e) {
        e.preventDefault();
        var parent = $(this).closest(".row");
        var nextparent = parent.next();
        var textareaid = parent.find("textarea").attr("id");       
        tinymce.execCommand('mceRemoveEditor', false, textareaid);
        parent.empty().remove();       
        nextparent.show();
        nextparent.find("a.edit-comment-reply").data("clicks", false);
        nextparent.next().show();
        nextparent.next().next().show();
    });
});

// New Comment-Reply (link)
//===========================================
$('a.new-comment-reply').click(function (e) {
    
    e.preventDefault();

    var clicks = $(this).data('clicks') ;   
    var arr = $(this).attr("href").split('/');
    var action = arr[2];
    var id = arr[3];
    var iscomment = action.indexOf('Comment') > -1;
    var isnew = action.indexOf('New') > -1;
    var body = "";  
    var controlid =   action + id;
    var parent = $(this).closest(".row");
    var parentmargin = parseInt(parent.css("margin-left").split("px")[0]);
    var margin = parentmargin + offset * (isnew && !iscomment ? 1 : 0)

        if (!clicks) {          
            var value = {
                isNew: isnew,
                isComment: iscomment,
                margin: margin,
                id: id,
                controlid: controlid,
                action: action,
                body: body
        };
            
            var newControl = $("#tmpFormControl").tmpl(value);
            newControl.insertAfter(parent);
            $('#ddimages' + controlid + " option[value=" + $("#userimageid" + controlid).val() + "]").attr("selected", "selected");
            $('#ddimages' + controlid).ddslick({
                width: 90,
                onSelected: function (data) {
                    $("#userimageid" + controlid).val(data.selectedData.value.toString());
                }
            });

            tinymce.init({
                selector: '#' + "body" + controlid,
                charLimit: 10,
                setup: function (editor) {
                    editor.on('keyup', function (e) {
                        updateTinyMiceHtmlCounter(editor, e, "#CommentsCharCount" + controlid)
                    });
                    editor.on('loadcontent', function (e) {
                        updateTinyMiceHtmlCounter(editor, e, "#CommentsCharCount" + controlid)
                    });
                },
                height: 200,
                theme: 'modern',
                plugins: [
                  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                  'searchreplace wordcount visualblocks visualchars code fullscreen',
                  'insertdatetime media nonbreaking save table contextmenu directionality',
                  'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons',
                image_advtab: true,

                templates: [
                  { title: 'Test template 1', content: 'Test 1' },
                  { title: 'Test template 2', content: 'Test 2' }
                ],
                content_css: [
                  '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                  '//www.tinymce.com/css/codepen.min.css'
                ]
            });
            var form = $("#CommentsCharCount" + controlid).closest("form");
          
            clicks = false;      
        }
        else
        {    
            tinymce.execCommand('mceRemoveEditor', false, "body" + controlid);
            $(this).closest(".row").next().empty().remove();        
        }

    $(this).data("clicks", !clicks);
});


 // Edit Comment-Reply (link)
//=========================================== 
function updateTinyMiceHtmlCounter(ed, e, inputCopyIdentifier) {
    var  tinylen, htmlcount;
    tinylen = ed.getContent().length;
    $(inputCopyIdentifier).val(tinylen);
}
 //==============================

var tinyMiceMaxCharLength = 5000;
 


    // Edit Comment-Reply (link)
    //===========================================
$('a.edit-comment-reply').click(function (e) {

    e.preventDefault();

    var clicks = $(this).data('clicks');
    var arr = $(this).attr("href").split('/');
    var action = arr[1];
    var id = arr[2];
    var iscomment = action.indexOf('Comment') > -1;
    var isnew = action.indexOf('New') > -1;
    var controlid = action + id;
    var parent = $(this).closest(".row");
    var next = parent.next();
    var nextnext = next.next();
    var margin = parseInt(parent.css("margin-left").split("px")[0]);
    var userimageid = parent.find("img").attr("src").split("/")[3];
    var body = next.find(".col").text();

    if (!clicks) {
        var value = {
            isNew: isnew,
            isComment: iscomment,
            userimageid: userimageid,
            margin: margin,
            id: id,
            controlid: controlid,
            action: action,
            body: next.find(".col").text()
        };

        var newControl = $("#tmpFormControl").tmpl(value);
        newControl.insertBefore(parent);
        parent.hide();
        next.hide();
        nextnext.hide();
        $('#ddimages' + controlid + " option[value=" + userimageid + "]").attr("selected", "selected");

        $('#ddimages' + controlid).ddslick({
            width: 90,
            onSelected: function (data) {
                $("#userimageid" + controlid).val(data.selectedData.value.toString());
            }
        });

        tinymce.init({
            selector: '#' + "body" + controlid,

            charLimit:10,
            setup: function(editor) {
                editor.on('keyup', function (e) {
                    updateTinyMiceHtmlCounter(editor, e, "#CommentsCharCount" + controlid)
                });
                editor.on('loadcontent', function (e) {
                    updateTinyMiceHtmlCounter(editor, e, "#CommentsCharCount" + controlid)
                });
            },

            height: 200,
            theme: 'modern',
            plugins: [
              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
              'searchreplace wordcount visualblocks visualchars code fullscreen',
              'insertdatetime media nonbreaking save table contextmenu directionality',
              'emoticons template paste textcolor colorpicker textpattern imagetools'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons',
            image_advtab: true,

            templates: [
              { title: 'Test template 1', content: 'Test 1' },
              { title: 'Test template 2', content: 'Test 2' }
            ],
            content_css: [
              '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
              '//www.tinymce.com/css/codepen.min.css'
            ]
        });
        clicks = false;
    }
    else {
        tinymce.execCommand('mceRemoveEditor', false, "body" + controlid);
        $(this).closest(".row").next().empty().remove();

        parent.show();
        next.show();
        nextnext.show();
    }

    $(this).data("clicks", !clicks);
});

    //Preventing Cross-Site Request Forgery (CSRF) Attacks in ASP.NET Web API
    // http://www.asp.net/web-api/overview/security/preventing-cross-site-request-forgery-csrf-attacks
     
    //Comments sort order
    //=======================================
     $('#cmtsortorder').change(function (e) {       
        
         $(this).closest('form').submit(); 
     });
    //===================================


});





