﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.ViewModels.User;

namespace LJ.Repository.UserRepository
{
    public interface IUserRepository<T, TId> where T : class
    {
        ApplicationUser GetUserByName(string username); //for test
        ApplicationUser GetUserBy(string username);
        UserProfile GetProfileBy(string username);
        int GetUserType(string visitorUserName, string jornalUserName);

        int SaveImage(ApplicationUserImage img);
        ApplicationUserImage GetImageBy(int id);
        ApplicationUserImage GetDefaultImageBy(string username);        
        int SaveSectionUserpics(IEnumerable<ApplicationUserImage> newImages,
                                IEnumerable<ApplicationUserImage> images,
                                UserViewModel userFromView, int defaultOrderId);

        int SaveSectionAbout(string UserName, string About);
        int SaveSectionInfo(ApplicationUserInfo info, string UserName, int DefaultImageId);
        int DeleteFriend(string userNameToDelete, string username);
        int AddFriend(string userNameToAdd, string username);

       int GetUsersCount();
       IEnumerable<string> GetUsersAlfabet();
       IEnumerable<ApplicationUser> GetUsers(int perPage, int pageNumber);
       IEnumerable<ApplicationUser> GetUsersByFirstLetter(string letter, int perPage, int pageNumber);
       UserStatistics GetUserStatistics(ApplicationUser user);

       StatisticsByFriendsViewModel GetFriendsStatistics(string username);
       StatisticsByCommentsViewModel GetPostsStatistics(string username, DateTime startDate, DateTime endDate);
       StatisticsByCommentsViewModel GetCommentsStatistics(string username, DateTime startDate, DateTime endDate) ;

       StatisticsByFriendsViewModel GetManageFriends(string username);
       ApplicationUser GetManageFriendsGroups(string username);

       UserAd GetAdByUserName(string username = "Pavarotti");

       FrontPageViewModel GetInfoForFrontPage(int usercount, int postcount);
       TopUsersViewModel GetUsersByAverageTopRating(int perPage, int pageNumber);
       TopUsersViewModel GetUsersByCurrentTopRating(int perPage, int pageNumber);
       IEnumerable<PostStatisticsViewModel> GetUsersRatingsChart(DateTime d1, DateTime d2, string username);

       RatingsViewModel GetPostRatingsMonthChart(DateTime d1, DateTime d2, int id);
       IEnumerable<PostStatisticsViewModel> GetUsersRatingsMonthChart(DateTime d1, DateTime d2, string username);
       IEnumerable<PostStatisticsViewModel> GetAllUsersRatingsMonthChart(DateTime d1, DateTime d2);
       IEnumerable<PostStatisticsViewModel> GetAllPostsRatingsMonthChart(DateTime d1, DateTime d2);

       ApplicationUser GetFriendsForListBoxes(string username);
       ApplicationUser GetGroupForListBoxes(string username, int groupid);

       void AddOrEditGroup(string username, string groupname, int groupid, IEnumerable<string> names);
       int DeleteGroup(string username, int groupid);
       void DeleteFriends(string username, IEnumerable<string> names);

       void CreateProfile(ApplicationUser user);
       DateTime GetAllUsersRatingsLastJob();
       DateTime GetUserFirstPostDate(string username);
       IEnumerable<string> GetPostCommentsDates(string userid);
       DateTime GetPostCommentsStartDate(string userid);

       string GetRoleId(string Name);

        
    }
}