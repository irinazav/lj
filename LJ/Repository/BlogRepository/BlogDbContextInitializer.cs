﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.Controllers;

using LJ.Repository.BlogRepository;

namespace LJ.Repository.BlogRepository
{   
public class BlogDbContextInitializer : DropCreateDatabaseIfModelChanges<BlogDbContext>
    {
        protected override void Seed(BlogDbContext context)
        {
            InitializeIdentityForEF(context);
            AddComments(context);
            base.Seed(context);
        }

        // need to add to global @ Application_Start()
        private void InitializeIdentityForEF(BlogDbContext context)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            string[] roles = { "Admin", "Community Moderator", "Community Member", "User", "UserPlus" };
            foreach (string role in roles)
            {
                if (!RoleManager.RoleExists(role))
                {
                    var roleresult = RoleManager.Create(new IdentityRole(role));
                }
            }


           
            ApplicationUserImage pImage;
            string imagePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content/UserImages/", "");

            //User Image Albums            
            List<ApplicationUserImage>[] userImages = { new List<ApplicationUserImage>(), new List<ApplicationUserImage>(), 
                                                        new List<ApplicationUserImage>(), new List<ApplicationUserImage>(), 
                                                        new List<ApplicationUserImage>(), new List<ApplicationUserImage>(),
                                                        new List<ApplicationUserImage>(), new List<ApplicationUserImage>(),
                                                        new List<ApplicationUserImage>(), new List<ApplicationUserImage>(),
                                                        new List<ApplicationUserImage>(), new List<ApplicationUserImage>(),
                                                        new List<ApplicationUserImage>(), new List<ApplicationUserImage>(),
                                                        new List<ApplicationUserImage>(), new List<ApplicationUserImage>()};

            //No image
            pImage = new ApplicationUserImage() { ImageOrderId = 1 };
            pImage.SaveImageFromDisc(imagePath + "NoImage.jpg");
            context.Set<ApplicationUserImage>().Add(pImage);
            context.SaveChanges(); 

            // User[0]  
            for (int i = 1; i <= 7; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "MarilynMonroeC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[0].Add(pImage);
            }


            // User[1]
            for (int i = 1; i <= 8; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "Barbra StreisandC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[1].Add(pImage);
            }

            // User[2]
            for (int i = 1; i <= 4; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "JacquelineC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[2].Add(pImage);
            }

            // User[3]
            for (int i = 1; i <= 9; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "PavarottC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[3].Add(pImage);
            }

            // User[4]
            for (int i = 1; i <= 7; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "MarxBrothersC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[4].Add(pImage);
            }

            // User[5]
            for (int i = 1; i <= 6; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "MarleneDietrichC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[5].Add(pImage);
            }

            // User[6]
            for (int i = 1; i <= 4; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "AudreyHepburnC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[6].Add(pImage);
            }


            // User[7]
            for (int i = 1; i <= 4; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "CherC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[7].Add(pImage);
            }

            // User[8]
            for (int i = 1; i <= 4; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "TaylorC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[8].Add(pImage);
            }

            // User[9]
            for (int i = 1; i <= 2; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "RingoStarrC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[9].Add(pImage);
            }

            // User[10]
            for (int i = 1; i <= 2; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "MattJonesC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[10].Add(pImage);
            }

            // User[11]
            for (int i = 1; i <= 3; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "BeatlesC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[11].Add(pImage);
            }

            // User[12]
            for (int i = 1; i <= 3; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "WhoopiGoldbergC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[12].Add(pImage);
            }

            // User[13]
            for (int i = 1; i <= 3; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "WoodyAllenC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[13].Add(pImage);
            }

            // User[14]
            for (int i = 1; i <= 4; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "LizaMinnelliC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[14].Add(pImage);
            }

            // User[15]
            for (int i = 1; i <= 3; i++)
            {
                pImage = new ApplicationUserImage() { ImageOrderId = i };
                pImage.SaveImageFromDisc(imagePath + "AlHirschfeldC" + i + ".png");
                context.Set<ApplicationUserImage>().Add(pImage);
                userImages[15].Add(pImage);
            }




            //Create ApplicationUser infos  Jacqueline Kennedy Onassis
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content/Images/", "");
            ApplicationUserInfo[] userInfos = {
                                       new ApplicationUserInfo() { 
                                                FirstName = "Marilyn", LastName = "Monroe",  
                                                Country ="USA", City ="Los Angeles",
                                                About ="Marilyn Monroe  was an American actress and model. Famous for playing \"dumb blonde\" characters, she became one of the most popular sex symbols of the 1950s, emblematic of the era's attitudes towards sexuality."},
                                       new ApplicationUserInfo() { 
                                                FirstName = "Barbra", LastName = "Streisand",
                                                Country ="USA", City ="Malibu",
                                                About = "Barbara Joan \"Barbra\" Streisand  is an American singer, songwriter, actress, and filmmaker. During a career spanning six decades, she has become an icon in multiple fields of entertainment, winning numerous awards, and has earned her recognition as Mother of All Contemporary Pop Divas or Queen of The Divas."},
                                       new ApplicationUserInfo() { 
                                                FirstName = "Jacqueline", LastName = "Kennedy-Onassis" ,  
                                                Country ="USA", City ="Manhattan",
                                                About = "Jacqueline Lee \"Jackie\" Kennedy Onassis was the wife of the 35th President of the United States, John F. Kennedy, and First Lady of the United States during his presidency from 1961 until his assassination in 1963."},                                                                             
                                       new ApplicationUserInfo() { 
                                                FirstName = "Luciano", LastName = "Pavarotti",  
                                                Country ="Italy"  , City ="Modena", 
                                                About ="Luciano Pavarotti,  was an Italian operatic tenor who also crossed over into popular music, eventually becoming one of the most commercially successful tenors of all time. He made numerous recordings of complete operas and individual arias, gaining worldwide fame for the brilliance and beauty of his tone—especially into the upper register—and eventually established himself as one of the finest tenors of the 20th century"},
                                       new ApplicationUserInfo() { 
                                                FirstName = "Marx", LastName = "The Marx Brothers",  
                                                Country ="USA", City ="Manhattan",
                                                About = "The Marx Brothers were a family comedy act that was successful in vaudeville, on Broadway, and in motion pictures from 1905 to 1949."},
                                       new ApplicationUserInfo() { 
                                                FirstName = "Marlene", LastName = "Dietrich",  
                                                Country ="France", City="Paris",
                                                About = "Marie Magdalene \"Marlene\" Dietrich  was a German-American actress and singer. Dietrich maintained popularity throughout her unusually long show business career by continually re-inventing herself, professionally and characteristically."},
                                      new ApplicationUserInfo() { 
                                                FirstName = "Audrey", LastName = "Hepburn",  
                                                Country ="Switzerland", City="Tolochenaz",
                                                About = "Audrey Hepburn was a British actress. Recognised as a film and fashion icon, Hepburn was active during Hollywood's Golden Age. She was ranked by the American Film Institute as the third-greatest female screen legend in Golden Age Hollywood and was inducted into the International Best Dressed List Hall of Fame."},
                                      new ApplicationUserInfo() { 
                                                FirstName = "Cherilyn", LastName = "Sarkisian",  
                                                Country ="USA", City="El Centro",
                                                About = "Cher is an American singer and actress. Called the Goddess of Pop, she is described as embodying female autonomy in a male-dominated industry"},
                                     new ApplicationUserInfo() { 
                                                FirstName = "Elizabeth", LastName = "Taylor",  
                                                Country ="USA", City="Los Angeles",
                                                About = "Elizabeth Rosemond Taylor was an English-American actress, businesswoman and humanitarian. She began as a child actress in the early 1940s, and was one of the most popular stars of classical Hollywood cinema in the 1950s. She continued her career successfully into the 1960s, and remained a well-known public figure for the rest of her life. The American Film Institute named her the seventh greatest female screen legend in 1999."},
                                    new ApplicationUserInfo() { 
                                                FirstName = "Richard", LastName = "Starkey",  
                                                Country ="England", City="Dingle",
                                                About = "Richard Starkey known professionally as Ringo Starr, is an English musician, singer, songwriter and actor who gained worldwide fame as the drummer for the Beatles. He occasionally sang lead vocals, usually for one song on an album, including \"With a Little Help from My Friends\", \"Yellow Submarine\" and their cover of \"Act Naturally\". He also wrote the Beatles' songs \"Don't Pass Me By\" and \"Octopus's Garden\", and is credited as a co-writer of others, including \"What Goes On\" and \"Flying\"."},
                                    new ApplicationUserInfo() { 
                                                FirstName = "Matt", LastName = "Jones",  
                                                Country ="USA", City="Sacramento",
                                                About = "Matthew Lee  is an American actor, voice actor, and comedian. He is best known for portraying Brandon \"Badger\" Mayhew on the AMC crime drama series Breaking Bad and for playing NCIS Special Agent Ned Dorneget in NCIS."},
                                    new ApplicationUserInfo() { 
                                                FirstName = "Beatles", LastName = "The Beatles",  
                                                Country ="England", City="Liverpool",
                                                About = "The Beatles were an English rock band, formed in Liverpool in 1960. With members John Lennon, Paul McCartney, George Harrison and Ringo Starr, they became widely regarded as the foremost and most influential act of the rock era."},
                                    new ApplicationUserInfo() { 
                                                FirstName = "Whoopi", LastName = "Goldberg",  
                                                Country ="USA", City="New York ",
                                                About = "Caryn Elaine Johnson, known professionally by her stage name, Whoopi Goldberg, is an American actress, comedian, and television host"},
                                    new ApplicationUserInfo() { 
                                                FirstName = "Woody", LastName = "Allen",  
                                                Country ="USA", City="NYC",
                                                About = "Heywood \"Woody\" Allen is an American actor, comedian, filmmaker, playwright and musician, whose career spans more than six decades."},
                                    new ApplicationUserInfo() { 
                                                FirstName = "Liza", LastName = "Minnelli",  
                                                Country ="USA", City=" Hollywood",
                                                About = "Liza May Minnelli is an American actress and singer. With a career spanning six decades, she has reached legendary status in multiple fields of entertainment and is among a small group of entertainers who have been honored with an Emmy, Grammy, Oscar, and Tony Award."},
                                    new ApplicationUserInfo() { 
                                                FirstName = "Al", LastName = "Hirschfeld",  
                                                Country ="USA", City="NYC",
                                                About = "Albert \"Al\" Hirschfeld  was an American caricaturist best known for his black and white portraits of celebrities and Broadway stars."},
                                                 
                                   };

            ApplicationUser[] users = { 
                               new ApplicationUser() { UserName = "Marilyn",  Email="MarilynMonroe@yahoo.com" , Created = DateTime.Now.AddDays(-14)}, 
                               new ApplicationUser() { UserName = "Barbra",  Email="BS@yahoo.com" , Created = DateTime.Now.AddDays(-90)}, 
                               new ApplicationUser() { UserName = "Jacqueline",   Email="JK@yahoo.com",Created = DateTime.Now.AddYears(-2) },
                               new ApplicationUser() { UserName = "Pavarotti",   Email="LP@yahoo.com" ,Created = DateTime.Now.AddYears(-1)},  
                               new ApplicationUser() { UserName = "Marx" , Email="inasonoff@yahoo.com" , Created = DateTime.Now.AddDays(-180)},
                               new ApplicationUser() { UserName = "Marlene" , Email="MarleneDietrich@yahoo.com" ,Created = DateTime.Now.AddDays(-300)}, 
                               new ApplicationUser() { UserName = "Audrey" , Email="Hepburn@yahoo.com" ,Created = DateTime.Now.AddDays(-300)}, 
                               new ApplicationUser() { UserName = "Cher" , Email="Cher@yahoo.com" ,Created = DateTime.Now.AddDays(-300)}, 
                               new ApplicationUser() { UserName = "Elizabeth" , Email="Taylor@yahoo.com" ,Created = DateTime.Now.AddDays(-800)}, 
                               new ApplicationUser() { UserName = "Ringo" , Email="RingoStarr@yahoo.com" ,Created = DateTime.Now.AddDays(-800)},                               
                               new ApplicationUser() { UserName = "Jones" , Email="MattJones@yahoo.com" ,Created = DateTime.Now.AddDays(-800)}, 
                               new ApplicationUser() { UserName = "Beatles" , Email="Beatles@yahoo.com" ,Created = DateTime.Now.AddDays(-800)}, 
                               new ApplicationUser() { UserName = "Whoopi" , Email="Whoopi@yahoo.com" ,Created = DateTime.Now.AddDays(-800)}, 
                               new ApplicationUser() { UserName = "Woody" , Email="WoodyAllen@yahoo.com" ,Created = DateTime.Now.AddDays(-800)},
                                new ApplicationUser() { UserName = "Liza" , Email="LizaMinnelli@yahoo.com" ,Created = DateTime.Now.AddDays(-800)},
                               new ApplicationUser() { UserName = "Hirschfeld" , Email="AlHirschfeld@yahoo.com" ,Created = DateTime.Now.AddDays(-800)},                              
                           };



            for (int i = 0; i < userInfos.Length; i++)
            {

                users[i].UserImages = userImages[i];
                users[i].DefaultUserImage = userImages[i].FirstOrDefault();
                users[i].ApplicationUserInfo = userInfos[i];
                users[i].password2 = "Password=" + (i + 1);

                var identityResult = UserManager.Create(users[i], users[i].password2);

                if (identityResult.Succeeded)
                {
                    UserManager.AddToRole(users[i].Id, "User");
                    if (users[i].UserName == "Marx" || users[i].UserName == "Hirschfeld")
                    {
                        UserManager.AddToRole(users[i].Id, "Admin");
                    }
                }
            }

            Category category1 = new Category { Id = 1, Name = "Art", Url = "-1-" };
            Category category2 = new Category { Id = 2, Name = "History", Url = "-2-" };
            Category category3 = new Category { Id = 3, Name = "Nature", Url = "-3-" };
            Category category4 = new Category { Id = 4, Name = "Life", Url = "-4-" };

            context.Categories.Add(category1);
            context.Categories.Add(category2);
            context.Categories.Add(category3);
            context.Categories.Add(category4);

            Tag tag1 = new Tag { Id = 1, Name = "War", Url = "--1" };
            Tag tag2 = new Tag { Id = 2, Name = "Hardagen", Url = "--2" };
            Tag tag3 = new Tag { Id = 3, Name = "New York", Url = "--3" };
            Tag tag4 = new Tag { Id = 4, Name = "NYBG", Url = "--4" };
            Tag tag5 = new Tag { Id = 5, Name = "Sculpture", Url = "--4" };
            Tag tag6 = new Tag { Id = 6, Name = "Fotografy", Url = "--4" };
            Tag tag7 = new Tag { Id = 7, Name = "Illustration", Url = "--4" };
            Tag tag8 = new Tag { Id = 8, Name = "Caricature", Url = "--4" };
            Tag tag9 = new Tag { Id = 9, Name = "Christmas", Url = "--4" };
            Tag tag10 = new Tag { Id = 10, Name = "Al Hirschfeld", Url = "--4" };
            Tag tag11 = new Tag { Id = 11, Name = "Halloween", Url = "--4" };
            Tag tag12 = new Tag { Id = 12, Name = "Robert Burns", Url = "--4" };
            Tag tag13 = new Tag { Id = 13, Name = "Horror", Url = "--4" };
            Tag tag14 = new Tag { Id = 14, Name = "Decoration", Url = "--4" };
            Tag tag15 = new Tag { Id = 15, Name = "Fashion ", Url = "--4" };
            Tag tag16 = new Tag { Id = 16, Name = "Juveliere", Url = "--4" };
            Tag tag17 = new Tag { Id = 17, Name = "Draft", Url = "--4" };
            Tag tag18 = new Tag { Id = 18, Name = "Movie", Url = "--4" };
            Tag tag19 = new Tag { Id = 19, Name = "Advertisement", Url = "--4" };
            Tag tag20 = new Tag { Id = 20, Name = "Engraving", Url = "--4" };
            Tag tag21 = new Tag { Id = 21, Name = "Famous People", Url = "--4" };
            Tag tag22 = new Tag { Id = 22, Name = "Math", Url = "--4" };
            Tag tag23 = new Tag { Id = 23, Name = "Manhattan", Url = "--4" };
            Tag tag24 = new Tag { Id = 24, Name = "Subway", Url = "--4" };
            Tag tag25 = new Tag { Id = 24, Name = "Flora", Url = "--4" };
            Tag tag26 = new Tag { Id = 25, Name = "Mosaic", Url = "--4" };
            Tag tag27 = new Tag { Id = 26, Name = "Stained Glass", Url = "--4" };




            context.Tags.Add(tag1);
            context.Tags.Add(tag2);
            context.Tags.Add(tag3);
            context.Tags.Add(tag4);
            context.Tags.Add(tag5);
            context.Tags.Add(tag6);
            context.Tags.Add(tag7);
            context.Tags.Add(tag8);
            context.Tags.Add(tag9);
            context.Tags.Add(tag10);
            context.Tags.Add(tag11);
            context.Tags.Add(tag12);
            context.Tags.Add(tag13);
            context.Tags.Add(tag14);
            context.Tags.Add(tag15);
            context.Tags.Add(tag16);
            context.Tags.Add(tag17);
            context.Tags.Add(tag18);
            context.Tags.Add(tag19);
            context.Tags.Add(tag20);
            context.Tags.Add(tag21);
            context.Tags.Add(tag22);
            context.Tags.Add(tag23);
            context.Tags.Add(tag24);
            context.Tags.Add(tag25);
            context.Tags.Add(tag26);
            context.Tags.Add(tag27);

            ApplicationUserImage[] userDafaultImage = userImages.Select(i => i.FirstOrDefault()).ToArray(); 
            string postPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content/UserPosts/", "");

            Post post1_1 = new Post
            {
                Id = 1,
                Title = "Мемориал американским морякам торгового флота  (часть 1)",
                Category = category2,
                UserImageId = userImages[4].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[4].Id,
                ApplicationUser = users[4],
                Url = "1--",
                Meta = "PP",
                Tags = new List<Tag> { tag1, tag3, tag5 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost1-1.txt"),
                Description = File.ReadAllText(postPath + "Post1-1.txt"),
                Content = "",
                PostedOn = DateTime.Now,
                Published = true
            };

            Post post1_2 = new Post
            {
                Id = 2,
                Title = "Мемориал американским морякам торгового флота  (часть 2)",
                Category = category2,
                UserImageId = userImages[4].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[4].Id,
                ApplicationUser = users[4],
                Url = "1--",
                Meta = "PP",
                Tags = new List<Tag> { tag1, tag2, tag3, tag5, tag6 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost1-2.txt"),

                Description = File.ReadAllText(postPath + "Post1-2.txt"),
                Content = "",
                PostedOn = DateTime.Now.AddMonths(-2).AddDays(3),
                Published = true
            };

            Post post1_3 = new Post
            {
                Id = 3,
                Title = "Мемориал американским морякам торгового флота  (часть 3)",
                Category = category2,
                UserImageId = userImages[4].Skip(3).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[4].Id,
                ApplicationUser = users[4],
                Url = "1--",
                Meta = "PP",
                Tags = new List<Tag> { tag1, tag2 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost1-3.txt"),

                Description = File.ReadAllText(postPath + "Post1-3.txt"),
                Content = "",
                PostedOn = DateTime.Now.AddMonths(-4).AddDays(-8),
                Published = true
            };

            Post post1_4 = new Post
            {
                Id = 4,
                Title = "Мемориал американским морякам торгового флота  (часть 4)",
                Category = category2,
                UserImageId = userDafaultImage[4].Id,
                ApplicationUserId = users[4].Id,
                ApplicationUser = users[4],
                Url = "1--",
                Meta = "PP",
                Tags = new List<Tag> { tag1, tag2 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost1-4.txt"),

                Description = File.ReadAllText(postPath + "Post1-4.txt"),
                Content = "",
                PostedOn = DateTime.Now.AddMonths(-6).AddDays(9),
                Published = true
            };

            Post post1_5 = new Post
            {
                Id = 5,
                Title = "Рейнхард Хардеген",
                Category = category2,
                UserImageId = userImages[4].Skip(4).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[4].Id,
                ApplicationUser = users[4],
                Url = "2--",
                Tags = new List<Tag> { tag1, tag2, tag6 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost1-5.txt"),
                Description = File.ReadAllText(postPath + "Post1-5.txt"),
                Content = "",
                PostedOn = DateTime.Now.AddMonths(-12).AddDays(18),
                Meta = "Nulla",
                Published = true
            };

            Post post3_1 = new Post
            {
                Id = 6,
                Title = "Венерина мухоловка  (часть 1)",
                Category = category3,
                UserImageId = userImages[3].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[3].Id,
                ApplicationUser = users[3],
                Url = "3--",
                Tags = new List<Tag> { tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost3-1.txt"),
                Description = File.ReadAllText(postPath + "Post3-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-1).AddDays(-6),
                Meta = "Nulla",
                Published = true
            };

            Post post3_2 = new Post
            {
                Id = 7,
                Title = "Венерина мухоловка  (часть 2)",
                Category = category3,
                UserImageId = userImages[3].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[3].Id,
                ApplicationUser = users[3],
                Url = "3--",
                Tags = new List<Tag> { tag5, tag16, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost3-2.txt"),
                Description = File.ReadAllText(postPath + "Post3-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-2).AddDays(20),
                Meta = "Nulla",
                Published = true
            };

            Post post3_3 = new Post
            {
                Id = 8,
                Title = "Венерина мухоловка  (часть 3)",
                Category = category1,
                UserImageId = userImages[3].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[3].Id,
                ApplicationUser = users[3],
                Url = "3--",

                Tags = new List<Tag> { tag15, tag8, tag13, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost3-3.txt"),
                Description = File.ReadAllText(postPath + "Post3-3.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-4).AddDays(24),
                Meta = "Nulla",
                Published = true
            };

            Post post3_4 = new Post
            {
                Id = 9,
                Title = "Венерина мухоловка  (часть 4)",
                Category = category1,
                UserImageId = userImages[3].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[3].Id,
                ApplicationUser = users[3],
                Url = "3--",
                Tags = new List<Tag> { tag19, tag18, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost3-4.txt"),
                Description = File.ReadAllText(postPath + "Post3-4.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-1).AddDays(45),
                Meta = "Nulla",
                Published = true
            };


            Post post4_0 = new Post
            {
                Id = 10,
                Title = "Тэм О'Шентер. Иллюстрации (часть 1)",
                Category = category1,
                UserImageId = userImages[0].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[0].Id,
                ApplicationUser = users[0],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag12, tag13 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost4-0.txt"),
                Description = File.ReadAllText(postPath + "Post4-0.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddDays(-8),
                Meta = "Nulla",
                Published = true,

            };



            Post post4_1 = new Post
            {
                Id = 11,
                Title = "Тэм О'Шентер. Иллюстрации (часть 2)",
                Category = category1,
                UserImageId = userDafaultImage[0].Id,
                ApplicationUserId = users[0].Id,
                ApplicationUser = users[0],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag12, tag13 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost4-1.txt"),
                Description = File.ReadAllText(postPath + "Post4-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddDays(-7).AddMonths(-1),
                Meta = "Nulla",
                Published = true,

            };

            Post post4_2 = new Post
            {
                Id = 12,
                Title = "Тэм О'Шентер. Иллюстрации (часть 3)",
                Category = category1,
                UserImageId = userImages[0].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[0].Id,
                ApplicationUser = users[0],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag12, tag13 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost4-2.txt"),
                Description = File.ReadAllText(postPath + "Post4-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-3),
                Meta = "Nulla",
                Published = true
            };

            Post post4_3 = new Post
            {
                Id = 13,
                Title = "Тэм О'Шентер. Иллюстрации (часть 4)",
                Category = category1,
                UserImageId = userImages[0].Skip(3).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[0].Id,
                ApplicationUser = users[0],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag12, tag13, tag20 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost4-3.txt"),
                Description = File.ReadAllText(postPath + "Post4-3.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-3).AddDays(-7),
                Meta = "Nulla",
                Published = true
            };

            Post post4_4 = new Post
            {
                Id = 14,
                Title = "Тэм О'Шентер. Иллюстрации (часть 5)",
                Category = category1,
                UserImageId = userImages[0].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[0].Id,
                ApplicationUser = users[0],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag12, tag13, tag20 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost4-4.txt"),
                Description = File.ReadAllText(postPath + "Post4-4.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-5),
                Meta = "Nulla",
                Published = true
            };

            Post post4_5 = new Post
            {
                Id = 15,
                Title = "Тэм О'Шентер. Иллюстрации (часть 6)",
                Category = category1,
                UserImageId = userImages[0].Skip(5).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[0].Id,
                ApplicationUser = users[0],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag12, tag13 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost4-5.txt"),
                Description = File.ReadAllText(postPath + "Post4-5.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-8).AddDays(6),
                Meta = "Nulla",
                Published = true
            };

            Post post4_6 = new Post
            {
                Id = 16,
                Title = "Тэм О'Шентер. Иллюстрации (часть 7)",
                Category = category1,
                UserImageId = userImages[0].Skip(6).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[0].Id,
                ApplicationUser = users[0],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag12, tag13 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost4-6.txt"),
                Description = File.ReadAllText(postPath + "Post4-6.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-12).AddDays(8),
                Meta = "Nulla",
                Published = true
            };


            Post post5_1 = new Post
            {
                Id = 17,
                Title = "NYBG. Хэллоуин 2011",
                Category = category1,
                UserImageId = userImages[1].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[1].Id,
                ApplicationUser = users[1],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag4, tag5, tag13, tag11, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost5-1.txt"),
                Description = File.ReadAllText(postPath + "Post5-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-4),
                Meta = "Nulla",
                Published = true
            };

            Post post5_2 = new Post
            {
                Id = 18,
                Title = "NYBG. Хэллоуин 2012",
                Category = category1,
                UserImageId = userImages[1].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[1].Id,
                ApplicationUser = users[1],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag4, tag5, tag13, tag11, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost5-2.txt"),
                Description = File.ReadAllText(postPath + "Post5-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-3),
                Meta = "Nulla",
                Published = true
            };

            Post post5_3 = new Post
            {
                Id = 19,
                Title = "NYBG. Хэллоуин 2013",
                Category = category1,
                UserImageId = userImages[1].Skip(3).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[1].Id,
                ApplicationUser = users[1],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag4, tag5, tag13, tag11, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost5-3.txt"),
                Description = File.ReadAllText(postPath + "Post5-3.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-2),
                Meta = "Nulla",
                Published = true
            };

            Post post5_4 = new Post
            {
                Id = 20,
                Title = "NYBG. Хэллоуин 2014",
                Category = category1,
                UserImageId = userImages[1].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[1].Id,
                ApplicationUser = users[1],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag4, tag5, tag13, tag11, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost5-4.txt"),
                Description = File.ReadAllText(postPath + "Post5-4.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-1),
                Meta = "Nulla",
                Published = true
            };

            Post post5_5 = new Post
            {
                Id = 21,
                Title = "NYBG. Хэллоуин 2015",
                Category = category1,
                UserImageId = userImages[1].Skip(3).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[1].Id,
                ApplicationUser = users[1],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag4, tag5, tag13, tag11, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost5-5.txt"),
                Description = File.ReadAllText(postPath + "Post5-5.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6),
                Meta = "Nulla",
                Published = true
            };

            Post post6_1 = new Post
            {
                Id = 22,
                Title = "Грандиозное шоу Гиршфельда",
                Category = category1,
                UserImageId = userImages[15].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[15].Id,
                ApplicationUser = users[15],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag8, tag9, tag10, tag14 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost6-1.txt"),
                Description = File.ReadAllText(postPath + "Post6-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-3),
                Meta = "Nulla",
                Published = true
            };

            Post post7_1 = new Post
            {
                Id = 23,
                Title = "Хэллоуин. Экскурсия по «Party City» (часть 1)",
                Category = category4,
                UserImageId = userImages[5].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[5].Id,
                ApplicationUser = users[5],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag11, tag13 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost7-1.txt"),
                Description = File.ReadAllText(postPath + "Post7-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-1).AddDays(54),
                Meta = "Nulla",
                Published = true
            };

            Post post7_2 = new Post
            {
                Id = 24,
                Title = "Хэллоуин. Экскурсия по «Party City» (часть 2)",
                Category = category4,
                UserImageId = userImages[5].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[5].Id,
                ApplicationUser = users[5],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag11, tag13, tag14 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost7-2.txt"),
                Description = File.ReadAllText(postPath + "Post7-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-1).AddDays(34),
                Meta = "Nulla",
                Published = true
            };

            Post post8_1 = new Post
            {
                Id = 25,
                Title = "Злой клоун",
                Category = category4,
                UserImageId = userImages[7].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[7].Id,
                ApplicationUser = users[7],
                Url = "3--",
                Tags = new List<Tag> { tag18, tag11, tag13 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost8-1.txt"),
                Description = File.ReadAllText(postPath + "Post8-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-1).AddDays(84),
                Meta = "Nulla",
                Published = true
            };

            Post post9_1 = new Post
            {
                Id = 26,
                Title = "Американская история. Президенты. Zero Factor",
                Category = category2,
                UserImageId = userImages[11].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[11].Id,
                ApplicationUser = users[11],
                Url = "3--",
                Tags = new List<Tag> { tag21, tag13 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost9-1.txt"),
                Description = File.ReadAllText(postPath + "Post9-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-4),
                Meta = "Nulla",
                Published = true
            };

            Post post9_2 = new Post
            {
                Id = 27,
                Title = "Американская история. Президенты. Джеймс Гарфилд. Президентское доказательство",
                Category = category2,
                UserImageId = userImages[11].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[11].Id,
                ApplicationUser = users[11],
                Url = "3--",
                Tags = new List<Tag> { tag21, tag22 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost9-2.txt"),
                Description = File.ReadAllText(postPath + "Post9-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-9),
                Meta = "Nulla",
                Published = true
            };

            Post post10_1 = new Post
            {
                Id = 28,

                Title = "Самое красивое самоубийство (часть 1)",
                Category = category1,
                UserImageId = userImages[6].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[6].Id,
                ApplicationUser = users[6],
                Url = "3--",
                Tags = new List<Tag> { tag6, tag13, tag15, tag19 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost10-1.txt"),
                Description = File.ReadAllText(postPath + "Post10-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-3),
                Meta = "Nulla",
                Published = true
            };

            Post post10_2 = new Post
            {
                Id = 29,
                Title = "Самое красивое самоубийство (часть 2)",
                Category = category1,
                UserImageId = userImages[6].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[6].Id,
                ApplicationUser = users[6],
                Url = "3--",
                Tags = new List<Tag> { tag6, tag13, tag15, tag19 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost10-2.txt"),
                Description = File.ReadAllText(postPath + "Post10-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-4),
                Meta = "Nulla",
                Published = true
            };


            Post post10_3 = new Post
            {
                Id = 30,
                Title = "Смерть ей к лицу",
                Category = category1,
                UserImageId = userImages[6].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[6].Id,
                ApplicationUser = users[6],
                Url = "3--",
                Tags = new List<Tag> { tag6, tag13, tag15, tag19 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost10-3.txt"),
                Description = File.ReadAllText(postPath + "Post10-3.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-1),
                Meta = "Nulla",
                Published = true
            };


            Post post11_1 = new Post
            {
                Id = 31,
                Title = "Женщины Америки",
                Category = category2,
                UserImageId = userImages[12].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[12].Id,
                ApplicationUser = users[12],
                Url = "3--",
                Tags = new List<Tag> { tag21 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost11-1.txt"),
                Description = File.ReadAllText(postPath + "Post11-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-44),
                Meta = "Nulla",
                Published = true
            };

            Post post4_7 = new Post
            {
                Id = 32,
                Title = "Тэм О'Шентер",
                Category = category1,
                UserImageId = userDafaultImage[0].Id,
                ApplicationUserId = users[0].Id,
                ApplicationUser = users[0],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag12, tag5 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost4-7.txt"),
                Description = File.ReadAllText(postPath + "Post4-7.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddDays(-8),
                Meta = "Nulla",
                Published = true
            };

            Post post3_5 = new Post
            {
                Id = 33,
                Title = "Венерина мухоловка  (часть 5)",
                Category = category4,
                UserImageId = userImages[3].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[3].Id,
                ApplicationUser = users[3],
                Url = "3--",
                Tags = new List<Tag> { tag13, tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost3-5.txt"),
                Description = File.ReadAllText(postPath + "Post3-5.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddYears(-2).AddDays(45),
                Meta = "Nulla",
                Published = true
            };



            Post post12_1 = new Post
            {
                Id = 34,
                Title = "\"У Йосефа было маленькое пальто\" Симмс Табак",
                Category = category1,
                UserImageId = userImages[2].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[2].Id,
                ApplicationUser = users[2],
                Url = "3--",
                Tags = new List<Tag> { tag7 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost12-1.txt"),
                Description = File.ReadAllText(postPath + "Post12-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-8),
                Meta = "Nulla",
                Published = true
            };
            Post post12_2 = new Post
            {
                Id = 35,
                Title = "\"Старая леди, которая проглотила муху\" Симмс Табак",
                Category = category1,
                UserImageId = userDafaultImage[2].Id,
                ApplicationUserId = users[2].Id,
                ApplicationUser = users[2],
                Url = "3--",
                Tags = new List<Tag> { tag7 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost12-2.txt"),
                Description = File.ReadAllText(postPath + "Post12-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-14),
                Meta = "Nulla",
                Published = true
            };

            Post post12_3 = new Post
            {
                Id = 36,
                Title = "Kнига с «дыркой»",
                Category = category1,
                UserImageId = userDafaultImage[2].Id,
                ApplicationUserId = users[2].Id,
                ApplicationUser = users[2],
                Url = "3--",
                Tags = new List<Tag> { tag7 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost12-3.txt"),
                Description = File.ReadAllText(postPath + "Post12-3.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-49),
                Meta = "Nulla",
                Published = true
            };

            Post post13_1 = new Post
            {
                Id = 37,
                Title = "Манхэттен. Переобитаемый остров",
                Category = category2,
                UserImageId = userImages[14].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[14].Id,
                ApplicationUser = users[14],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag23 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost13-1.txt"),
                Description = File.ReadAllText(postPath + "Post13-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-9),
                Meta = "Nulla",
                Published = true
            };

            Post post13_2 = new Post
            {
                Id = 38,
                Title = "Манхэттен. Районы",
                Category = category2,
                UserImageId = userDafaultImage[14].Id,
                ApplicationUserId = users[14].Id,
                ApplicationUser = users[14],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag23 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost13-2.txt"),
                Description = File.ReadAllText(postPath + "Post13-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-5),
                Meta = "Nulla",
                Published = true
            };

            Post post13_3 = new Post
            {
                Id = 39,
                Title = "Манхэттен. История. Манна-хатта, Новый Амстердам, Нью-Йорк",
                Category = category2,
                UserImageId = userDafaultImage[14].Id,
                ApplicationUserId = users[14].Id,
                ApplicationUser = users[14],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag23 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost13-3.txt"),
                Description = File.ReadAllText(postPath + "Post13-3.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-1),
                Meta = "Nulla",
                Published = true
            };

            Post post14_1 = new Post

           {
               Id = 40,
               Title = "Достопримечательности Нью-Йорка: Метро. Redbirds",
               Category = category2,
               UserImageId = userDafaultImage[9].Id,
               ApplicationUserId = users[9].Id,
               ApplicationUser = users[9],
               Url = "3--",
               Tags = new List<Tag> { tag3, tag24 },
               ShortDescription = File.ReadAllText(postPath + "ShortPost14-1.txt"),
               Description = File.ReadAllText(postPath + "Post14-1.txt"),
               Content = "--",
               PostedOn = DateTime.Now.AddMonths(-6),
               Meta = "Nulla",
               Published = true
           };

            Post post15_1 = new Post

            {
                Id = 41,
                Title = "Достопримечательности Нью-Йорка: Метро. Искусство подземки. Masstransiscope",
                Category = category1,
                UserImageId = userImages[8].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[8].Id,
                ApplicationUser = users[8],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost15-1.txt"),
                Description = File.ReadAllText(postPath + "Post15-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-1),
                Meta = "Nulla",
                Published = true
            };

            Post post16_1 = new Post

            {
                Id = 42,
                Title = "Мозаика в метро Нью-Йорка. ДеКалб Импровизация",
                Category = category1,
                UserImageId = userImages[7].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[7].Id,
                ApplicationUser = users[7],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24 ,tag26},
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-1.txt"),
                Description = File.ReadAllText(postPath + "Post16-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-3),
                Meta = "Nulla",
                Published = true
            };

            Post post16_2 = new Post

            {
                Id = 43,
                Title = "Мозаика в метро Нью-Йорка. Городские Обитатели",
                Category = category1,
                UserImageId = userImages[8].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[8].Id,
                ApplicationUser = users[8],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24  ,tag26},
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-2.txt"),
                Description = File.ReadAllText(postPath + "Post16-2.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-4),
                Meta = "Nulla",
                Published = true
            };

            Post post16_3 = new Post

            {
                Id = 44,
                Title = "Мозаика в метро Нью-Йорка. Летающий дом: герои и героини Гарлема",
                Category = category1,
                UserImageId = userImages[12].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[12].Id,
                ApplicationUser = users[12],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24  ,tag26},
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-3.txt"),
                Description = File.ReadAllText(postPath + "Post16-3.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-3),
                Meta = "Nulla",
                Published = true
            };

            Post post16_4 = new Post

            {
                Id = 45,
                Title = "Мозаика в метро Нью-Йорка. Алиса: путь наружу",
                Category = category1,
                UserImageId = userImages[13].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[13].Id,
                ApplicationUser = users[13],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24  ,tag26},
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-4.txt"),
                Description = File.ReadAllText(postPath + "Post16-4.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-3),
                Meta = "Nulla",
                Published = true
            };
            Post post16_5 = new Post

            {
                Id = 46,
                Title = "Мозаика в метро Нью-Йорка. Carrying On",
                Category = category1,
                UserImageId = userImages[13].Skip(0).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[13].Id,
                ApplicationUser = users[13],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24  ,tag26},
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-5.txt"),
                Description = File.ReadAllText(postPath + "Post16-5.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6),
                Meta = "Nulla",
                Published = true
            };


            Post post16_6 = new Post

            {
                Id = 47,
                Title = "Скульптура в метро Нью-Йорка. Слёт птиц",
                Category = category1,
                UserImageId = userImages[10].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[10].Id,
                ApplicationUser = users[10],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag5 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-6.txt"),
                Description = File.ReadAllText(postPath + "Post16-6.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-53),
                Meta = "Nulla",
                Published = true,
                SecurityStatus = (int)SecurityStatuses.Private
            };
            Post post16_7 = new Post

            {
                Id = 48,
                Title = "Скульптура в метро Нью-Йорка. Услышь одинокий стон гудка",
                Category = category1,
                UserImageId = userImages[10].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[10].Id,
                ApplicationUser = users[10],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag5 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-7.txt"),
                Description = File.ReadAllText(postPath + "Post16-7.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6).AddYears(-6),
                Meta = "Nulla",
                Published = true,
                SecurityStatus = (int)SecurityStatuses.Private
            };
            Post post16_8 = new Post

            {
                Id = 49,
                Title = "Скульптура в метро Нью-Йорка. Пять точек наблюдения",
                Category = category1,
                UserImageId = userImages[10].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[10].Id,
                ApplicationUser = users[10],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24,  tag5 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-8.txt"),
                Description = File.ReadAllText(postPath + "Post16-8.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6).AddYears(-3),
                Meta = "Nulla",
                Published = true,
                SecurityStatus = (int)SecurityStatuses.Friends
            };


            Post post16_9 = new Post

            {
                Id = 50,
                Title = "Мозаика в метро Нью-Йорка. Цветение",
                Category = category1,
                UserImageId = userImages[10].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[10].Id,
                ApplicationUser = users[10],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag26 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-9.txt"),
                Description = File.ReadAllText(postPath + "Post16-9.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6).AddYears(-3),
                Meta = "Nulla",
                Published = true,
                SecurityStatus = (int)SecurityStatuses.Friends
            };

            Post post16_10 = new Post

            {
                Id = 51,
                Title = "Мозаика в метро Нью-Йорка. Поток",
                Category = category1,
                UserImageId = userImages[10].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[10].Id,
                ApplicationUser = users[10],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag26 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-10.txt"),
                Description = File.ReadAllText(postPath + "Post16-10.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6).AddYears(-6),
                Meta = "Nulla",
                Published = true,
               
            };

            Post post16_11 = new Post

            {
                Id = 52,
                Title = "Мозаика в метро Нью-Йорка. Прохождение",
                Category = category1,
                UserImageId = userImages[10].Skip(1).Take(2).FirstOrDefault().Id,
                ApplicationUserId = users[10].Id,
                ApplicationUser = users[10],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag26 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-11.txt"),
                Description = File.ReadAllText(postPath + "Post16-11.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6).AddYears(-2),
                Meta = "Nulla",
                Published = true
                
            };

            Post post16_12 = new Post

            {
                Id = 53,
                Title = "Мозаика в метро Нью-Йорка. Сад комьюнити",
                Category = category1,
                UserImageId = userImages[8].Skip(3).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[8].Id,
                ApplicationUser = users[8],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag26 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-12.txt"),
                Description = File.ReadAllText(postPath + "Post16-12.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6),
                Meta = "Nulla",
                Published = true
                
            };

            Post post16_13 = new Post
            {
                Id = 54,
                Title = "Мозаика в метро Нью-Йорка. Сад цирковых наслаждений",
                Category = category1,
                UserImageId = userImages[15].Skip(1).Take(3).FirstOrDefault().Id,
                ApplicationUserId = users[15].Id,
                ApplicationUser = users[15],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag26 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-13.txt"),
                Description = File.ReadAllText(postPath + "Post16-13.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6),
                Meta = "Nulla",
                Published = true
                
            };

            Post post16_14 = new Post
            {
                Id = 55,
                Title = "Мозаика в метро Нью-Йорка. Стенное скольжение, Место спокойствия",
                Category = category1,
                UserImageId = userImages[9].Skip(1).Take(2).FirstOrDefault().Id,
                ApplicationUserId = users[9].Id,
                ApplicationUser = users[9],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag26 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-14.txt"),
                Description = File.ReadAllText(postPath + "Post16-14.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6).AddYears(-3),
                Meta = "Nulla",
                Published = true

            };

            Post post16_15 = new Post
            {
                Id = 56,
                Title = "Витражи в метро Нью-Йорка. Моя малышка с Кони-Айленда",
                Category = category1,
                UserImageId = userImages[10].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[10].Id,
                ApplicationUser = users[10],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag27 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-15.txt"),
                Description = File.ReadAllText(postPath + "Post16-15.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-1).AddDays(-9),
                Meta = "Nulla",
                Published = true

            };

            Post post16_16 = new Post
            {
                Id = 57,
                Title = "Витражи в метро Нью-Йорка. See it Split, See it Change",
                Category = category1,
                UserImageId = userImages[13].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[13].Id,
                ApplicationUser = users[13],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag24, tag27 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost16-16.txt"),
                Description = File.ReadAllText(postPath + "Post16-16.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddDays(-29).AddYears(-3),
                Meta = "Nulla",
                Published = true

            };



            Post post17_1 = new Post

            {
                Id = 58,
                Title = "Филип Хаас \"Времена года\"",
                Category = category1,
                UserImageId = userImages[15].Skip(1).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[15].Id,
                ApplicationUser = users[15],
                Url = "3--",
                Tags = new List<Tag> { tag3, tag4, tag5,  tag25 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost17-1.txt"),
                Description = File.ReadAllText(postPath + "Post17-1.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-6).AddYears(-4),
                Meta = "Nulla",
                Published = true
            };

            Post post10_4 = new Post
            {
                Id = 59,
                Title = "История одной рукописи",
                Category = category1,
                UserImageId = userImages[6].Skip(2).Take(1).FirstOrDefault().Id,
                ApplicationUserId = users[6].Id,
                ApplicationUser = users[6],
                Url = "3--",
                Tags = new List<Tag> { tag7, tag15, tag19 },
                ShortDescription = File.ReadAllText(postPath + "ShortPost10-4.txt"),
                Description = File.ReadAllText(postPath + "Post10-4.txt"),
                Content = "--",
                PostedOn = DateTime.Now.AddMonths(-5),
                Meta = "Nulla",
                Published = true
            };

            
            List<PostLike> postLike1 = new List<PostLike>{
                                         new PostLike() {Id = 1,PostId = 1, User = users[0], 
                                             DateTime = DateTime.Now.AddHours(5), Like =true, Dislike = false},
                                         new PostLike() {Id = 2,PostId = 1, User = users[5], Like =true, Dislike = false},
                                         new PostLike() {Id = 3,PostId = 1, User = users[2], Like = false, Dislike = true},
                                         new PostLike() {Id = 4,PostId = 1, User = users[3], Like =true, Dislike = false},
                                         };
            List<PostLike> postLike2 = new List<PostLike>{
                                      
                                         new PostLike() {Id = 5,PostId = 2, User = users[4], Like =true, Dislike = true},
                                         new PostLike() {Id = 6,PostId = 2, User = users[3], Like = false, Dislike = true},
                                         new PostLike() {Id = 7,PostId = 2, User = users[2], Like =true, Dislike = false},
                                         };
            List<PostLike> postLike3 = new List<PostLike>{
                                         new PostLike() {Id = 8,PostId = 3,User = users[1], Like =true, Dislike = false},
                                         new PostLike() {Id = 9,PostId = 3, User = users[5], Like =true, Dislike = true},
                                         new PostLike() {Id = 10,PostId= 3, User = users[2], Like = true, Dislike = false},
                                         };

            context.Posts.Add(post1_1);
            context.Posts.Add(post1_2);
            context.Posts.Add(post1_3);
            context.Posts.Add(post1_4);
            context.Posts.Add(post1_5);

            context.Posts.Add(post3_1);
            context.Posts.Add(post3_2);
            context.Posts.Add(post3_3);
            context.Posts.Add(post3_4);

            context.Posts.Add(post4_0);
            context.Posts.Add(post4_1);
            context.Posts.Add(post4_2);
            context.Posts.Add(post4_3);
            context.Posts.Add(post4_4);
            context.Posts.Add(post4_5);
            context.Posts.Add(post4_6);

            context.Posts.Add(post5_1);
            context.Posts.Add(post5_2);
            context.Posts.Add(post5_3);
            context.Posts.Add(post5_4);
            context.Posts.Add(post5_5);

            context.Posts.Add(post6_1);

            context.Posts.Add(post7_1);
            context.Posts.Add(post7_2);

            context.Posts.Add(post8_1);
            context.Posts.Add(post9_1);
            context.Posts.Add(post9_2);
            context.Posts.Add(post10_1);
            context.Posts.Add(post10_2);
            context.Posts.Add(post10_3);
            context.Posts.Add(post11_1);
            context.Posts.Add(post4_7);
            context.Posts.Add(post3_5);
            context.Posts.Add(post12_1);
            context.Posts.Add(post12_2);
            context.Posts.Add(post12_3);
            context.Posts.Add(post13_1);
            context.Posts.Add(post13_2);
            context.Posts.Add(post13_3);
            context.Posts.Add(post14_1);
            context.Posts.Add(post15_1);
            context.Posts.Add(post16_1);
            context.Posts.Add(post16_2);
            context.Posts.Add(post16_3);
            context.Posts.Add(post16_4);
            context.Posts.Add(post16_5);
            context.Posts.Add(post16_6);
            context.Posts.Add(post16_7);
            context.Posts.Add(post16_8);
            context.Posts.Add(post16_9);
            context.Posts.Add(post16_10);
            context.Posts.Add(post16_11);
            context.Posts.Add(post16_12);
            context.Posts.Add(post16_13);
            context.Posts.Add(post16_14);
            context.Posts.Add(post16_15);
            context.Posts.Add(post16_16);
            context.Posts.Add(post17_1);
            context.Posts.Add(post10_4);

            context.SaveChanges();



            Comment comment1 = new Comment
            {
                Id = 1,
                PostId = 1,
                UserImageId = userDafaultImage[4].Id,
                Body = "Comment: 1-1",
                DateTime = DateTime.Now.Add(new TimeSpan(-6, 0, 0)),
                UserName = users[4].UserName
            };
            Comment comment2 = new Comment
            {
                Id = 2,
                PostId = 1,
                UserImageId = userDafaultImage[4].Id,
                Body = "Comment: 1-2",
                DateTime = DateTime.Now.Add(new TimeSpan(-6, 0, 0)),
                UserName = users[4].UserName
            };
            Comment comment3 = new Comment
            {
                Id = 3,
                PostId = 1,
                UserImageId = userDafaultImage[4].Id,
                Body = "Comment: 1-3",
                DateTime = DateTime.Now.Add(new TimeSpan(-6, 0, 0)),
                StatusId = 2,
                UserName = users[4].UserName
            };

            context.Comments.Add(comment1);
            context.Comments.Add(comment2);
            context.Comments.Add(comment3);
            // PostLikes
            List<PostLike> postLikes = new List<PostLike> {
                                         new PostLike(){Id = 1,PostId = 1,  User = users[0], DateTime = DateTime.Now.Add(new TimeSpan(-6, 0, 0)),Like =true, Dislike = false},
                                         new PostLike() {Id = 2,PostId = 1, User = users[1], DateTime = DateTime.Now.Add(new TimeSpan(-5, 0, 0)),Like =true, Dislike = false},
                                         new PostLike() {Id = 3,PostId = 1, User = users[2],DateTime = DateTime.Now.Add(new TimeSpan(-3, 0, 0)), Like = true, Dislike = false},
                                         new PostLike() {Id = 4,PostId = 2, User = users[5], DateTime = DateTime.Now.Add(new TimeSpan(-2, 0, 0)),Like =true, Dislike = false},                                                                               
                                         new PostLike() {Id = 5,PostId = 1, User = users[14], DateTime = DateTime.Now.Add(new TimeSpan(-4, 0, 0)),Like =true, Dislike = true},
                                         new PostLike() {Id = 6,PostId = 1, User = users[13], DateTime = DateTime.Now.Add(new TimeSpan(-3, 0, 0)), Like = false, Dislike = true}
            };
            context.PostLikes.AddRange(postLikes);

           
            List<CommentLike> commentLikes = new List<CommentLike> {
                                         new CommentLike(){Id = 1,CommentId = 1,  User = users[0], Like =true, Dislike = false},
                                         new CommentLike() {Id = 2,CommentId = 1, User = users[1], Like =true, Dislike = false},
                                         new CommentLike() {Id = 3,CommentId = 2, User = users[2], Like = false, Dislike = true},
                                         new CommentLike() {Id = 4,CommentId = 2, User = users[5], Like =true, Dislike = false},                                                                               
                                         new CommentLike() {Id = 5,CommentId = 3, User = users[4], Like =true, Dislike = true},
                                         new CommentLike() {Id = 6,CommentId = 3, User = users[3], Like = false, Dislike = true}
            };
            context.CommentLikes.AddRange(commentLikes);
            context.SaveChanges();

            Reply reply1 = new Reply
            {
                Id = 1,
                PostId = 1,
                CommentId = 1,
                ParentReplyId = null,
                DateTime = DateTime.Now.Add(new TimeSpan(-4, 0, 0)),
                UserName = users[2].UserName,
                UserImageId = userDafaultImage[2].Id,
                Body = " ParentReply 1-1-1"
            };
            Reply reply2 = new Reply
            {
                Id = 2,
                PostId = 1,
                CommentId = 1,
                ParentReplyId = null,
                DateTime = DateTime.Now.Add(new TimeSpan(-4, 0, 0)),
                UserName = users[0].UserName,
                UserImageId = userDafaultImage[0].Id,
                Body = " ParentReply 1-1-2"
            };
            Reply reply3 = new Reply
            {
                Id = 3,
                PostId = 1,
                CommentId = 1,
                ParentReplyId = 1,
                DateTime = DateTime.Now,
                UserName = users[3].UserName,
                UserImageId = userDafaultImage[3].Id,
                Body = "ChildReply 1-1-1-3"
            };
            Reply reply4 = new Reply
            {
                Id = 4,
                PostId = 1,
                CommentId = 1,
                ParentReplyId = 1,
                DateTime = DateTime.Now,
                UserName = users[3].UserName,
                UserImageId = userDafaultImage[3].Id,
                Body = "ChildReply 1-1-1-4"
            };
            Reply reply5 = new Reply
            {
                Id = 5,
                PostId = 1,
                CommentId = 1,
                ParentReplyId = 3,
                DateTime = DateTime.Now,
                StatusId = (int)Status.Edited,
                UserName = users[0].UserName,
                UserImageId = userDafaultImage[0].Id,
                Body = "ChildReply 1-1-1-3-5"
            };
            Reply reply6 = new Reply
            {
                Id = 6,
                PostId = 1,
                CommentId = 2,
                ParentReplyId = null,
                DateTime = DateTime.Now.Add(new TimeSpan(-4, 0, 0)),
                UserName = users[0].UserName,
                UserImageId = userDafaultImage[0].Id,
                Body = "Parent Reply 1-2-6"
            };
            Reply reply7 = new Reply
            {
                Id = 7,
                PostId = 1,
                CommentId = 2,
                ParentReplyId = 6,
                DateTime = DateTime.Now,
                UserName = users[3].UserName,
                UserImageId = userDafaultImage[3].Id,
                Body = "ChildReply 1-2-6-7"
            };
            Reply reply8 = new Reply
            {
                Id = 8,
                PostId = 1,
                CommentId = 2,
                ParentReplyId = 6,
                DateTime = DateTime.Now,
                StatusId = (int)Status.Edited,
                UserName = users[1].UserName,
                UserImageId = userDafaultImage[1].Id,
                Body = "ChildReply 1=2-6-8"
            };
            Reply reply9 = new Reply
            {
                Id = 9,
                PostId = 1,
                CommentId = 2,
                ParentReplyId = 7,
                DateTime = DateTime.Now,
                UserName = users[5].UserName,
                UserImageId = userDafaultImage[5].Id,
                Body = "ChildReply 1-2-7-9"
            };
            Reply reply10 = new Reply
            {
                Id = 10,
                PostId = 1,
                CommentId = 1,
                ParentReplyId = 3,
                DateTime = DateTime.Now,
                StatusId = (int)Status.Deleted,
                UserName = users[4].UserName,
                UserImageId = userDafaultImage[4].Id,
                Body = "Deleted  old: ChildReply 1-1-1-3-10 "
            };
            Reply reply11 = new Reply
            {
                Id = 11,
                PostId = 1,
                CommentId = 1,
                ParentReplyId = 10,
                DateTime = DateTime.Now,
                UserName = users[4].UserName,
                UserImageId = userImages[4].Skip(2).FirstOrDefault().Id,
                Body = "ChildReply 1-1-1-3-10-11"
            };
            Reply reply12 = new Reply
            {
                Id = 12,
                PostId = 1,
                CommentId = 3,
                ParentReplyId = null,
                DateTime = DateTime.Now,
                UserName = users[4].UserName,
                UserImageId = userImages[4].Skip(2).FirstOrDefault().Id,
                Body = "ChildReply 1-3-12"
            };

            context.Replies.Add(reply1);
            context.Replies.Add(reply2);
            context.Replies.Add(reply3);
            context.Replies.Add(reply4);
            context.Replies.Add(reply5);
            context.Replies.Add(reply6);
            context.Replies.Add(reply7);
            context.Replies.Add(reply8);
            context.Replies.Add(reply9);
            context.Replies.Add(reply10);
            context.Replies.Add(reply11);
            context.Replies.Add(reply12);


            // ReplyLikes
            List<ReplyLike> replyLikes = new List<ReplyLike>{
                                         new ReplyLike() {Id = 1,ReplyId = 1, User = users[0], Like =true, Dislike = false},
                                         new ReplyLike() {Id = 2,ReplyId = 1, User = users[1], Like =true, Dislike = false},
                                         new ReplyLike() {Id = 3,ReplyId = 2, User = users[2], Like = false, Dislike = true},
                                         new ReplyLike() {Id = 4,ReplyId = 2, User = users[5], Like =true, Dislike = false},                                                                               
                                         new ReplyLike() {Id = 5,ReplyId = 3, User = users[4], Like =true, Dislike = true},
                                         new ReplyLike() {Id = 6,ReplyId = 3, User = users[3], Like = false, Dislike = true},

                                         new ReplyLike() {Id = 7,ReplyId = 4, User = users[2], Like =true, Dislike = false},
                                        
                                         new ReplyLike() {Id = 8,ReplyId = 4,User = users[1], Like =true, Dislike = false},
                                         new ReplyLike() {Id = 9,ReplyId = 5, User = users[3], Like =true, Dislike = true},
                                         new ReplyLike() {Id = 10,ReplyId= 5, User = users[2], Like = true, Dislike = false},
                                         new ReplyLike() {ReplyId= 5, User = users[5], Like = true, Dislike = false},
                                         new ReplyLike() {ReplyId= 5, User = users[1], Like = true, Dislike = false},

                                         new ReplyLike() {ReplyId= 6, User = users[0], Like = true, Dislike = false},
                                         new ReplyLike() {ReplyId= 6, User = users[1], Like = true, Dislike = false},
                                         new ReplyLike() {ReplyId= 7, User = users[3], Like = true, Dislike = false},
                                         new ReplyLike() {ReplyId= 8, User = users[4], Like = true, Dislike = false},
                                         new ReplyLike() {ReplyId= 9, User = users[4], Like = true, Dislike = false},
                                         new ReplyLike() {ReplyId= 9, User = users[0], Like = true, Dislike = false},
                                         new ReplyLike() {ReplyId= 9, User = users[5], Like = true, Dislike = false},
                                         };

            context.ReplyLikes.AddRange(replyLikes);


            users[0].FriendsWith = new List<ApplicationUser>() { users[1], users[2], users[4], users[5], users[7], users[9], users[10], users[12] };
            users[0].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Deleted},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[10], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[12], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };


            users[1].FriendsWith = new List<ApplicationUser>() { users[0], users[2], users[4], users[6], users[7], users[9], users[10], users[12] };
            users[1].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Deleted},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[10], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[12], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[2].FriendsWith = new List<ApplicationUser>() { users[0], users[3], users[4], users[5], users[6], users[7], users[8], users[13], users[14], users[15] };
            users[2].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Deleted},
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[6], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[8], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[13], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[14], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[15], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[3].FriendsWith = new List<ApplicationUser>() { users[1], users[2], users[4], users[6], users[9], users[10], users[13], users[14] };
            users[3].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Deleted},
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[10], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[13], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[14], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[4].FriendsWith = new List<ApplicationUser>() { users[1], users[2], users[3], users[8], users[9], users[10], users[11], users[12], users[13], users[14], users[15] };
            users[4].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[8], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[10], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[11], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[12], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[13], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[14], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[15], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added}
            };

            users[5].FriendsWith = new List<ApplicationUser>() { users[0], users[1], users[2], users[3], users[4], users[7], users[8], users[9], users[13] };            
            users[5].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[8], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[13], Date = DateTime.Now.AddDays(-4), Action = (short)FriendAction.Added}
            };

            users[6].FriendsWith = new List<ApplicationUser>() { users[0], users[1], users[2], users[3], users[4], users[8], users[9], users[10], users[12] };
            users[6].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[8], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[10], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[12], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[7].FriendsWith = new List<ApplicationUser>() { users[0], users[1], users[2], users[5], users[6], users[9], users[15] };
            users[7].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[15], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[8].FriendsWith = new List<ApplicationUser>() { users[0], users[11], users[2], users[3], users[9], users[4] };
            users[8].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[11], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[9].FriendsWith = new List<ApplicationUser>() { users[0], users[1], users[2], users[3], users[4], users[5], users[6], users[9], users[13], users[14] };
            users[9].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[6], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[13], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[14], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[10].FriendsWith = new List<ApplicationUser>() { users[2], users[3], users[4], users[5], users[8], users[9], };
            users[10].FriendsWithArxiv = new List<FriendArxiv>()
            {  
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[8], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[11].FriendsWith = new List<ApplicationUser>() { users[0], users[3], users[4], users[13], users[9] };
            users[11].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},     
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[9], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},    
                new FriendArxiv{Friend = users[13], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[12].FriendsWith = new List<ApplicationUser>() { users[0], users[1], users[2], users[3], users[4], users[5], users[6], users[7], users[8], users[12], users[15] };
            users[12].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[4], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[5], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[6], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[8], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[12], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[15], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added}
            };

            users[13].FriendsWith = new List<ApplicationUser>() { users[0], users[1], users[2], users[3], users[11], users[12],  users[7] };
            users[13].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[3], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[11], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[12], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[7], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[14].FriendsWith = new List<ApplicationUser>() { users[0], users[1], users[2], users[6], users[8], users[11], users[13], users[15] };
            users[14].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[0], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-2), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[2], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[6], Date = DateTime.Now.AddDays(-3), Action = (short)FriendAction.Added},               
                new FriendArxiv{Friend = users[8], Date = DateTime.Now.AddDays(-12), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[11], Date = DateTime.Now.AddDays(-5), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[13], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added},
                new FriendArxiv{Friend = users[15], Date = DateTime.Now.AddDays(-6), Action = (short)FriendAction.Added}
            };

            users[15].FriendsWith = new List<ApplicationUser>() { users[1], };
            users[15].FriendsWithArxiv = new List<FriendArxiv>()
            {
                new FriendArxiv{Friend = users[1], Date = DateTime.Now.AddDays(-1), Action = (short)FriendAction.Added}  
            };


            users[0].FriendGroups = new List<Group> { 
                new Group { Name="Close Friends", Friends = new List<ApplicationUser>() { users[1], users[2], users[4]}},
                new Group { Name="Work", Friends = new List<ApplicationUser>() {  users[5], users[7], users[9]}},
            };


         
            users[10].FriendGroups = new List<Group> { 
                new Group { Name="School Group", Friends = new List<ApplicationUser>() {  users[2], users[3]}},
                new Group { Name="Work Group", Friends = new List<ApplicationUser>() {users[4], users[5]}},
                new Group { Name="Art Group", Friends = new List<ApplicationUser>() {users[3], users[4], users[5], users[8]}},
                new Group { Name="Sport Group", Friends = new List<ApplicationUser>() {users[9]}},
            };

           

            post16_8.Groups = new List<Group> { { users[10].FriendGroups.FirstOrDefault() } };
            post16_8.SecurityStatus = (int)SecurityStatuses.Custom;

            post16_10.Groups = new List<Group> { { users[10].FriendGroups.Skip(1).FirstOrDefault() } };
            post16_10.SecurityStatus = (int)SecurityStatuses.Custom;

            post16_11.Groups = new List<Group> {  users[10].FriendGroups.Skip(2).FirstOrDefault(), users[10].FriendGroups.FirstOrDefault()  };
            post16_11.SecurityStatus = (int)SecurityStatuses.Custom;
        }


        private void AddComments(BlogDbContext context)
        {
            var users = context.Users.Include(u => u.UserImages).OrderBy(p => p.Id).ToList();
            
            var posts = context.Posts.Include(u => u.ApplicationUser);
            var usersCount = users.Count();
            var postsCount = posts.Count();
            Random random = new Random();
            
            foreach(var p in posts.Where(p => p.Id !=1).ToList())
            {
                var postUserName = p.ApplicationUser.UserName;
                var postImageId = p.UserImageId;
                for (int i = 1; i <= random.Next(1, usersCount); i++ )
                {
                    if (random.Next(1, 4) != 2)
                    {
                        var userLeftComment = users.Skip(i).FirstOrDefault();
                        var userImages = userLeftComment.UserImages;
                        var imageIds = userImages.Select(img => img.Id).ToArray();
                        var isPostLike = random.Next(1, 4) != 2;

                        PostLike postLike = new PostLike
                        {

                            PostId = p.Id,
                            DateTime = DateTime.Now.AddDays(-(random.Next(1, 4))),
                            User = userLeftComment,
                            Like = isPostLike,
                            Dislike = !isPostLike
                        };
                        context.PostLikes.Add(postLike);

                        Comment comment = new Comment
                        {

                            PostId = p.Id,
                            UserImageId = imageIds[random.Next(0, userImages.Count() - 1)],
                            Body = "comment left by " + userLeftComment.UserName,
                            DateTime = DateTime.Now.AddDays(-(random.Next(7, 10))),
                            UserName = userLeftComment.UserName
                        };

                        context.Comments.Add(comment);
                    }
                }
            }
            context.SaveChanges();


            var comments = context.Comments.Where(c => c.PostId!=1).ToList();
            foreach(var c in comments)
            {
                var userReply = posts.Where(p => p.Id == c.PostId).FirstOrDefault().ApplicationUser;
               
                Reply reply = new Reply
                {
                    
                    PostId = c.PostId,
                    CommentId = c.Id,
                    ParentReplyId = null,
                    DateTime = DateTime.Now.AddDays(-(random.Next(1, 4))),
                    UserName = userReply.UserName,
                    UserImageId = (int)userReply.DefaultUserImageId ,
                    Body = "comment left by " + userReply.UserName,
                };
                context.Replies.Add(reply);

                
            }
            context.SaveChanges();

            var parentreplies = context.Replies.Where(r => r.PostId!=1 && r.ParentReplyId == null).ToList();
            foreach (var r in parentreplies)
            {
                for (int i = random.Next(12, 16); i < 16; i++) //i <= random.Next(1, usersCount); i++)
                {
                    var userLeftReply = users.Skip(i).FirstOrDefault();
                    var userImages = userLeftReply.UserImages;
                    var imageIds = userImages.Select(img => img.Id).ToArray();
                    var isReplyLike = random.Next(1, 4) != 2;
                    ReplyLike replyLike = new ReplyLike
                    {

                        ReplyId = r.Id,
                        User = userLeftReply,
                        Like = isReplyLike,
                        Dislike = !isReplyLike
                    };
                    context.ReplyLikes.Add(replyLike);
                    CommentLike commentLike = new CommentLike
                    {

                        CommentId = r.CommentId,
                        User = userLeftReply,
                        Like = isReplyLike,
                        Dislike = !isReplyLike
                    };
                    context.CommentLikes.Add(commentLike);

                    Reply reply = new Reply
                    {

                        PostId = r.PostId,
                        CommentId = r.CommentId,
                        ParentReplyId = r.Id,
                        UserImageId = imageIds[random.Next(0, userImages.Count() - 1)],
                        Body = "comment left by " + userLeftReply.UserName,
                        DateTime = DateTime.Now.AddHours(-(random.Next(1, 56))),
                        UserName = userLeftReply.UserName
                    };

                    context.Replies.Add(reply);
                }
            }
            context.SaveChanges();

            LJ.Controllers.VisitorController.CalculateRatingsForSeed();
        }
       
  }    
    
}