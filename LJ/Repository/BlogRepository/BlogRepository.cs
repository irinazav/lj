﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Reflection;
using LJ.Infrastructure;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.ViewModels;
using LJ.ViewModels.Blog;



namespace LJ.Repository.BlogRepository
{
    public class BlogRepository : IBlogRepository<Post, int>//, IDisposable
    {
        private BlogDbContext _context;
        public BlogRepository(BlogDbContext context)
        {
            _context = context;
        }

        

        // GRUD post
        //=============================
        public IEnumerable<Post> GetAll(int pageNo, int pageSize) //n
        {
            IEnumerable<Post> posts = _context.Posts
                    .Where(p => p.Published)
                    .OrderByDescending(p => p.PostedOn)
                    .Skip(pageSize * (pageNo - 1))
                    .Take(pageSize)
                    .Include("Category")
                    .Include("Tags")
                    .ToList();

            return posts;
        }

        

        // Totals
        //==================   
        public int TotalPosts(bool checkIsPublished ) //n
        {
            return _context.Posts.Where(p => p.Published).Count();
        }

        public int TotalPosts()
        {
            return _context.Posts.Where(p => p.Published).Count();
        }

        public int TotalPostsByUser(string username, bool published = true)
        {
            return _context.Posts.Where(p => p.ApplicationUser.UserName == username && (published || p.Published)).Count();
        }

        

        

        // Get post by
        //========================
        public Post GetById(int id) //n
        {
            return _context.Posts.Find(id);
        }

  
         
            
            
               
         //  GRUD Post
        //=================================
        public int SavePost1(Post updatedUser)
        {
            int id = updatedUser.Id;
            try 
            { 
                     Post original = _context
                         .Posts
                         .Where(p =>p.Id == updatedUser.Id)
                         .Include("Category")
                         .Include("Tags")
                         .FirstOrDefault();

                     if (original != null)
                     {                       
                         original.Tags.Clear();

                         updatedUser.Modified = DateTime.UtcNow;
                         CopyPropertyValues(updatedUser, original, new List<string> () { "Category", "Tags" });
                         foreach (Tag t in updatedUser.Tags)
                                original.Tags.Add(_context.Tags.Find(t.Id));
                         
                         original.Category = _context.Categories.Find(updatedUser.Category.Id);
                         original.CategoryId = original.Category.Id;
                         _context.SaveChanges();
                     }
                     else id = 0;
            }
            catch
            {
                id=0;
            }
            return id;
        }

        public int AddPost(Post post)
        {
            int id;
            try
            {   post.PostedOn = DateTime.UtcNow;
                var ids = post.Tags.Select(p => p.Id);
                IEnumerable<Tag> tags = _context
                    .Tags
                    .Where(t => ids
                        .Any(k => k == t.Id));
                post.Tags = tags.ToList();
                _context.Entry(post).State = EntityState.Added;
                _context.SaveChanges();
                id = post.Id;
            }
            catch
            {
                id = 0;
            }
            return id;

        }




        public int DeletePost(int id)
        {
            try
            {                   
                   Post post =_context.Posts.Find(id);

                
                    if (post != null){
                        _context.Entry(post).State = EntityState.Deleted;
                        _context.SaveChanges();
                    }  
            }
            catch
            {
                id = 0;
            }
            return id;
        }





        // CRUD Category
        //=============================
        public int TotalCategories()
        {
            return _context.Categories
                    .Count();
        }
        //===============================
        public Category GetCategory(int id)
        {
            return _context.Categories.Find(id);
        }

       
        
        //=========================================
        public int AddCategory(Category category)
        {
            Category entity = category;
            int id;
            try
            {
                _context.Entry(entity).State = EntityState.Added;
                _context.SaveChanges();
                id = entity.Id;
            }
            catch
            {
                id = 0;
            }
            return id;

        }
        //=========================================
        public int SaveCategory(Category updatedUser)
        {
            int id = 0;
            Category original = _context.Categories.FirstOrDefault<Category>(b => b.Id == updatedUser.Id);
            if (original != null)
            {
              
                CopyPropertyValues(updatedUser, original);
                _context.Entry(original).CurrentValues.SetValues(updatedUser);
                _context.SaveChanges();
            }
            return id;
        }
        //========================================
        public int DeleteCategory(int id)
        {
            try
            {
                foreach (var post in _context.Posts.Where(p => p.Category.Id == id))
                {
                    Category category= post.Category;
                    if (category != null) _context.Entry(category).State = EntityState.Deleted;
                }
                _context.SaveChanges();

            }
            catch
            {
                id = 0;
            }
            return id;
        }


        // Grud TAG
        //=============================
        public Tag GetTag(int id)
        {
            return _context.Tags.Where(p => p.Id == id).FirstOrDefault();
        }

        public int TotalTags()
        {
            return _context.Tags
                    .Count();
        }
        //=================================
        public IEnumerable<Tag> GetTags(string name ="")
        {
            IEnumerable<Tag> t = _context.Tags.OrderBy(p => p.Name);
            return t;
        }
        //================================
        public IEnumerable<Tag> GetTags(int pageNo, int pageSize, string sortColumn,
                          bool sortByAscending)
        {
            IEnumerable<Tag> tags = _context.Tags
                            .OrderBy(sortColumn, sortByAscending)
                            .Skip((pageNo - 1) * pageSize)
                            .Take(pageSize);
            return tags;
        }
        //===============================
        public void RemoveTag(int id)
        {
            Tag tag = _context.Tags.FirstOrDefault<Tag>(t => t.Id == id);
            _context.Entry(tag).State = EntityState.Deleted;
            _context.SaveChanges();
        }
        //==============================
        public int SaveTag(Tag entity)
        {
            int id = 0;
            Tag t = _context.Tags.FirstOrDefault<Tag>(b => b.Id == entity.Id);
            if (t != null)
            {
                t.Name = entity.Name;
                _context.Entry(t).State = EntityState.Modified;
                _context.SaveChanges();
                id = entity.Id;
            }
            return id;
        }


        //==================================
        public int AddTag(Tag tag)
        {
            Tag t = tag;
            int id;
            try
            {
                _context.Entry(t).State = EntityState.Added;
                _context.SaveChanges();
                id = t.Id;
            }
            catch
            {
                id = 0;
            }
            return id;

        }

        //=================================
        public Category GetCategory(string url)
        {
            return _context.Categories.Where(p => p.Url.Equals(url)).FirstOrDefault();
        }
        //===================================
        public int DeleteTag(int id)
        {
            try
            {
                IEnumerable<Post> posts = _context
                    .Posts
                    .Include("Tags")
                    .Where(p => p.Tags.Any(t => t.Id == id));

                foreach (var p in posts)
                {
                    Tag t = _context.Tags.Where(tt => tt.Id == id)
                        .FirstOrDefault();
                    if (t != null) _context.Entry(t).State = EntityState.Deleted;
                }
                _context.SaveChanges();

            }
            catch
            {
                id = 0;
            }
            return id;
        }

      // Help functions
      //================== http://stackoverflow.com/questions/4712889/copy-data-from-one-object-to-another
        private static void CopyPropertyValues(object source, object destination, IEnumerable<string> typesToRemove = null)
        {
            
            var destProperties = destination
                .GetType()
                .GetProperties()
                .Where(p => (typesToRemove == null) || !(typesToRemove.Any(k => k==p.Name)));

            foreach (var sourceProperty in source.GetType().GetProperties())
            {
                foreach (var destProperty in destProperties)
                {
                    if (destProperty.Name == sourceProperty.Name &&
                destProperty.PropertyType.IsAssignableFrom(sourceProperty.PropertyType))
                    {
                        destProperty.SetValue(destination, sourceProperty.GetValue(
                            source, new object[] { }), new object[] { });

                        break;
                    }
                }
            }
        }


        // Get comments, replies
        //=============================
        public IEnumerable<CommentViewModel> GetPostComments(Post post)
        {
            return _context.Comments
                .Where(p => p.PostId == post.Id)
                .Select(c => new CommentViewModel()
                {
                    Comment = c,
                    CommentReplies = _context.Replies.Where(a => a.PostId == post.Id && a.CommentId == c.Id).Count(),
                    CommentLikes = _context.CommentLikes.Where(a => c.Id == a.CommentId &&  a.Like).Count(),
                    CommentDislikes = _context.CommentLikes.Where(a => c.Id == a.CommentId && a.Dislike).Count()
                }
                 );                
        }


       //==============================================
        public List<CommentViewModel> GetChildReplies(int parentReplyId)
        {
            List<CommentViewModel> chldReplies = new List<CommentViewModel>();
            var childReplies = _context.Replies.Where(p => p.ParentReplyId == parentReplyId).ToList();
            foreach (var reply in childReplies)
            {
                var chReplies = GetChildReplies(reply.Id);
                chldReplies.Add(new CommentViewModel()
                {
                    Body = reply.Body,
                    ParentReplyId = reply.ParentReplyId,
                    DateTime = reply.DateTime,
                    Id = reply.Id,
                    StatusId = reply.StatusId,
                    CommentDislikes = LikeDislikeCount((int)Likes.DislikeReply, reply.Id),
                    CommentLikes = LikeDislikeCount((int)Likes.LikeReply, reply.Id),
                    UserName = reply.UserName,
                    UserImageId = reply.UserImageId,
                    ChildReplies = chReplies
                });
            }

            return chldReplies;
        }

        //==========================================
       public List<CommentViewModel> GetReplies(Comment comment)
        {
            var parentReplies = _context.Replies
                .Where(p => p.CommentId == comment.Id && p.ParentReplyId == null)
                .ToList();

            List<CommentViewModel> parReplies = new List<CommentViewModel>();
            foreach (var pr in parentReplies)
            {
                var chReplies = GetChildReplies(pr.Id);
                parReplies.Add(new CommentViewModel() { Body = pr.Body, 
                    ParentReplyId = pr.ParentReplyId, 
                    DateTime = pr.DateTime, 
                    Id = pr.Id, 
                    CommentDislikes = LikeDislikeCount((int)Likes.DislikeReply, pr.Id),
                    CommentLikes = LikeDislikeCount((int)Likes.LikeReply, pr.Id),
                    UserName = pr.UserName, 
                    ChildReplies = chReplies });
            }
            return parReplies;
        }


        //  Get comment-reply ById
        //=============================
         public Reply GetReplyById(int id)
         {
             return _context
                 .Replies
                 .Where(p => p.Id == id)
                 .FirstOrDefault();
         }


         //=============================
         public Comment GetCommentById(int id)
        {
             return _context.Comments.Find(id);                
         }

         

         // Add comment and reply
         //=============================
         public int AddNewReply(Reply reply)
         {
             
             _context.Replies.Add(reply);
             _context.SaveChanges();
             return reply.Id;
         }


        //===================================
        public int AddNewComment(Comment comment)
         {             
             _context.Comments.Add(comment);
             _context.SaveChanges();
             return comment.Id;
         }


        //Edit comment reply
        //=============================
        public int EditReply(int id, string body, string username, int userimageid)
        {            
            var original = _context.Replies.Find(id);
            if (original == null || original.UserName != username) return 0;
            var result = original.PostId;
            original.Body = body;
            original.UserImageId = userimageid;
            original.StatusId = (int)Status.Edited;
            original.DateTime = DateTime.Now;
            
            try {
                _context.Entry(original).Property(x => x.Body).IsModified = true;
                _context.Entry(original).Property(x => x.StatusId).IsModified = true;
                _context.Entry(original).Property(x => x.DateTime).IsModified = true;
                _context.Entry(original).Property(x => x.UserImageId).IsModified = true;
                _context.SaveChanges();
            }
            catch {
                result= 0;
            }
            return result;
        }

        
        //============================================
         public int EditComment(int id, string body, string username, int userimageid)
        {            
            var original = _context.Comments.Find(id);
            if (original == null || original.UserName != username) return 0;
            var result = original.PostId;
            original.Body = body;
            original.UserImageId = userimageid;
            original.StatusId = (int)Status.Edited;
            original.DateTime = DateTime.Now;
            
            try {
                _context.Entry(original).Property(x => x.Body).IsModified = true;
                _context.Entry(original).Property(x => x.StatusId).IsModified = true;
                _context.Entry(original).Property(x => x.DateTime).IsModified = true;
                _context.Entry(original).Property(x => x.UserImageId).IsModified = true;
                _context.SaveChanges();
            }
            catch {
                result= 0;
            }
            return result;
        }

         //Delete post comment reply
         //=============================
         public int DeletePost(int postId, string userName)
         {           
             var originalPost = _context.Posts.Where(p => p.Id == postId)
                 .Include( p =>p.ApplicationUser)
                         .Include(p => p.UserImage)
                         .Include(p => p.Category)
                         .Include(p => p.PostLikes)
                         .Include(p => p.Comments)
                          .Include(p => p.Tags)
                         .Include(p => p.Replies)
                 .FirstOrDefault();
            

             if (userName == originalPost.ApplicationUser.UserName)
             {
                 originalPost.Replies.Clear();
                 originalPost.Comments.Clear();
               
                 //Likes
                 IEnumerable<PostLike> postLikes = _context.PostLikes.Where(p => p.PostId == postId).ToList();
                 foreach (var pl in postLikes)
                 {
                     _context.Entry(pl).State = EntityState.Deleted;
                 }
                 originalPost.PostLikes.Clear();

                 //Replies
                 IEnumerable<Reply> postReplies = _context.Replies.Where(p => p.PostId == postId).ToList();
                 foreach (var pl in postReplies)
                 {
                     _context.Entry(pl).State = EntityState.Deleted;
                 }
                 originalPost.Replies.Clear();

                 //Comments
                 IEnumerable<Comment> postComments = _context.Comments.Where(p => p.PostId == postId).ToList();
                 foreach (var pl in postComments)
                 {
                     _context.Entry(pl).State = EntityState.Deleted;
                 }
                 originalPost.Comments.Clear();

                 //Tags
                 IEnumerable<Comment> postTags = _context.Comments.Where(p => p.PostId == postId).ToList();
                 foreach (var pl in postTags)
                 {
                     _context.Entry(pl).State = EntityState.Deleted;
                 }
                 originalPost.Tags.Clear();
               
                 originalPost.Category = null;
                 originalPost.UserImage = null;
                 originalPost.ApplicationUser = null;

                  _context.Entry(originalPost).State = EntityState.Deleted;
                  
                 _context.SaveChanges();
                 return 1;
             }
             return 0;
         }


        
        //Delete comment reply
        //=============================
        public DeleteInfo DeleteComment(int commentId, string userName)
        {
            DeleteInfo deleteInfo = new DeleteInfo();           
            var originalComment = _context.Comments.Find(commentId);

            if (userName == originalComment.UserName)
            {
                var hasParentReplies = _context.Replies
                     .Any(r => r.CommentId == originalComment.Id && r.ParentReplyId == null);

                if (!hasParentReplies)
                {
                    deleteInfo.CommentIdsForDelete.Add(new DeleteCommenReplyInfo() { Id = originalComment.Id, Deleted = false });
                    _context.Comments.Remove(originalComment);
                }
                else
                {   var comment = originalComment;
                    comment.DateTime = DateTime.Now;
                    comment.Body = deleteInfo.DeletedCommentText;
                    comment.StatusId = comment.StatusId | (int)Status.Deleted;

                    deleteInfo.CommentIdsForDelete.Add(new DeleteCommenReplyInfo()
                    {
                        Id = comment.Id,
                        Deleted = false,
                        DateTime =  "Deleted " + LJ.Infrastructure.Extensions.TimePassed(comment.DateTime)
                     });

                    _context.Entry(originalComment).CurrentValues.SetValues(comment);
                }
                _context.SaveChanges();
            }

            return deleteInfo;
        }


        //=====================================
        public DeleteInfo DeleteReply(int replyId, string userName)
        {
            DeleteInfo deleteInfo = new DeleteInfo();
            
            var originalReply = _context.Replies.Find(replyId);
             
            if (userName == originalReply.UserName)
            {
               
               bool hasChildReplies = _context.Replies
                     .Any(r => originalReply.Id == r.ParentReplyId);

                // reply doesn't have children
               if (!hasChildReplies)
                {
                    var parentReplyId = originalReply.ParentReplyId;
                    var childReplyId = originalReply.Id;
                    var commentId = originalReply.CommentId;

                    // Delete last reply
                    _context.Replies.Remove(originalReply);
                    deleteInfo.ReplyIdsForDelete.Add( new DeleteCommenReplyInfo() {Id = childReplyId, Deleted= true}); 
                       
                    // Delete parents with deleted status without children
                    var reply = parentReplyId == null ? null : _context.Replies.Find(parentReplyId);
                   
                    while (reply != null && (reply.StatusId & (int)Status.Deleted)>0 &&
                                      !_context.Replies.Any((r => r.ParentReplyId == reply.Id && r.Id != childReplyId)))                                     
                    {

                        parentReplyId = reply.ParentReplyId;
                        childReplyId = reply.Id;
                        _context.Replies.Remove(reply);
                        reply = _context.Replies.Find(parentReplyId);
                       deleteInfo.ReplyIdsForDelete.Add(new DeleteCommenReplyInfo() {Id = childReplyId, Deleted= true});                       
                    }

                   // delete comment with status deleted if no replies left
                    if (reply == null)
                    {
                        var comment = _context.Comments
                            .Where(c => c.Id == commentId && (c.StatusId & (int)Status.Deleted) > 0)
                            .Include("Replies")
                            .Where( p => !p.Replies.Any(r => r.ParentReplyId == null))
                            .FirstOrDefault();
                         if (comment!= null)
                         {
                                deleteInfo.CommentIdsForDelete.Add( new DeleteCommenReplyInfo() { Id = commentId, Deleted= true});
                                _context.Comments.Remove(comment);
                         }
                    }
                }
                else
                {                 
                    var reply = originalReply;
                    reply.DateTime = DateTime.Now;
                    reply.Body = deleteInfo.DeletedReplyText;
                    reply.StatusId = reply.StatusId | (int)Status.Deleted;

                    deleteInfo.ReplyIdsForDelete = new List<DeleteCommenReplyInfo>() { 
                        new DeleteCommenReplyInfo() {Id = reply.Id, Deleted= false,  
                            DateTime = "Deleted " + LJ.Infrastructure.Extensions.TimePassed(reply.DateTime)}
                    };

                    _context.Entry(originalReply).CurrentValues.SetValues(reply);
                }
                _context.SaveChanges();
            }
            return deleteInfo;
        }

        


        //  Likes Dislikes
        //=============================
        public int UpdateLikes(int id, string username, int likeordislike)
        {            
            var type = likeordislike & (int)Likes.PostCommentReply;
            var like = (likeordislike & (int)Likes.Like) > 0;
            var dislike = (likeordislike & (int)Likes.Dislike) > 0;

            switch (type)
            {
                case (int)Likes.Post:
                    PostLike postLike = _context.PostLikes
                           .Include("User")
                           .Where(x => x.User.UserName == username && x.PostId == id).FirstOrDefault();
                    if (postLike == null) 
                    {
                        ApplicationUser user = _context.Users.Where (u => u.UserName == username).FirstOrDefault();
                        postLike = _context.PostLikes.Add(new PostLike() { PostId = id,
                            User = user, Like = like, Dislike = dislike , DateTime = DateTime.Now});
                    }
                    else       
                    {
                        if (like && postLike.Like || dislike && postLike.Dislike) return -1;
                        if (like) postLike.Like = like;
                        if (dislike) postLike.Dislike = dislike;
                        _context.Entry(postLike).State = EntityState.Modified;
                    }
                    break;

                case (int)Likes.Reply:
                    ReplyLike replyLike = _context.ReplyLikes
                           .Include("User")
                           .Where(x => x.User.UserName == username && x.ReplyId == id).FirstOrDefault();
                    if (replyLike == null) 
                    {
                        ApplicationUser user = _context.Users.Where (u => u.UserName == username).FirstOrDefault();
                        replyLike = _context.ReplyLikes.Add(new ReplyLike() { ReplyId = id, User = user, Like = like, Dislike = dislike });
                    }
                    else       
                    {
                         if (like && replyLike.Like || dislike && replyLike.Dislike) return -1;
                         if (like)   replyLike.Like = like;                        
                         if (dislike) replyLike.Dislike = dislike;

                        _context.Entry(replyLike).State = EntityState.Modified;
                    }
                    break;

                case (int)Likes.Comment:
                    CommentLike commentLike = _context.CommentLikes
                           .Include("User")
                           .Where(x => x.User.UserName == username && x.CommentId == id).FirstOrDefault();
                    if (commentLike == null)
                    {
                        ApplicationUser user = _context.Users.Where(u => u.UserName == username).FirstOrDefault();
                        commentLike = _context.CommentLikes.Add(new CommentLike() { CommentId = id, User = user, Like = like, Dislike = dislike });
                    }
                    else
                    {                       
                        if (like && commentLike.Like || dislike && commentLike.Dislike) return -1;
                        if (like) commentLike.Like = like;
                        if (dislike) commentLike.Dislike = dislike;
                        _context.Entry(commentLike).State = EntityState.Modified;
                    }
                    break;
            }
            _context.SaveChanges();
            return   this.LikeDislikeCount(likeordislike, id);
              
        }
        //==================================================
        public IEnumerable<string> GetListLikes(int id, int likeordislike)
        {
            switch (likeordislike)
            {
                case (int)Likes.LikePost:
                    return _context.PostLikes.Where(p => p.PostId == id && p.Like == true)
                        .OrderByDescending(c => c.Id).Select(f => f.User.UserName);
                case (int)Likes.DislikePost:
                    return _context.PostLikes.Where(p => p.PostId == id && p.Dislike == true)
                        .OrderByDescending(c => c.Id).Select(f => f.User.UserName);
                case (int)Likes.LikeComment:
                    return _context.CommentLikes.Where(p => p.CommentId == id && p.Like == true)
                        .OrderByDescending(c => c.Id).Select(f => f.User.UserName);
                case (int)Likes.DislikeComment:
                    return _context.CommentLikes.Where(p => p.CommentId == id && p.Dislike == true)
                        .OrderByDescending(c => c.Id).Select(f => f.User.UserName);
                case (int)Likes.LikeReply:
                    return _context.ReplyLikes.Where(p => p.ReplyId == id && p.Like == true)
                        .OrderByDescending(c => c.Id).Select(f => f.User.UserName);
                case (int)Likes.DislikeReply:
                    return _context.ReplyLikes.Where(p => p.ReplyId == id && p.Dislike == true)
                        .OrderByDescending(c => c.Id).Select(f => f.User.UserName);
                default:
                    return null;
            }
        }


        //=====================================================
        public int LikeDislikeCount(int likeordislike, int id)
        {
            switch (likeordislike)
            {
                case (int)Likes.LikePost:
                    return _context.PostLikes.Where(p => p.PostId == id && p.Like == true).Count();
                case (int)Likes.DislikePost:
                    return _context.PostLikes.Where(p => p.PostId == id && p.Dislike == true).Count();
                case (int)Likes.LikeComment:
                    return _context.CommentLikes.Where(p => p.CommentId == id && p.Like == true).Count();
                case (int)Likes.DislikeComment:
                    return _context.CommentLikes.Where(p => p.CommentId == id && p.Dislike == true).Count();
                case (int)Likes.LikeReply:
                    return _context.ReplyLikes.Where(p => p.ReplyId == id && p.Like == true).Count();
                case (int)Likes.DislikeReply:
                    return _context.ReplyLikes.Where(p => p.ReplyId == id && p.Dislike == true).Count();
                default:
                    return 0;
            }
        }


        //Images
        //=============================
        public IEnumerable<ApplicationUserImage> GetImagesBy(string username)
        {

            ApplicationUser user = _context.Users
                    .Where(u => u.UserName.Equals(username))
                    .Include("UserImages").FirstOrDefault();
            if (user != null)
                return user.UserImages == null ? null : 
                     user.UserImages.OrderBy(c => c.ImageOrderId).ToList();
            else return null;
        }

        public ApplicationUserImage GetImageBy(int id)
        {
            return (id != 0) ? _context.UserImages.Find(id) : null;
        }


        public ApplicationUserImage GetDefaultImageBy(string username)
        {

            ApplicationUser user = _context.Users.Where(u => u.UserName.Equals(username)).Include("DefaultUserImage").FirstOrDefault();
            if (user != null && user.DefaultUserImage != null)
                return user.DefaultUserImage;
            else return null;
        }

        //=========================
        public IEnumerable<int> GetPostIds(string username, int categoryid, int tagid, string visitorUserName ="")
        {
            IQueryable<Post> posts = _context.Posts
                 .Where(p => p.ApplicationUser.UserName == username);

            var visitorUser = visitorUserName == "" ? null :
                _context.Users.Where(u => u.UserName == visitorUserName).FirstOrDefault();

            if (visitorUser == null)
            {
                posts = posts.Where(p => p.SecurityStatus == (int)SecurityStatuses.Public);
            }
            else
            {
               
                posts = posts.Include(k => k.Groups)
                    .Where(p => p.SecurityStatus == (int)SecurityStatuses.Public ||
                            (p.SecurityStatus != (int)SecurityStatuses.Private &&

                              ((p.SecurityStatus == (int)SecurityStatuses.Friends &&
                                   p.ApplicationUser.FriendsWith.Any(u => u.Id == visitorUser.Id)) ||
                        
                                (p.SecurityStatus == (int)SecurityStatuses.Custom &&
                                   p.Groups.Any(gr => gr.Friends.Any(b => b.Id == visitorUser.Id))))
                       ));            
            }

            if (categoryid != 0)
            {
              posts = posts.Where(p => p.CategoryId == categoryid);
            }

            if (tagid != 0)
            {
              posts = posts.Include(p => p.Tags).Where(p => p.Tags.Any(t => t.Id == tagid));
            }

            IEnumerable<int> ids = posts.OrderBy(p => p.PostedOn)
                 .Select(k => k.Id);
            return ids;
       }


       
        //==================================
        public Post GetPostById(int id)
        {
            var post = _context.Posts.Where(p => p.Id == id)
                .Include(p => p.ApplicationUser)
                .Include(p=> p.UserImage)
                .Include("Category")
                .Include("Tags")
                .Include(p =>p.Groups)
                .Include(p => p.Comments)
                .Include(p => p.Replies)
                .FirstOrDefault();
            
            var tt = post;

            return post;
        }

//========================================================
        public PostViewModel GetPostExtraInfo(PostViewModel vm, int categoryid, int tagid, string visitorUserName ="")
        {
            var username = vm.UserName;
            long action = vm.UserPanelInfo.FriendType & (long)FriendType.Action;
            bool isMyself = (vm.UserPanelInfo.FriendType & (long)FriendType.MySelf) > 0;
            bool isAnonymous = (vm.UserPanelInfo.FriendType & (long)FriendType.Anonymous) > 0;

            ModelViewPosts mv = new ModelViewPosts();
            Category category = null;
            Tag tag = null;
            ApplicationUser user = null;
            IQueryable<Category> categories = null;
            IQueryable<Tag> tags = null;
            IQueryable<Post> posts = null;

            // Check filters          
            if (categoryid != 0)
            {
                category = _context.Categories.Find(categoryid);
                if (category == null) return null;
            }

            if (tagid != 0)
            {
                tag = _context.Tags.Find(tagid);
                if (tag == null) return null;
            }

            //Apply filters
            posts = _context.Posts;

            if (categoryid != 0)
            {

                posts = posts.Where(p => p.CategoryId == categoryid);
            }
            if (tagid != 0)
            {

                posts = posts.Include(p => p.Tags).Where(p => p.Tags.Any(tt => tt.Id == tagid));
            }

            // Query
            switch (action)
            {
                case ((long)FriendType.UserPost):
                    {
                        posts = posts.Where(p => p.ApplicationUserId == user.Id);

                        if (!isMyself)
                        {
                            if (isAnonymous)
                            {
                                posts = posts.Where(p => p.SecurityStatus == (int)SecurityStatuses.Public);
                            }
                            else
                            {
                                var visitorUser = _context.Users.Where(u => u.UserName == visitorUserName).FirstOrDefault();
                                if (visitorUser == null)
                                {
                                    posts = posts.Where(p => p.SecurityStatus == (int)SecurityStatuses.Public);
                                    break;
                                }
                                posts = posts.Include(k => k.Groups)
                                    .Where(p => p.SecurityStatus == (int)SecurityStatuses.Public ||
                                            (p.SecurityStatus != (int)SecurityStatuses.Private &&

                                              ((p.SecurityStatus == (int)SecurityStatuses.Friends &&
                                                   p.ApplicationUser.FriendsWith.Any(u => u.Id == visitorUser.Id)) ||
                                         
                                                (p.SecurityStatus == (int)SecurityStatuses.Custom &&
                                                   p.Groups.Any(gr => gr.Friends.Any(b => b.Id == visitorUser.Id))))
                                       ));
                            }
                        }
                        break;
                    }

                case ((long)FriendType.FriendsPost):
                    {
                        var friendsWith = user.FriendsWith.Select(p => p.UserName).ToList(); ;

                        posts = posts
                                      .Where(p => p.Published &&
                                          friendsWith.Any(pp => p.ApplicationUser.UserName == pp));
                        break;
                    }

                case ((long)FriendType.TopPost):
                    {
                        posts = posts.Where(p => p.Published);
                        break;
                    }
            }

            //SideBars
            categories = posts
                               .GroupBy(post => post.CategoryId)
                               .Select(post => post.FirstOrDefault())
                               .Include(post => post.Category)
                               .Select(post => post.Category)
                               .OrderBy(c => c.Name);

            tags = (from p in posts.Include(p => p.Tags)
                    from t in p.Tags
                    select t)
                    .Distinct()
                    .OrderBy(k => k.Name);
           
            //posts ids
            IEnumerable<int> ids = posts.OrderBy(p => p.PostedOn)
                .Select(k => k.Id).AsEnumerable().ToList();
           
            //paging
            vm.PrevPostId = ids.SkipWhile(i => i != vm.Id).Skip(1).Select(i => i).FirstOrDefault();
            vm.NextPostId = ids.TakeWhile(i => i != vm.Id).Select(i => i).LastOrDefault();
            vm.FirstPostId = ids.First();
            vm.LastPostId = ids.Last();
            vm.PostCount = ids.Count();
            var temp = _context.Posts.Where(p => p.Id == vm.Id)
                               .Include(p => p.Comments).Include(p => p.Replies)
                               .FirstOrDefault();
            vm.CommentsCount = temp.Replies.Count() + temp.Comments.Count();
                               
            //sidebar and filter
            vm.FilterCategories = categories;
            vm.FilterCategory = category;
            vm.FilterTags = tags;
            vm.FilterTag = tag;
            return vm;
        }
        //==================================
        public ModelViewPosts GetPosts1(int pageNumber, int postsPerPage,
                                        int categoryid, int tagid, string username,
                                        long userType, int groupid, int srcurity, 
                                        string visitorUserName = "")
        {
                    
            long action = userType & (long)FriendType.Action;            
            bool isMyself = (userType & (long)FriendType.MySelf) > 0;
            bool isAnonymous = (userType & (long)FriendType.Anonymous) > 0;
            ModelViewPosts mv = new ModelViewPosts();
            Category category = null;
            Tag tag = null;
            Group group = null;
            ApplicationUser user = null;
            IQueryable<Category> categories = null;
            IQueryable<Tag> tags = null ;
            IQueryable<Post> posts = null;
          
            // Check filters
            if (username!="")
            {
                user = _context.Users
                       .Where(u => u.UserName == username)
                       .Include(p => p.DefaultUserImage)
                       .Include(p => p.FriendGroups)
                       .FirstOrDefault();
                mv.User = user;
                if (user == null) return null;       
            }

            if (categoryid != 0)
            {
                category = _context.Categories.Find(categoryid);
                if (category == null) return null;
            }

            if (tagid != 0)
            {
                tag = _context.Tags.Find(tagid);
                if (tag == null) return null;
            }           

            if (groupid != 0)
            {
                group = _context.Groups
                    .Where(p => p.Id == groupid)
                    .Include( gr => gr.Friends)
                    .FirstOrDefault();

                if (group == null) return null;
            }

            //Apply filters
            posts = _context.Posts;
            if (categoryid != 0)
            {
               
                posts = posts.Where(p => p.CategoryId == categoryid);
            }

            if (tagid != 0)     
            {               
                posts = posts.Include(p => p.Tags).Where(p => p.Tags.Any(tt => tt.Id == tagid));
            }
       
            // Query
            switch (action)
            {
                case ((long)FriendType.UserPosts):
                {                    
                    posts = posts.Where(p => p.ApplicationUserId == user.Id);

                    if (!isMyself)
                    {
                        if (isAnonymous)
                        {
                            posts = posts
                            .Where(p => p.SecurityStatus == (int)SecurityStatuses.Public);
                        }
                        else
                        {
                            var visitorUser = _context.Users.Where(u => u.UserName == visitorUserName).FirstOrDefault();
                            if (visitorUser == null)
                            {
                                posts = posts.Where(p => p.SecurityStatus == (int)SecurityStatuses.Public);
                                break;
                            }                                                   
                            posts = posts.Include(k => k.Groups)
                                .Where(p => p.SecurityStatus == (int)SecurityStatuses.Public ||
                                        (p.SecurityStatus != (int)SecurityStatuses.Private &&
                                          
                                          ((p.SecurityStatus == (int)SecurityStatuses.Friends &&
                                               p.ApplicationUser.FriendsWith.Any(u => u.Id == visitorUser.Id))  ||
                                             //User -> Groups -> Friends
                                            (p.SecurityStatus == (int)SecurityStatuses.Custom &&
                                               p.Groups.Any(gr => gr.Friends.Any(b => b.Id == visitorUser.Id))))                                                                                                   
                                   ));
                            
                        }
                    }
                    else
                    { 
                    
                    }
                   
                   break;
                }

                case ((long)FriendType.FriendsPosts):
                {
                    // check from user side
                    var friendsWith = groupid == 0 ?
                              user.FriendsWith.Select(p => p.UserName).ToList():
                                  group.Friends.Select(p => p.UserName).ToList();
                    posts = posts.Where(p => friendsWith.Any(pp => p.ApplicationUser.UserName == pp));
                 
                    // check from friends sides    
                    posts = posts.Include(k => k.Groups)
                               .Where(p => p.SecurityStatus == (int)SecurityStatuses.Public ||

                                       (p.SecurityStatus != (int)SecurityStatuses.Private &&

                                        ((p.SecurityStatus == (int)SecurityStatuses.Friends &&                                            
                                              p.ApplicationUser.FriendsWith.Any(u => u.Id == user.Id)) ||

                                         (p.SecurityStatus == (int)SecurityStatuses.Custom &&
                                              p.Groups.Any(gr => gr.Friends.Any(fr => fr.Id == user.Id))))
                                  ));

                                                               
                    break;
                }
                case ((long)FriendType.TopPosts):
                {
                    posts = posts.Where(p => p.SecurityStatus == 0);
                    break;

                   
                }

               

            }

            //SideBar
            categories = posts
                               .GroupBy(post => post.CategoryId)
                               .Select(post => post.FirstOrDefault())
                               .Include(post => post.Category)
                               .Select(post => post.Category)
                               .OrderBy(c => c.Name);

            tags = (from p in posts.Include(p => p.Tags)
                    from t in p.Tags
                    select t)
                    .Distinct()
                    .OrderBy(k => k.Name);
                
            //Total & Paging Info
                mv.TotalPosts = posts.Count();
                mv.TotalPages = (int)Math.Ceiling((double)mv.TotalPosts / postsPerPage);
                mv.PageNumber = pageNumber;
                mv.PostsPerPage = postsPerPage;
                mv.IsPrevious = pageNumber > 1;
                mv.IsNext = mv.TotalPages  != pageNumber;
               
            //Posts for page
            posts = posts.OrderByDescending(p => p.PostedOn)
                         .Skip(postsPerPage * (pageNumber - 1))
                         .Take(postsPerPage)
                         .Include(p => p.ApplicationUser)
                         .Include(p => p.UserImage)
                         .Include(p => p.Category)
                         .Include(p => p.PostLikes)
                         .Include(p => p.Comments)
                         .Include( p => p.Replies)
                         .Include(p => p.Groups);
          
            mv.Posts = posts.ToList();
            mv.Category = category;
            mv.Tag = tag;
            mv.Group = group;
            mv.ModelViewSideBar = new ModelViewSideBar
            {
                Categories = categories.ToList(),
                Tags = tags.ToList(),
                LatestPosts = posts,
                PageSize = postsPerPage,
                Category = category,
                Tag = tag,
              //  Group = group
                
            };
           
            return mv;          
        }


        
        
        //===========================================================
        public IEnumerable<Post> GetPosts(int pageNo, int pageSize, string sortColumn,
                           bool sortByAscending)
        {
            //https://blogs.msdn.microsoft.com/alexj/2009/06/02/tip-22-how-to-make-include-really-include/
            IEnumerable<Post> posts = _context.Posts
                       .OrderBy(sortColumn, sortByAscending)
                        .Skip((pageNo - 1) * pageSize)
                        .Take(pageSize)
                        .Include("Category");

            return posts;
        }
        
      
   
        // for by category for paging
        //==========================================
        public IEnumerable<Category> GetCategories(int pageNo, int pageSize, string sortColumn,
                           bool sortByAscending) 
        {
            IEnumerable<Category> categories = _context.Categories
                            .OrderBy(sortColumn, sortByAscending)
                            .Skip((pageNo - 1) * pageSize)
                            .Take(pageSize);
            return categories;
        }

        

        
       // Categories for side bar
        //================================
        public IEnumerable<Category> GetCategories(string username = "") 
        {
            IEnumerable<Category> c;
            if (username == "")
                c = _context.Categories.OrderBy(p => p.Name).ToList();
            else
                c = _context.Posts
                    .Where(post => post.ApplicationUser.UserName == username)
                    .GroupBy(post => post.CategoryId)
                    .Select(post => post.FirstOrDefault())
                    .Include(post => post.Category)
                    .Select(post => post.Category)
                    .OrderBy(category => category.Name)
                    .ToList();

            return c;
        }

     
        //========================================================================

        public int GetUserType(string visitorUserName, string jornalUserName)
        {
            var friendType = _context.Users
                .Where(u => u.UserName == jornalUserName)
                .Include(p => p.FriendsFor)
                .Include(p => p.FriendsWith)
                .Select(u => (u.FriendsFor.Any(k => k.UserName == visitorUserName) ? (int)FriendType.Friend : 0) +
                              (u.FriendsWith.Any(k => k.UserName == visitorUserName) ? (int)FriendType.FriendOf : 0))
                 .FirstOrDefault();
            return friendType;
        }
        //======================================================================

        public int SavePost(Post post)
        {
            int id = 0;
            List<Group> newGroups = null;
            var newTags = post.Tags.Where(t => t.Id != 0).Select(p => p.Id).ToList();

            if (post.SecurityStatus != (int)SecurityStatuses.Custom)
            {
                newGroups = null;
            }
            else
            {
                var groups = post.Groups.Where(p => p.Id != 0).Select(p => p.Id).ToList();
                var groupsFromDB = _context.Groups.Where(gr => groups.Any(g => g == gr.Id)).ToList();
                newGroups = groupsFromDB;
            }
          
            if (post.Id == 0)
            {             
                var tags = _context.Tags.Where(t => newTags.Any(pt => pt == t.Id)).ToList();
                var category = _context.Categories.Find(post.CategoryId);
                var postImage = _context.UserImages.Find(post.UserImageId);
                var username = post.ApplicationUser.UserName;
                var user = _context.Users.Where(u => u.UserName == username).FirstOrDefault();
                post.UserImage = postImage;
                post.Category = category;
                post.Tags = tags;
                post.ApplicationUserId = user.Id;
                post.ApplicationUser = user;
                post.Meta = "ooooooooo";
                post.PostedOn = DateTime.UtcNow;
                post.Groups = newGroups;                
                _context.Posts.Add(post);
                _context.SaveChanges();
                id = post.Id;
            }
            else
            {
                var originalPost = _context.Posts.Where(p => p.Id == post.Id)
                    .Include(p => p.Category)
                    .Include(p => p.Tags)
                    .Include(p => p.Groups)
                    .Include(p => p.UserImage)
                    .Include(p => p.ApplicationUser)
                    .FirstOrDefault();

                if (originalPost == null) return 0;

                    originalPost.Modified = DateTime.UtcNow; 
                    originalPost.UserImageId = post.UserImageId;                
                    originalPost.CategoryId = post.CategoryId;              
                    originalPost.Title = post.Title;                              
                    originalPost.ShortDescription = post.ShortDescription;               
                    originalPost.Description = post.Description;              
                    originalPost.Published = post.Published;
                    originalPost.Title = post.Title;
                    originalPost.SecurityStatus = post.SecurityStatus;

                //Tags
                var tagsToAdd = newTags
                    .GroupJoin(originalPost.Tags,
                                      n => n, o => o.Id, 
                                       (n, o) => new { Key = n, Value = o.Count() })
                    .Where(p => p.Value == 0)
                    .Select(p => p.Key)
                    .ToList();

                var tagsToDelete = originalPost.Tags
                    .GroupJoin(newTags,
                                      o => o.Id, n => n,
                                       (o, n) => new { Key = o.Id, Value = n.Count() })
                    .Where(p => p.Value == 0)
                    .Select(p => p.Key)
                    .ToList();
               
                  foreach  (var t in  tagsToAdd)
                  {
                      originalPost.Tags.Add(_context.Tags.Find(t));
                  }

                  foreach (var t in tagsToDelete)
                  {
                      originalPost.Tags.Remove(_context.Tags.Find(t));
                  }

                  //Groups
                  var groupsToAdd = newGroups == null ? null :
                      newGroups.GroupJoin(originalPost.Groups,
                                        n => n.Id, o => o.Id,
                                         (n, o) => new { Key = n, Value = o.Count() })
                      .Where(p => p.Value == 0)
                      .Select(p => p.Key)
                      .ToList();

                  

                  var groupsToDelete = newGroups == null ? originalPost.Groups.Select(p => p.Id).ToList() :
                          originalPost.Groups.GroupJoin(newGroups,
                                       o => o.Id, n => n.Id,
                                        (o, n) => new { Key = o.Id, Value = n.Count() })
                     .Where(p => p.Value == 0)
                     .Select(p => p.Key)
                     .ToList();

                  if (groupsToAdd != null)
                  {
                      foreach (var t in groupsToAdd)
                      {
                          originalPost.Groups.Add(_context.Groups.Find(t.Id));
                      }
                  }

                  if (groupsToDelete != null)
                  {
                      foreach (var t in groupsToDelete)
                      {
                          originalPost.Groups.Remove(_context.Groups.Find(t));
                      }
                  }
                
                _context.Entry(originalPost).State = EntityState.Modified;
                _context.SaveChanges();
                id = originalPost.Id;
            }
            return id;        
        }

        //==================================================================
        public ModelViewSideBar GetSideBarByUser(string username, int categoryid=0, int tagid = 0, bool onlyPublished = true)
         {

             var user = _context.Users.Where(u => u.UserName == username).FirstOrDefault();
             if (user == null) return null;

             IQueryable<Post> posts = _context.Posts.Where(p => p.ApplicationUserId == user.Id);                       
             if (posts == null || posts.Count() == 0) return new ModelViewSideBar();

             if (onlyPublished)
             {

                 posts = posts.Where(p => p.Published);
             }

             posts = posts.OrderByDescending(p => p.PostedOn);

               var categories = _context.Posts
                            .Where(post => post.ApplicationUser.UserName == username)
                            .GroupBy(post => post.CategoryId)
                            .Select(post => post.FirstOrDefault())
                            .Include(post => post.Category)
                            .Select(post => post.Category)
                            .OrderBy(c => c.Name).ToList();
           
               var tags = (from p in posts.Include(p => p.Tags)
                        from t in p.Tags
                         select t)
                        .Distinct()
                        .OrderBy(k => k.Name).ToList();

               var category = _context.Categories.Find(categoryid);
               var tag = _context.Tags.Find(tagid);

               ModelViewSideBar sb = new ModelViewSideBar() { Categories = categories,
                   Tags = tags, Category = category,Tag = tag,
                   LatestPosts = posts };
               return sb;
             
         }
        //===================================================================
        public ApplicationUser GetUser(string username)
        {
            
            return _context.Users
                .Where(u => u.UserName == username)
                .Include(p => p.FriendGroups)
                .FirstOrDefault();
        }


        //===============================================================
        public string GetScript()
        {           
            string sqlscript = (_context as IObjectContextAdapter).ObjectContext.CreateDatabaseScript();
            return sqlscript;
        }
        

        //====================================================================
        private void AddCommentsJobSuppost()
        {
            var users = _context.Users.Include(u => u.UserImages).OrderBy(p => p.Id).ToList();

            var posts = _context.Posts
                .Where(p => p.SecurityStatus == (int)SecurityStatuses.Public)
                .Include(u => u.ApplicationUser);

            var usersCount = users.Count();
            var postsCount = posts.Count();
            Random random = new Random();

            foreach (var p in posts.ToList())
            {
                var postUserName = p.ApplicationUser.UserName;
                var postImageId = p.UserImageId;                   
                        var userLeftComment = users.Skip(random.Next(0, 15)).FirstOrDefault();
                        var userImages = userLeftComment.UserImages;
                        var imageIds = userImages.Select(img => img.Id).ToArray();
                        var isPostLike = random.Next(1, 4) != 2;
                        Comment comment = new Comment
                        {
                            PostId = p.Id,
                            UserImageId = imageIds[random.Next(0, userImages.Count() - 1)],
                            Body = "comment left by " + userLeftComment.UserName,
                            DateTime = DateTime.Now,
                            UserName = userLeftComment.UserName
                        };
                        _context.Comments.Add(comment);                        
             }
             
            var comments = _context.Comments.ToList();
            var commentsCount =  comments.Count();
            for (int i = 1; i < random.Next(1, 3); i++)
           {
            var userReply = users.Skip(random.Next(0, 15)).FirstOrDefault();
            var cmt = comments.Skip(random.Next(0, commentsCount - 1)).FirstOrDefault();

                Reply reply = new Reply
                {

                    PostId = cmt.PostId,
                    CommentId = cmt.Id,
                    ParentReplyId = null,
                    DateTime = DateTime.Now,
                    UserName = userReply.UserName,
                    UserImageId = (int)userReply.DefaultUserImageId,
                    Body = "comment left by " + userReply.UserName,
                };
                _context.Replies.Add(reply);
           }
            _context.SaveChanges();

    }


        //====================================================================
        public ModelViewPosts GetTopPosts1(int pageNumber, int postsPerPage,
                                        int categoryid, int tagid, long userType)
        {
            long action = userType & (long)FriendType.Action;
            bool isMyself = (userType & (long)FriendType.MySelf) > 0;
            bool isAnonymous = (userType & (long)FriendType.Anonymous) > 0;
            ModelViewPosts mv = new ModelViewPosts();
            Category category = null;
            Tag tag = null;
            Group group = null;           
            IQueryable<Category> categories = null;
            IQueryable<Tag> tags = null;
            IQueryable<Post> posts = null;

            // Check filters
            var lastRatingJob = _context.UserRatingJobs
                           .Where(p => p.PostsCount > 0)
                           .OrderByDescending(t => t.DateTime)
                           .FirstOrDefault();
            if (lastRatingJob == null) return null;

            var postRatings = _context.PostRatings
                           .Where(p => p.PostRatingJobId == lastRatingJob.Id)
                           .Include(j => j.Post)
                           .Where(j => j.Post.SecurityStatus == (long)SecurityStatuses.Public);
                          
            if (categoryid != 0)
            {
                category = _context.Categories.Find(categoryid);
                if (category == null) return null;
                postRatings = postRatings.Where(p => p.Post.CategoryId == categoryid);
            }

            if (tagid != 0)
            {
                tag = _context.Tags.Find(tagid);
                if (tag == null) return null;
                postRatings = postRatings
                    .Include(p => p.Post.Tags)
                    .Where(p => p.Post.Tags.Any(tt => tt.Id == tagid));
            }

            var temp = postRatings
                        .OrderBy(p => p.Id)
                        .Select(p => new { Post = p.Post, TopOrder = p.TopDenseRank, TopRating = p.Rating });                        
           posts = postRatings
                       .OrderBy(p => p.Id)
                       .Select(p => p.Post);

            //SideBar
            categories = posts
                         .GroupBy(post => post.CategoryId)
                         .Select(post => post.FirstOrDefault())
                         .Include(post => post.Category)
                         .Select(post => post.Category)
                         .OrderBy(c => c.Name);

            tags = ( from p in posts.Include(p => p.Tags)
                     from t in p.Tags
                     select t).Distinct()
                              .OrderBy(k => k.Name);

            //Total & Paging Info
            mv.TotalPosts =  posts.Count();
            mv.TotalPages = (int)Math.Ceiling((double)mv.TotalPosts / postsPerPage);
            mv.PageNumber = pageNumber;
            mv.PostsPerPage = postsPerPage;
            mv.IsPrevious = pageNumber > 1;
            mv.IsNext = mv.TotalPages != pageNumber;

            //Posts for page
            posts = posts
                     .Skip(postsPerPage * (pageNumber - 1))
                     .Take(postsPerPage)
                     .Include(p => p.PostRatings)
                     .Include(p => p.ApplicationUser)
                     .Include(p => p.UserImage)
                     .Include(p => p.Category)
                     .Include(p => p.PostLikes)
                     .Include(p => p.Comments)
                     .Include(p => p.Replies)
                     .Include(p => p.Groups);

            var postsE = posts
                    .Select(p => new ModelViewPostShort
                    {
                        Id = p.Id,
                        Title = p.Title,
                        PostedOn = p.PostedOn,
                        ShortDescription = p.ShortDescription,
                        CategoryId = p.CategoryId,
                        Category = p.Category,
                        Tags = p.Tags,
                        ApplicationUserId = p.ApplicationUserId,
                        ApplicationUser = p.ApplicationUser,
                        UserImageId = p.UserImageId,
                        UserImage = p.UserImage,
                        PostLikesCount = p.PostLikes
                                                 .Where(pp => pp.Like).Count(),
                        PostDislikesCount = p.PostLikes
                                                 .Where(pp => pp.Dislike).Count(),
                        CommentsCount = p.Comments.Count() + p.Replies.Count(),
                        Rating = p.PostRatings.Where(pp => pp.PostRatingJobId == lastRatingJob.Id)
                                                 .FirstOrDefault().Rating,
                        TopOrder = p.PostRatings.Where(pp => pp.PostRatingJobId == lastRatingJob.Id)
                                                 .FirstOrDefault().TopDenseRank,
                    });

            mv.PostsShort = postsE.ToList();
            mv.Category = category;
            mv.Tag = tag;
            mv.Group = group;

            mv.ModelViewSideBar = new ModelViewSideBar
            {
                Categories = categories.ToList(),
                Tags = tags.ToList(),
                LatestPosts = posts,
                PageSize = postsPerPage,
                Category = category,
                Tag = tag,
            };

            return mv;
        }

       

    }
}