﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.ViewModels;
using LJ.Models.User;

namespace LJ.Repository.BlogRepository
{
    public interface IBlogRepository<T, TId> where T : class
    {       
        string GetScript();
        IEnumerable<Tag> GetTags(string username ="");
        IEnumerable<Category> GetCategories(string username ="");

        int TotalPosts();
        int TotalPostsByUser(string username, bool published = true);

        IEnumerable<T> GetAll(int pageNo, int pageSize);
        Post GetById(TId id);
            
        Category GetCategory(int id);
        Tag GetTag(int id);

        int TotalPosts(bool checkIsPublished);
        int TotalTags();
        int TotalCategories();
        IEnumerable<Post> GetPosts(int pageNo, int pageSize, string sortColumn,
                            bool sortByAscending);
        IEnumerable<Tag> GetTags(int pageNo, int pageSize, string sortColumn,
                           bool sortByAscending);
        IEnumerable<Category> GetCategories(int pageNo, int pageSize, string sortColumn,
                           bool sortByAscending);

        int AddTag(Tag tag);
        int SaveTag(Tag entity);
        int DeleteTag(int id);

        int AddCategory(Category tag);
        int SaveCategory(Category entity);
        int DeleteCategory(int id);

        int AddPost(Post post);
        int SavePost(Post entity);
        int DeletePost(int id);

        ApplicationUserImage GetImageBy(int id);
        ApplicationUserImage GetDefaultImageBy(string username);
        IEnumerable<ApplicationUserImage> GetImagesBy(string username);
        
       IEnumerable<CommentViewModel> GetPostComments(Post post);
       List<CommentViewModel> GetReplies(Comment comment);
       List<CommentViewModel> GetChildReplies(int parentReplyId);

       Post GetPostById(int id);
       Comment GetCommentById(int id);
       Reply GetReplyById(int id);

       int AddNewReply(Reply reply);
       int AddNewComment(Comment comment);

       int DeletePost(int postId, string userName);
       DeleteInfo DeleteComment(int commentId, string userName);
       DeleteInfo DeleteReply(int replyId, string userName);

       int EditReply(int id, string body, string username, int userimageid);
       int EditComment(int id, string body, string username, int userimageid);

       int LikeDislikeCount(int likeordislike, int id);
       int UpdateLikes(int id, string user, int likeordislike);
       IEnumerable<string> GetListLikes(int id, int likeordislikeint);

      ModelViewPosts GetPosts1(int pageNumber, int postsPerPage,
                           int categoryid, int tagid, string username,
                            long userType, int groupid, int srcurity, string visitorUserName="");
        IEnumerable<int> GetPostIds(string username, int categoryid, int tagid, string visitorUserName = "");
        ModelViewSideBar GetSideBarByUser(string username, int categoryid = 0, int tagid = 0, bool onlyPublished = true);

        int GetUserType(string visitorUserName, string jornalUserName);
        ApplicationUser GetUser(string username);
        PostViewModel GetPostExtraInfo(PostViewModel vm, int categoryid, int tagid, string visitorUserName = "");

        ModelViewPosts GetTopPosts1(int pageNumber, int postsPerPage,
                                       int categoryid, int tagid, long userType);
       
        
       

      




       
       
    

    }
}