﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Models.User;

namespace LJ.Models.Blog
{
    public class CommentLike
    {
        [Key]      
        public int Id { get; set; }

        [ForeignKey("Comment")]
        public int CommentId { get; set; }
        public virtual Comment Comment { get; set; }

        public virtual ApplicationUser User { get; set; }

        public bool Like { get; set; }
        public bool Dislike { get; set; }
    }
}