﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Models.User;

namespace LJ.Models.Blog
{
    public enum SecurityStatuses
    {
        Public = 0, 
        Friends = 1, 
        Private = 2, 
        Custom = 3         
    }
}