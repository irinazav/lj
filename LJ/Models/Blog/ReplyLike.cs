﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Models.User;

namespace LJ.Models.Blog
{
    public class ReplyLike
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Reply")]
        public int ReplyId { get; set; }
        public virtual Reply Reply { get; set; }

        public bool Like { get; set; }
        public bool Dislike { get; set; }
        public virtual ApplicationUser User { get; set; }

        
    }
}