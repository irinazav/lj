﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;




namespace LJ.Models.Blog
{
    public class UserRatingJob
    {   [Key]
        public int Id { get; set; }
        public int JobType { get; set; }
        public DateTime DateTime { get; set; }
        public int UsersCount { get; set; }
        public int PostsCount { get; set; } 

        ICollection<UserRating> UserRatings { get; set; }
        [InverseProperty("PostRatingJob")]
        ICollection<PostRating> PostRatings { get; set; } 
    }
}

