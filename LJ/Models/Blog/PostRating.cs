﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;




namespace LJ.Models.Blog
{
    public class PostRating
    {   [Key]
        public int Id { get; set; }

        public int PostRatingJobId { get; set; }
        [ForeignKey("PostRatingJobId")]
        public UserRatingJob PostRatingJob { get; set; }

        public string ApplicationUserId { get; set; }
        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser User { get; set; }

        public int PostId { get; set; }
        [ForeignKey("PostId")]
        public Post Post { get; set; }
        
        public int Rating { get; set; }
        public DateTime DateTime { get; set; }
        public DateTime FromDateTime { get; set; }
        public DateTime ToDateTime { get; set; }
        public int TopDenseRank { get; set; }

        //Comments
        public int CommentsCount { get; set; }

        //Likes
        public int LikesCount { get; set; }
        public int DislikesCount { get; set; }

        //Visits
        public long SpentTimeMC { get; set; }
        public int VisitsCount { get; set; }
        public int VisitorsCount { get; set; }
        public int UsersCount { get; set; }
        public int AnonymousCount { get; set; }

    }
}