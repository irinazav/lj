﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;

namespace LJ.Models.Blog
{
    public class DeleteCommenReplyInfo
    {
        public int Id { get; set; }
        public bool Deleted { get; set; }
        public string DateTime { get; set; }        
    }

    public class DeleteInfo
    {
        public DeleteInfo()
        {
            ReplyIdsForDelete = new List<DeleteCommenReplyInfo>();
            CommentIdsForDelete = new List<DeleteCommenReplyInfo>();
            DeletedReplyText = "This reply has been deleted";
            DeletedCommentText ="This comment has been deleted";
             
        }
        public IList<DeleteCommenReplyInfo> ReplyIdsForDelete {get; set;}
        public IList<DeleteCommenReplyInfo> CommentIdsForDelete { get; set; }

        public  string DeletedReplyText { get; set; }
        public  string DeletedCommentText { get; set; }
    
    }

}