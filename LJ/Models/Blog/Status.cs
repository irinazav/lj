﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.Models.Blog
{
    public enum Status { 
        Deleted = 1, 
        Edited = 2 
    };
    
   
    public static class StatusNames
    {
        public static string GetStatusName(int id)
        {
            foreach (int i in Enum.GetValues(typeof(Status)))
            {
                if ((id & i) > 0)
                    return Enum.GetName(typeof(Status), i) +" ";
            }
           return "";
       }
    }
}
