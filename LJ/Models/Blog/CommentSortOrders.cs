﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Models.User;

namespace LJ.Models.Blog
{
    public enum CommentSortOrders
    {
        Earliest = 1, 
        Oldest = 2, 
        Best = 4, 
        Worst = 8, 
        Heaviest = 16, 
        Lightest = 32
    }
}