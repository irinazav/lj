﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.Models.Visitor
{
    public enum ActionType
    {
        Report = 1, Blog= 2, Edit = 4, Profile = 8, See = 128, Log = 256,
        User = 16, Website = 32, Admin = 64, 
        UserReport= 1 + 16,
        WebsiteReport= 1 + 32, 
        AdminReport = 1 + 64,
        ManageProfile = 8 + 4,

        SeeBlog = 2 + 128,
        EditBlog = 2 + 4,

        SeeProfile = 8 + 128,
        EditProfile = 8 + 4,
        SeeWebsite = 8 + 32,
    }
}