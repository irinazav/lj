﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.Models.Visitor
{
    public class GeoLocation
    {
         public string ip {get; set;}
         public string country_code {get; set;}
         public string country_name {get; set;}
         public string region_code {get; set;}
         public string city {get; set;}
         public string zip_code {get; set;}
         public decimal latitude {get; set;}
         public decimal longitude {get; set;}
         public  int metro_code  {get; set;}


        /*Sample  http://freegeoip.net/
          "ip":"192.30.253.113",
          "country_code":"US",
          "country_name":"United States",
          "region_code":"CA","region_name":"California",
          "city":"San Francisco",
          "zip_code":"94107",
          "time_zone":"America/Los_Angeles",
          "latitude":37.7697,
          "longitude":-122.3933,
          "metro_code":807}*/
    }
}