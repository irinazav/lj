﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LJ.Models.Visitor
{
    public class GeoLocationRestService
    {
        readonly string uri = "http://freegeoip.net/json/";

        public GeoLocation GetGeoLocation(string ip)
        {

            using (WebClient webClient = new WebClient())
            {

                return JsonConvert.DeserializeObject<GeoLocation>(
                        webClient.DownloadString(uri + ip)
                );
            }
        }

        public async Task<GeoLocation> GetGeoLocationAsync(string ip)
        {

            using (HttpClient httpClient = new HttpClient())
            {

                return JsonConvert.DeserializeObject<GeoLocation>(
                       await httpClient.GetStringAsync(uri + ip)
                );
            }
        }
    }
}