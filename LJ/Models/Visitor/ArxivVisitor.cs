﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Concurrent;
using System.Web.Security;
using System.Web.Mvc;
using LJ.Models.Visitor;
using LJ.Models.User;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.Models.Visitor
{
    public class ArxivVisitor
    {
        [Key] 
        public int Id { get; set; }
        public int VisitorsCount { get; set; }
        public int UsersCount { get; set; }
        public DateTime Date { get; set; }
 
    }
}