﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LJ.Repository.BlogRepository;

namespace LJ.Models.User
{
    public class ApplicationUserInfo
    {
        [Key]
        public int Id { get; internal set; }
       // public DateTime BirthDate { get; internal set; }
        [Display(Name = "First Name")]
        public string LastName { get; internal set; }
        [Display(Name = "Last Name")]
        public string FirstName { get; internal set; }
        [Display(Name = "Country")]
        public string Country { get; internal set; }
        [Display(Name = "City")]
        public string City { get; internal set; }


        [Display(Name = "About")]
        [DataType(DataType.MultilineText)] 
        public string About { get; internal set; }
        [Display(Name = "Account is private")]
        public bool IsNotPublic { get; internal set; }
       // public DateTime EmailLinkDate { get; internal set; }
        //public DateTime JoinDate { get; internal set; }
        //public DateTime LastLoginDate { get; internal set; }
       
        


       



        
    }
}