﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using LJ.Models.Blog;
using LJ.Models.Visitor;
using LJ.Repository.BlogRepository;
using System.Linq;

namespace LJ.Models.User
{
    public class ApplicationUser : IdentityUser
    {      
        static ApplicationUser()
        {
            MessageForFriendType = new Dictionary<int, string>();
            MessageForFriendType.Add((int)FriendType.MySelf, ", you are viewing your journal");
            MessageForFriendType.Add((int)FriendType.FriendOf, " -  added you as a friend");
            MessageForFriendType.Add((int)FriendType.NoConnection, " -  no connection with this user");
            MessageForFriendType.Add((int)FriendType.Friend, " — in your friends list ");
            MessageForFriendType.Add((int)FriendType.MutualFriend, " - is your mutual friend");  
        }

        [NotMapped]
        public static Dictionary<int, string> MessageForFriendType;
        [NotMapped]
        public static Dictionary<int, string> IconAddRemoveByFriendType;

        public string password2 { get; set; } // for development
        [Display(Name = "Account created")]
        public DateTime Created { get; set; }

        //[SqlDefaultValue(DefaultValue = "1")]
        public int? DefaultUserImageId { get; set; }

        [Display(Name = "Default userpic")]
        [ForeignKey("DefaultUserImageId")]
        public virtual ApplicationUserImage DefaultUserImage { get; set; }

        [Display(Name = "Default Userpic size")]
        public string DefaultUserImageSize { get; internal set; }

        [Display(Name = "Default posts per page")]
        public int PostPerPageNumber { get; internal set; }

        public int? UserInfoId { get; set; }
        [ForeignKey("UserInfoId")]
        public virtual ApplicationUserInfo ApplicationUserInfo { get; set; }

        [Display(Name = "Userpics")]
        public virtual ICollection<ApplicationUserImage> UserImages { get; set; }

        [InverseProperty("Post")]
        public virtual IEnumerable<Post> Posts { get; set; }

        public virtual ICollection<PostLike> PostLikes { get; set; }
        public virtual IEnumerable<CommentLike> CommentLikes { get; set; }
        public virtual IEnumerable<ReplyLike> ReplyLikes { get; set; }

        public virtual ICollection<ApplicationUser> FriendsWith { get; set; }//I'm friendsWith       
        public virtual ICollection<ApplicationUser> FriendsFor { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<UserRating> UserRatings{ get; set; } 
       
        [InverseProperty("User")]
        public virtual ICollection<FriendArxiv> FriendsWithArxiv { get; set; } //friends Added, Deleted by user

        [InverseProperty("User")]
        public virtual ICollection<ApplicationVisitor> Visitors { get; set; }

        public virtual ICollection<Group> FriendGroups { get; set; }
        public virtual ICollection<Group> FriendOfGroups { get; set; }
      

       
        
       

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {           
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            userIdentity.AddClaim(new Claim(ClaimTypes.GivenName, this.DefaultUserImageId == null ? string.Empty : this.DefaultUserImageId.ToString()));

            var imageIdsClaim = new Claim("UserImageIds", this.UserImages == null ?
                string.Empty : string.Join(",", this.UserImages.Select(p => p.Id).ToArray()));

            userIdentity.AddClaim(imageIdsClaim);

            userIdentity.AddClaim(new Claim("UserId", this.Id == null ? string.Empty : this.Id.ToString()));
            
            return userIdentity;
        }



    }    
}