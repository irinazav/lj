﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace  LJ.Models.User
{
    public enum FriendType :long 
    {
       Anonymous = 1, 
       MySelf = 2, 
       Friend = 4, 
       NoConnection = 8,
       
       FriendOf = 16, MutualFriend = ( 4 | 16),

       CanAdd = (1 | 2 | 4), // can add as a friend to friendlist, check via & == 0   (in list who can't add)     
       CanDelete = 4, // can delete friend from friend list,  check via & > 0

        WithoutUserPanel = 32, 
        
        //Navigation type
        Post = 64,
        Posts =128,
        Navigation = (long)Post | (long)Posts,
        
        //RecordSet type
        User = 256,
        Friends = 256*2,
        Top = (long)WithoutUserPanel,
        RecordSet = (long)User |(long)Friends |(long)Top,
       
        //Action name
        UserPost = (long)User | (long)Post, //user post : my or oth
        UserPosts =(long)User | (long)Posts, //user posts : my or oth
        FriendsPost = (long)Friends | (long)Post,
        FriendsPosts = (long)Friends | (long)Posts, //friend posts: my or other
        TopPost = (long)Top | (long)Post,
        TopPosts = (long)WithoutUserPanel | (long)Posts,
        Action = (long)UserPost | (long)UserPosts | (long)FriendsPosts | (long)TopPosts,
        
        // Filters
        ByCategory = 256*4, 
        ByTag =256*8,
        ByTags = 256 * 16,
        Filter = (long)ByCategory | (long)ByTag | (long)ByTags           
    }
}