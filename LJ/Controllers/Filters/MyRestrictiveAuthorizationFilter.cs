﻿using System.Web;
using Hangfire.Annotations;
using Hangfire.Dashboard;

namespace LJ.Filters

{
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            //can add some more logic here...
           // return HttpContext.Current.User.Identity.IsAuthenticated;
            var username = HttpContext.Current.User.Identity.Name;
            return username == "Marx" || username == "Hirschfeld";
        }
    }
}