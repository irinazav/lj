﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Text;
using LJ.ViewModels;
using LJ.Models.Blog;
using LJ.Repository.BlogRepository;
using LJ.Infrastructure;

namespace LJ.Controllers
{
    public class AdminController : Controller
    {
         private IBlogRepository<Post,int> _blogRepository;
         
         public AdminController(IBlogRepository<Post, int> blogRepository)
        {
            _blogRepository = blogRepository;
           

        }


         public ActionResult Index()
         {
             return View();
         }
       
        public ActionResult Manage()
        {
            return View();
        }



        public ContentResult GetPosts(GridViewModel gr)
        {
            var posts = _blogRepository.GetPosts(gr.page, gr.rows,
                gr.sidx, gr.sord == "asc").ToList();
            var totalPosts = _blogRepository.TotalPosts(false);

            return Content(JsonConvert.SerializeObject(new
            {
                page = gr.page,
                records = totalPosts,
                rows = posts,
                total = Math.Ceiling (Convert.ToDouble(totalPosts) / gr.rows)
            }), "application/json");
        }


        public ContentResult GetTags(GridViewModel gr)
        {
            var tags = _blogRepository.GetTags(gr.page, gr.rows,
                gr.sidx, gr.sord == "asc");
            var total = tags.Count();

            return Content(JsonConvert.SerializeObject(new
            {
                page = gr.page,
                records = tags,
                rows = tags,
                total = Math.Ceiling(Convert.ToDouble(total) / gr.rows)
            }), "application/json");
        }

        public ContentResult GetCategories(GridViewModel gr)
        {
            var c = _blogRepository.GetCategories(gr.page, gr.rows,
                gr.sidx, gr.sord == "asc");
            var total = c.Count();

            return Content(JsonConvert.SerializeObject(new
            {
                page = gr.page,
                records = total,
                rows = c,
                total = Math.Ceiling(Convert.ToDouble(total) / gr.rows)
            }), "application/json");
        }

        
        // Delete ----------------------
        [HttpPost]
        public ContentResult DeletePost(int id)
        {
            string json;
            int result = _blogRepository.DeletePost(id);
            if (result != 0)
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = result,
                    success = true,
                    message = "Post is deleted successfully."
                });
            }
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = 0,
                    success = false,
                    message = "Failed to delete the post."
                });
            }
            return Content(json, "application/json");
        }

        [HttpPost]
        public ContentResult DeleteTag(int id)
        {
            string json;
            int result = _blogRepository.DeleteTag(id);
            if (result != 0)
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = result,
                    success = true,
                    message = "Tag is deleted successfully."
                });
            }
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = 0,
                    success = false,
                    message = "Failed to delete the tag."
                });
            }
            return Content(json, "application/json");
        }

        [HttpPost]
        public ContentResult DeleteCategory(int id)
        {
            string json;
            int result = _blogRepository.DeleteCategory(id);
            if (result != 0)
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = result,
                    success = true,
                    message = "Category deleted successfully."
                });
            }
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = 0,
                    success = false,
                    message = "Failed to delete the category. Must to delete posts assigned to categerory"
                });
            }
            return Content(json, "application/json");
        }


        //Edit ------------------------
        [HttpPost]
        public ActionResult EditTag(Tag tag)
        {
            string json;
            int id;

            if (ModelState.IsValid)
            {
                if (tag.Id == 0) id = _blogRepository.AddTag(tag);
                else _blogRepository.SaveTag(tag);

                json = JsonConvert.SerializeObject(new
                {
                    id = tag.Id,
                    success = true,
                    message = "Post added successfully."
                });
            }
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = 0,
                    success = false,
                    message = "Failed to add the post."
                });
            }

            return Content(json, "application/json");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult EditPost(Post post)
        {
            string json;
            int id;
           // ModelState.CleanSometing("Post.Category.Name", "Post.Category.Url");
            ModelState.Clear();

            if (TryValidateModel(post))
            {
                if (post.Id == 0) id = _blogRepository.AddPost(post);
                else _blogRepository.SavePost(post);

                json = JsonConvert.SerializeObject(new
                {
                    id = post.Id,
                    success = true,
                    message = "Post saved successfully."
                });
            }
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = 0,
                    success = false,
                    message = "Failed to save the post."
                });
            }

            return Content(json, "application/json");
        }


        
        [HttpPost]
        public ActionResult EditCategory(Category category)
        {
            string json;
            int id;

            if (ModelState.IsValid)
            {
                if (category.Id == 0) id = _blogRepository.AddCategory(category);
                else _blogRepository.SaveCategory(category);

                json = JsonConvert.SerializeObject(new
                {
                    id = category.Id,
                    success = true,
                    message = "Category added successfully."
                });
            }
            else
            {
                json = JsonConvert.SerializeObject(new
                {
                    id = 0,
                    success = false,
                    message = "Failed to add the category."
                });
            }

            return Content(json, "application/json");
        }


      


        // Html ----------------------

        public ContentResult GetCategoriesHtml()
        {
            var categories = _blogRepository.GetCategories().OrderBy(s => s.Name);

            var sb = new StringBuilder();
            sb.AppendLine(@"<select>");

            foreach (var category in categories)
            {
                sb.AppendLine(string.Format(@"<option value=""{0}"">{1}</option>",
                    category.Id, category.Name));
            }

            sb.AppendLine("<select>");
            return Content(sb.ToString(), "text/html");
        }
        //---------------------------------------
        public ContentResult GetTagsHtml()
        {
            var tags = _blogRepository.GetTags().OrderBy(s => s.Name);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(@"<select multiple=""multiple"">");

            foreach (var tag in tags)
            {
                sb.AppendLine(string.Format(@"<option value=""{0}"">{1}</option>",
                    tag.Id, tag.Name));
            }

            sb.AppendLine("<select>");
            return Content(sb.ToString(), "text/html");
        }

        //--------------------------------------

        public ActionResult GetHangfireDashboard()
        {
            return View();
        }
        //--------------------------------------

        
        
    }
}