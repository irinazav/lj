﻿


<h5><b>Фотография из немецкого журнала</b></h5>

<i>Из книги <a href="http://books.google.com/books?id=pRaLAAAAQBAJ&amp;pg=PT657&amp;dq=when+we+were+prisoners+in++tanker+uckermark&amp;hl=en&amp;sa=X&amp;ei=n4iTUs_PLfPmsATlvoGABw&amp;ved=0CC8Q6AEwAA#v=onepage&amp;q=when%20we%20were%20prisoners%20in%20%20tanker%20uckermark&amp;f=false">"Ambushed Under the Southern Cross" </a>  George W Duffy</i>
<br><br>
В 1942 году  <a href="http://www.axpow.org/duffygeorge.htm">Джордж Даффи</a>, моряк американского торгового флота,  был пленником на борту немецкого танкерa "Uckermark".
<br><br>
<i>Джордж Даффи 1941 год</i>
<br><br>
<a href="http://www.axpow.org/duffygeorge.htm"><img src="http://farm4.staticflickr.com/3720/11138667944_4034b4e877_o.gif" width="120" height="180"></a>
<br><br>

Ему случайно попался в руки  журнал иллюстрированных новостей <a href="http://en.wikipedia.org/wiki/Berliner_Illustrirte_Zeitung">Berliner Illustrierte Zeitung</a>, где целая страница  была посвящена потоплению немецкой подводной лодкой американского танкера. 
<br><br>

<i><b>Berliner Illustrierte Zeitung № 27, 1942</b></i>
<br>
<i>Сверху - торпедированный корабль, слева -  капитан немецкой подводной лодки Рейнхард Хардеген, справа - спасшиеся моряки с американского танкера</i>
<br><br>
<a href="http://www.flickr.com/photos/72635554@N08/10057232056/" title="Untitled by elefant5959, on Flickr" rel="nofollow"><img src="http://farm8.staticflickr.com/7436/10057232056_00b7a1aa9b.jpg" alt="Untitled" height="500" width="364"></a>
<br><br>
Даффи вырвал страницу с фотографиями  и сумел  сохранить  ее до окончания войны, пройдя через немецкий и японский плен. Потом  он 40 лет  искал информацию о людях, запечатленных на снимке. Лишь в  1981 году  удалось увеличить  фотографию так, что стало возможным прочитать название корабля - <b>Muskogee</b>. 
<br><br>
<i>Американский  танкер "Маскоджи", 1940 год</i>
<br><br>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/25866094824/in/dateposted-public/" title="Untitled"><img src="https://farm2.staticflickr.com/1527/25866094824_53028c6646_o.jpg" width="550" height="221" alt="Untitled"></a>
<br><br>



Судьба американского танкера  "Маскоджи" к тому времени уже была известна  - 22 марта 1942 года он  был потоплен немецким подводным крейсером <a href="http://ru.wikipedia.org/wiki/U-123_%281940%29">"U-123"</a>.
<br><br>
<i>Подводный крейсер "U-123", 1942 год</i>
<br><br>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/26538239401/in/dateposted-public/" title="Untitled"><img src="https://farm2.staticflickr.com/1708/26538239401_8e77f4cef8.jpg" width="550&quot;" alt="Untitled"></a>
<br><br><br>
Из книги Бийра Кеннета "Cуда-ловушки против подводных лодок - секретный проект Америки":
<br>
<div style="background:WhiteSmoke; padding:7px;">
<i>Воскресенье не было днем отдыха. В 09:35 по местному времени после проверки балластной системы и систем погружения и управления U-123 всплыла. Облачное небо и хорошая видимость позволили относительно безопасно идти по поверхности и подзарядить аккумуляторы. Через 12 минут вахтенный офицер на мостике, лейтенант Шюлер, доложил о появлении цели. U-123 принялась ее преследовать и в 12:57 местного времени выпустила приводимую в движение сжатым воздухом торпеду типа G7a с расстояния 600 метров, поразив судно ударом в машинное отделение. Через 16 минут американский танкер «Маскоджи» медленно исчез в волнах.  </i></div>
<br>
Из 34 членов экипажа удалось спастись только семи, изображенным на снимке, но из них  никто не выжил.<a name="3"></a><br/>
По фотографии удалось идентифицировать  только трeх человек:<br/>
Натаниэль Д. Фостер (третий помощник капитана), один из героев  книги <a href="http://www.amazon.com/Mathews-Men-Brothers-Against-Hitlers/dp/0525428151/ref=sr_1_1?s=books&amp;ie=UTF8&amp;qid=1463284753&amp;sr=1-1&amp;keywords=The+Mathews+Men%3A+Seven+Brothers+and+the+War+Against+Hitler%27s+U-boats">"The Mathews Men"</a>, - третий слева,<br> 
Морган Дж. Финикэйн (старший помощник капитана) - в центре в спасатeльном жилете,<br/>
Энтони Дж. Соуз (матрос) - человек, который приложил руки ко рту.<br/>  
<br><br>

<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/26190885502/in/dateposted-public/" title="Untitled"><img src="https://farm2.staticflickr.com/1659/26190885502_0e92c79330_z.jpg" width="640" height="438" alt="Untitled"></a>

<br><br><br>
 В 1976 году была создана общественная организация для сбора средств на создание памятника всем морякам  торгового флота Америки, погибшим в море с 1775 года по наши дни. Комитетом для отбора вариантов эскиза памятника руководил Чарльз Гибсон, капитан торгового флота и независимый эксперт по морской истории. Он предложил взять за основу мемориала  фотографию спасшихся членов команды танкера "Muskogee", потому что <span style="background:WhiteSmoke; "><i><b>"это самое реалистичное изображение, когда-либо запечатленное на фотопленке, которое наиболее полно характеризует жертв моря. Оно соответствует любому периоду морской истории."</b></i></span>
<br><br><br>

Мемориал американским морякам торгового флота был установлен  в 1991 году в Бэттери-Парк Нью-Йорка. Раньше на его месте располaгалась артиллeрийская батарея, которая в 17-18 веках защищала город и дала имя современному парку.
<br><br>
<i>Скульптор  мемориала Марисоль Эскобар и Джордж Даффи  в 1991 году. </i>
<br><br>
<a href="http://www.flickr.com/photos/72635554@N08/10057162344/" title="Untitled by elefant5959, on Flickr"><img src="http://farm8.staticflickr.com/7369/10057162344_594d0ef124_n.jpg" width="307" alt="Untitled"></a>
<br><br><br><br>


Исторических фотографий, которые были воспроизведены в виде скульптур, не так уж много, cамые знаменитые из них из них:<br>
<i> <a href="https://ru.wikipedia.org/wiki/%D0%9E%D0%B1%D0%B5%D0%B4_%D0%BD%D0%B0_%D0%BD%D0%B5%D0%B1%D0%BE%D1%81%D0%BA%D1%80%D1%91%D0%B1%D0%B5">Обед на небоскрёбе</a> -  29 сентября 1932 года,<br> <a href="https://ru.wikipedia.org/wiki/%D0%92%D0%BE%D0%B4%D1%80%D1%83%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5_%D1%84%D0%BB%D0%B0%D0%B3%D0%B0_%D0%BD%D0%B0%D0%B4_%D0%98%D0%B2%D0%BE%D0%B4%D0%B7%D0%B8%D0%BC%D0%BE%D0%B9">Поднятие флага над Иводзимой</a> -  23 февраля 1945 года,<br> <a href="https://ru.wikipedia.org/wiki/%D0%94%D0%B5%D0%BD%D1%8C_%D0%9F%D0%BE%D0%B1%D0%B5%D0%B4%D1%8B_%D0%BD%D0%B0%D0%B4_%D0%AF%D0%BF%D0%BE%D0%BD%D0%B8%D0%B5%D0%B9_%D0%BD%D0%B0_%D0%A2%D0%B0%D0%B9%D0%BC%D1%81-%D1%81%D0%BA%D0%B2%D0%B5%D1%80">День Победы над Японией на Таймс-сквер</a> - 14 августа 1945 года.</i>
<br><br>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/26001477474/in/dateposted-public/" title="Untitled"><img src="https://farm2.staticflickr.com/1616/26001477474_e08e1d75e4_z.jpg" width="640" height="353" alt="Untitled"></a>
<br><br><br><br>

В отличиe от традиционных воплощений <a href="http://latander.livejournal.com/381201.html">"из фотографии в скульптуру"</a> Марисоль Эскобар не стала делать  точную копию  снимка.
<br><br>
<a href="http://www.flickr.com/photos/72635554@N08/10057295533/" title="Untitled by elefant5959, on Flickr" rel="nofollow"><img src="http://farm8.staticflickr.com/7456/10057295533_ab2d9c74ac.jpg" alt="Untitled" h="h" width="400"></a>
<br><br><br><br>
  Она лишь перенесла  черты лица 4-х спасшихся членов команды и позаимствовала позу моряка, который кричит, приложив руки ко рту. 
<br><br>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/25749927764/in/dateposted-public/" title="Untitled"><img src="https://farm2.staticflickr.com/1681/25749927764_8cef08681a_z.jpg" width="400" alt="Untitled"></a>
<br><br><br><br>

А также  добавила драматизма, который остался за кадром фотографии.
<br><br>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/50463938@N04/26288576491/in/dateposted-public/" title="Untitled"><img src="https://farm2.staticflickr.com/1666/26288576491_bf4251e000_c.jpg" width="400" alt="Untitled"></a>
<br>








