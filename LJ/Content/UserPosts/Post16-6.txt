﻿<h5><b>"Слёт птиц", 2001</b></h5>

 Валтер Мартин  и Палома Маноз, бронза  
<br/>
Станция: "Canal Street-Holland Tunnel"  линии A ,C , E (Манхэттен)
<br/><br/>

<a href="http://www.martin-munoz.com/main.html">Валтер Мартин (1953 Норфлок, Виржиния) и Палома Маноз (1965, Мадрид)</a> стали совместно работать с 1993 года. Самые известные из их работ - скульптуры и фотографии скульптур, "противопоставляющих спокойствие пейзажа тревожному предчувствию или ужасным сценам" -
серии "<a href="http://www.martin-munoz.com/recent/indexTRAVELERS.html">Путешественники</a>" и "<a href="http://www.martin-munoz.com/recent/indexISLANDS.html">Острова</a>".

<br/><br/>
<a href="http://www.flickr.com/photos/51644460@N06/4791746629/" title="bird3 by ign2009, on Flickr"><img src="http://farm5.static.flickr.com/4139/4791746629_fe4fa36d2f_m.jpg" width="215" height="191" alt="bird3" /></a>
<br/><br/><br/><br/>


Уолтер Мартин и Палома Маноз превратили станцию "Canal Street" в сцену, которая, на первый взгляд, напоминает  фильм Альфреда Хичкока "Птицы". С той разницей, что птицы на станции выглядят скорее добродушными, чем угрожающими.<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4870404546/" title="ka1 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4117/4870404546_1c8c80faa1.jpg" width="500" height="340" alt="ka1" /></a>
<br/>
<br/>
<br/>

"Слёт птиц" очень оживил пространство, совершенно лишенное природы в этом коммерчeском районе.
Ходят упорные слухи, что на станции есть место, над которым стоит махнуть рукой и услышись пение птиц.  Увы, ни в одном интервью с авторами про это нет ни слова, работники станции тоже ничего не знают. 
<br/>
<a href="http://www.flickr.com/photos/51644460@N06/4751007984/" title="a2 by ign2009, on Flickr"><img src="http://farm5.static.flickr.com/4139/4751007984_7e919da557.jpg" width="500" height="375" alt="a2" /></a>
<br/>
<br/>
<br/>

Натурщиков для проекта предоставил Музей Натуральной Истории.
<br/> 
<a href="http://www.flickr.com/photos/51644460@N06/4751156586/" title="a4 by ign2009, on Flickr"><img src="http://farm5.static.flickr.com/4093/4751156586_ce56bbc3ab.jpg" width="500" height="316" alt="a4" /></a>
<br/>
<br/>
<br/>

174 черных дрозда и 7 ворон сидят и разглядывают  толпы людей, ежедневно проходящих мимо них.
<br/>
<a href="http://www.flickr.com/photos/50463938@N04/4892232458/" title="a0 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4114/4892232458_09a7da7e8d.jpg" width="500" height="339" alt="a0" /></a>
<br/>
<br/>
<br/>

Уолтер Мартин и Палома Маноз: "Птицы - очень социальные существа, пассажиры могут узнать себя в их живом общении." <br/>
<a href="http://www.flickr.com/photos/50463938@N04/4894581729/" title="bird6 by elefant59, on Flickr"><img src="http://farm5.static.flickr.com/4114/4894581729_e7d7cdeb4f.jpg" width="500" height="386" alt="bird6" /></a> 
<br/>
