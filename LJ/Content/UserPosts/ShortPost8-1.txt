﻿<a name="evil clown"></a><img src="https://farm8.staticflickr.com/7492/15515135179_25bb7e3437.jpg" width="400">


<br/>
<i>" Клоуну любое убийство сойдёт с рук."<br/>
Джон  Гейси перед арестом в 1978 году.</i>
<br/>
В американской массовой культуре "<a href="http://en.wikipedia.org/wiki/Evil_clown">злой клоун</a>" (evil clown)  олицетворяет не только смех, но и ужас. 
<a href="http://en.wikipedia.org/wiki/Evil_clown#Notable_depictions_of_evil_clowns">Cписок  его персонажей</a> гораздо внушительнее, чем традиционных клоунов. 
