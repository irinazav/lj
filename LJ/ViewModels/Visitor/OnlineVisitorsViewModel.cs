﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.ViewModels.Visitor
{
    public class OnlineVisitorsViewModel
    {
        public IEnumerable<string> UserNames { get; set; }
        public int AnonimousCount { get; set; }
    }
}