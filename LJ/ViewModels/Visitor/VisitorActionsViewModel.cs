﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LJ.ViewModels.Visitor
{
    public class VisitorActionsViewModel
    {
        public int Day { get; set; }
        public int Id { get; set; }

        // Storing TimeSpan Properties with EntityFramework Code First
        //http://evandontje.com/2013/04/30/storing-timespan-properties-with-entityframework-code-first/
        // EntityFunctions.DiffMilliseconds(dateTime1, dateTime2) < someNumber

       // public Int64 SpentTime { get; set; }
        [NotMapped]
        public TimeSpan SpentTime
        {
            get;
            set;
        }

        public long SpentTimeMC
        {
            get
            {
                //return SpentTime.TotalMilliseconds;
              return SpentTime.Ticks;
            }
            set
            {

               // var tt = TimeSpan.FromMilliseconds(value);
                SpentTime = TimeSpan.FromTicks(value);
            }
        }

        public int ActionType { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }

        public IEnumerable<string> Names1 { get; set; }
        public IEnumerable<string> Names2 { get; set; }

        public long Count1 { get; set; }
        public long Count2 { get; set; }
        public long Count3 { get; set; }
        public long Count4 { get; set; }

        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
    }
}