﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.ViewModels
{
    public class CommentReplyViewModel
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public string IdPrefix { get; set; }
        public string crDetails { get; set; }
        public int likes { get; set; }
        public int dislikes { get; set; }
        public string twshareUrl { get; set; }
        public string CommentReplyType { get; set; }
        public string FormName { get; set; }
        public string usernameCultureInfo { get; set; }
        public string imgUrl { get; set; }
        public string passedtime { get; set; }
        public bool ReplyDeleteChec { get; set; }
        public string UserName { get; set; }
        public string UserIdentityName { get; set; }
        public bool RequestIsAuthenticated { get; set; }
        public string replyType { get; set; }
        public string Body { get; set; }
        public string shareType { get; set; }
        public string sortorder { get; set; }
        public string date { get; set; }
        public string imgUrlL { get; set; }
    }
}

