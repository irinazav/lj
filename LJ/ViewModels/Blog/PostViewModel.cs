﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.Models.User;
using LJ.ViewModels.User;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LJ.ViewModels
{
    public class PostViewModel
    {
        public UserPanelInfo UserPanelInfo { get; set; }
        public string Body { get; set; }
        public int FirstPostId { get; set; }
        public int LastPostId { get; set; }
        public int NextPostId { get; set; }
        public int PrevPostId { get; set; }
        public int Id { get; set; }
        public long LineId { get; set; } //UserPost, FriendPost,TopPost info for sidebar and navigation
        public int ImageId { get; set; } //image selected for  post
        public int DefaultUserImageId { get; set; }
        public string UserName { get; set; }
        public bool Published { get; set; }
        
        public string NextPostSlug { get; set; }
        public int PostCount { get; set; }
        public int PostDislikes { get; set; }
        public int PostLikes { get; set; }
        public int CommentsCount { get; set; }
        public string PreviousPostSlug { get; set; }
        public string Title { get; set; }
        [Display(Name = "Posted On")]
        public DateTime PostedOn { get; set; }
        public string Description { get; set; }
        public Category Category { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public string Meta { get; set; }
        public string Url { get; set; }
        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }
        public int SecurityStatus { get; set; }
        public Group Group { get; set; }
        public IEnumerable<Group> Groups { get; set; }

        //SideBar
        public Category FilterCategory { get; set; }
        public Tag FilterTag { get; set; }
        public IEnumerable<Category> FilterCategories { get; set; }
        public IEnumerable<Tag> FilterTags { get; set; }

        //Comments
        public int SortOrder { get; set; }
        public IEnumerable<CommentViewModel> Comments { get; set; }

        [Display(Name = "Tags")]
        [Range(1, Int64.MaxValue, ErrorMessage = " count can't be zero")]
        public int CommentsCharCount { get; set; }

    }


}