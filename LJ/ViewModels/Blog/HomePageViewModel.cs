﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;
using LJ.Models.User;


namespace LJ.ViewModels
{
    public class HomePageViewModel
    {       
        public IEnumerable<Post> Posts { get; set; }
        public IEnumerable<ApplicationUser>Users { get; set; }         
    }
}
    