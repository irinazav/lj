﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using LJ.Models.Blog;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.ViewModels.User
{

    public class UserRatingViewModel
    {
        public decimal? AverageRating { get; set; }
        public int AverageTopOrder { get; set; }
        public int TopOrder { get; set; }
        public int OldTopOrder { get; set; }
        public int Rating { get; set; }
        public int OldRating { get; set; }
        public ApplicationUser User { get; set; }
    }
}

     

