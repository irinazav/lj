﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using LJ.Models.Blog;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.ViewModels.User
{
    public class StatisticsByFriendsViewModel
    {
        public ApplicationUser User { get; set; }
        public IEnumerable<FriendArxiv> FriendsWithArxiv { get; set; }
        public IEnumerable<FriendArxiv> FriendsOfArxiv { get; set; }
        public DateTime StartDateRange { get; set; }
    }
}