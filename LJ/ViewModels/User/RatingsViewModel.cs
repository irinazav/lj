﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.Blog;

namespace LJ.ViewModels.User
{
    public class RatingsViewModel
    {
        public IEnumerable<PostStatisticsViewModel> RatingsInfo { get; set; }
        public Post Post { get; set; }
    }
}