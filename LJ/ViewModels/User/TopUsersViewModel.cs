﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LJ.Models.User;
using LJ.Models.Blog;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LJ.ViewModels.User
{
    public class TopUsersViewModel
    {       
        public int TopUsersCount { get; set; }
        public IEnumerable<UserRatingViewModel> UserRatings { get; set; }
    }
}

     

