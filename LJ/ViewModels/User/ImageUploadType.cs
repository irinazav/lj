﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.ViewModels.User
{
    public enum ImageUploadType
    {
        Url = 1, 
        Flickr = 2, 
        File = 4, 
        IsFileFlickrUrl = (1 + 2 + 4)
    }

    public enum DefaultImageIdType
    {
        No = 0, 
        NewId = 2, 
        OldId = 1
    }
    public class DefaultImageInfo
    {
        public int OldId { get; set; }
        public int NewId { get; set; }
        public int DefaultImageType { get; set; }
    }   

}