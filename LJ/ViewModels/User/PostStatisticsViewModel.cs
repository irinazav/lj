﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LJ.ViewModels.User
{
    public class PostStatisticsViewModel
    {
        public int Id { get; set; }
        public string Id2 { get; set; }
        public string Title {get; set;}
        public int Count1 { get; set; }
        public int Count2 { get; set; }
        public int Count3 { get; set; }
        public int Count4 { get; set; }
        public int Count5 { get; set; }
        public int Count6 { get; set; }
        public int Day { get; set; }
        public DateTime DateTime { get; set; }
    }
}