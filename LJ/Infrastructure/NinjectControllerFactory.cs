﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Configuration;
using System.Web.Routing;
using System.Web.Mvc;
using LJ.Models.Blog;
using LJ.Models.Visitor;
using LJ.Models.User;
using LJ.Repository.BlogRepository;
using LJ.Repository.UserRepository;
using LJ.Repository.VisitorRepository;
using LJ.Controllers;


namespace LJ.Infrastructure
{
    // need to add to global.asax
    // ControllerBuilder.Current.SetControllerFactory(new NinjectControllerFactory());
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
            AddBindingsDB();
        }

        protected override IController GetControllerInstance(RequestContext requestContext,
                                    Type controllerType)
        {
            return controllerType == null
            ? null
            : (IController)kernel.Get(controllerType);
        }



        private void AddBindingsDB()
        {
            kernel.Bind<IBlogRepository<Post, int>>().To<BlogRepository>();
            kernel.Bind<IUserRepository<ApplicationUser, int>>().To<UserRepository>();
            kernel.Bind<IVisitorRepository<ApplicationVisitor, int>>().To<VisitorRepository>();

        }
    }
}