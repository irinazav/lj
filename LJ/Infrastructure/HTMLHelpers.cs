﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using LJ.Models.Blog;
using LJ.ViewModels;
using System.Linq.Expressions;
using LJ.ViewModels.User;
using LJ.Models.User;

namespace LJ.Infrastructure
{
    public static class HTMLHelpers { 
       


        public static MvcHtmlString File(this HtmlHelper html, string name)
        {
            var tb = new TagBuilder("input");
            tb.Attributes.Add("type", "file");
            tb.Attributes.Add("name", name);
            tb.GenerateId(name);
            return MvcHtmlString.Create(tb.ToString(TagRenderMode.SelfClosing));
        }





        public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            string name = GetFullPropertyName(expression);
            return html.File(name);
        }






        static string GetFullPropertyName<T, TProperty>(Expression<Func<T, TProperty>> exp)
        {
            MemberExpression memberExp;

            if (!TryFindMemberExpression(exp.Body, out memberExp))
                return string.Empty;

            var memberNames = new Stack<string>();

            do
            {
                memberNames.Push(memberExp.Member.Name);
            }
            while (TryFindMemberExpression(memberExp.Expression, out memberExp));

            return string.Join(".", memberNames.ToArray());
        }





        static bool TryFindMemberExpression(Expression exp, out MemberExpression memberExp)
        {
            memberExp = exp as MemberExpression;

            if (memberExp != null)
                return true;

            if (IsConversion(exp) && exp is UnaryExpression)
            {
                memberExp = ((UnaryExpression)exp).Operand as MemberExpression;

                if (memberExp != null)
                    return true;
            }

            return false;
        }




        static bool IsConversion(Expression exp)
        {
            return (exp.NodeType == ExpressionType.Convert || exp.NodeType == ExpressionType.ConvertChecked);
        }

       





        public static MvcHtmlString UserNameLinkToPosts(this HtmlHelper helper, Post post)
        {
            var username =  post.ApplicationUser.UserName;
            return helper.ActionLink(username, "GetUserPosts", "Blog",
                new
                {
                    id = username,
                    
                },
                new
                {

                    title = String.Format("See all posts by blogger {0}", username)
                });              
        }







        public static MvcHtmlString UserNameLinkToProfile(this HtmlHelper helper, Post post)
        {
            var username = post.ApplicationUser.UserName;
            return helper.ActionLink(username, "GetProfile", "User",
                new
                {
                    id = username,
                    
                },
                new
                {

                    title = String.Format("See profile blogger {0}", username)
                });

        }





        public static MvcHtmlString PostLink(this HtmlHelper helper, Post post, int categoryid=0, int tagid=0, string title="")
        {
            if (title == "") title = post.Title;
            if(tagid==0 && categoryid==0)
                return helper.ActionLink(title, "GetUserPost", "Blog", new { id = post.Id }, new { title = title });  
            else if (tagid >0)
                return helper.ActionLink(title, "GetUserPost", "Blog", new { id = post.Id, tagid = tagid }, new { title = title });
            else
                return helper.ActionLink(title, "GetUserPost", "Blog", new { id = post.Id, categoryid = categoryid }, new { title = title });
        }

   

      

        

       

        public static MvcHtmlString CategoryLink(this HtmlHelper helper,
           Category category, UserPanelInfo upi)
        {
            
            long action = upi.FriendType & (long)FriendType.Action;

            return helper.ActionLink(category.Name, "Get"+Enum.GetName(typeof(FriendType),  action), "Blog",
                new
                {                  
                    categoryid = category.Id,
                    id = upi.UserName,
                },
                new
                {
                    title = String.Format("See all posts in {0}", category.Name)
                });
        }




        public static MvcHtmlString TagLink(this HtmlHelper helper, Tag tag, UserPanelInfo upi)
        {
            long action = upi.FriendType & (long)FriendType.Action;
            return helper.ActionLink(tag.Name,  "Get"+ Enum.GetName(typeof(FriendType),action), "Blog", new { tagid = tag.Id, id = upi.UserName, },
                new {
                    title = String.Format("See all posts in {0}", tag.Name)
                });
        }

       




        public static MvcHtmlString CategoryLink(this HtmlHelper helper,
             Category category, string username, string action, int pagesize=0)
        {
            if (pagesize > 0)
            return helper.ActionLink(category.Name, action,
                          "Blog", new { categoryid = category.Id, id = username, pageSize = pagesize },
                 new
                 {
                     title = String.Format("See all posts in category {0}", category.Name)
                 });
            else

                return helper.ActionLink(category.Name, action,
                          "Blog", new { categoryid = category.Id, id = username },
                 new
                 {
                     title = String.Format("See all posts in category {0}", category.Name)
                 });
        }






        public static MvcHtmlString TagLink(this HtmlHelper helper, Tag tag, string username, string action, int pagesize)
        {
            if (pagesize > 0)
            return helper.ActionLink(tag.Name,  action,
                         "Blog", new { tagid = tag.Id, id = username, pageSize = pagesize },
                new
                {
                    title = String.Format("See all posts in tag {0}", tag.Name)
                });
            else

                return helper.ActionLink(tag.Name, action,
                        "Blog", new { tagid = tag.Id, id = username },
               new
               {
                   title = String.Format("See all posts in tag {0}", tag.Name)
               });
        }







        public static MvcHtmlString PostLink(this HtmlHelper helper, Post post,  ModelViewPosts mv)
        {
            long friendType = mv.UserPanelInfo.FriendType;
            long action = friendType & (long)FriendType.Action;
            var newAction = (mv.UserPanelInfo.FriendType & (~((long)FriendType.Posts))) | (long)FriendType.Post;
            string actionName = Enum.GetName(typeof(FriendType), action);
            
            string username = mv.UserPanelInfo.UserName;
            return helper.ActionLink(post.Title, "GetPost", "Blog",
                new
                {
                    id = post.Id,
                    tagid = mv.Tag == null ? 0 : mv.Tag.Id,
                    categoryid = mv.Category == null ? 0 : mv.Category.Id,
                    lineid = newAction
                },
                new { title = post.Title + "  in " + actionName.Split('P')[0] + " Posts" });
        }


    public static MvcHtmlString PostLink(this HtmlHelper helper,  PostViewModel mv)
    {
        long friendType = mv.UserPanelInfo.FriendType;
        long action = friendType & (long)FriendType.Action;
        string actionName = Enum.GetName(typeof(FriendType), action);
        var newAction = (mv.UserPanelInfo.FriendType & (~((long)FriendType.Posts))) | (long)FriendType.Post;
        string username = mv.UserPanelInfo.UserName;
        return helper.ActionLink(mv.Title, "GetPost", "Blog",
            new { id = mv.Id,
                  tagid = mv.FilterTag == null ? 0 : mv.FilterTag.Id,
                  categoryid = mv.FilterCategory == null ? 0 : mv.FilterCategory.Id,
                  lineid = newAction
            },
            new { title = mv.Title + "  in " + actionName.Split('P')[0] +" Posts" });
    }


        public static MvcHtmlString FilterTagLink(this HtmlHelper helper, Tag tag, ModelViewSideBar mv)
        {
            long friendType = mv.UserPanelInfo.FriendType;
            long action = friendType & (long)FriendType.Action;
            string actionName = Enum.GetName(typeof(FriendType), action);
            string username = mv.UserPanelInfo.UserName;
            int postsPerPage = (friendType & (long)FriendType.Navigation) == (long)FriendType.Post ? 0 : mv.PageSize;
           

            return helper.ActionLink(tag.Name, "Get" + actionName, "Blog", new { tagid = tag.Id, id = username, pageSize = postsPerPage },
                new
                {
                    title = String.Format("See all posts in {0}", tag.Name)
                });
        }

        public static MvcHtmlString FilterCategoryLink(this HtmlHelper helper, Category category, ModelViewSideBar mv)
        {
            long friendType = mv.UserPanelInfo.FriendType;
            long action = friendType & (long)FriendType.Action;
            string actionName = Enum.GetName(typeof(FriendType), action);
            string username = mv.UserPanelInfo.UserName;
            int postsPerPage = (friendType & (long)FriendType.Navigation) == (long)FriendType.Post ? 0 : mv.PageSize;


            return helper.ActionLink(category.Name, "Get" + actionName, "Blog", new { tagid = category.Id, id = username, pageSize = postsPerPage },
                new
                {
                    title = String.Format("See all posts in {0}", category.Name)
                });
        }
    }
}