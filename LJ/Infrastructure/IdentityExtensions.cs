﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using LJ.Models.Account;
using LJ.Models.User;
using LJ.Repository.BlogRepository;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Principal;
using System.Collections.Generic;
using System.Data.Entity;




namespace LJ.Infrastructure
{
    public static  class IdentityExtensions
    {


        public static string GetIdentityImageId(this IIdentity identity)
        {
           if (identity == null)
                return null;

           var imageDefaultId = (identity as ClaimsIdentity).FirstOrNull(ClaimTypes.GivenName);
           return imageDefaultId;
        }



        public static string GetIdentityUserId(this IIdentity identity)
        {
            if (identity == null)
                return null;

            var userId = (identity as ClaimsIdentity).FirstOrNull("UserId");
            return userId;
        }



        public static IEnumerable<string> GetIdentityImageIds(this IIdentity identity)
        {
            if (identity == null)
                return null;
           
            string userImageIds = (identity as ClaimsIdentity).FirstOrNull("UserImageIds");
            if (userImageIds == "") return new string[]{"1"};
            else return userImageIds.Split(',');
            
        }


       
        internal static string FirstOrNull(this ClaimsIdentity identity, string claimType)
        {
            var val = identity.FindFirst(claimType);
            return val == null ? null : val.Value;
        }
    }
}